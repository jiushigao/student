package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.InterviewFun;
import  com.student.student.service.InterviewFunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

/**
 * 访谈类型
 * @author 资凯
 */

@Controller
@RequestMapping("/interviewFun")
public class InterviewFunController {

    @Autowired
    private InterviewFunService InterviewFunService;

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/list")
    public String list(InterviewFun interviewFun, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<InterviewFun> pageInfo = new PageInfo<>();
        try{
            pageInfo = InterviewFunService.findListPage(interviewFun, pageNum, pageSize);
        }catch (Exception ex){
            model.addAttribute("rst","-1");
            ex.printStackTrace();
        }
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("interviewFun",interviewFun);
        return "interviewFun/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/interviewFun.json")
    @ResponseBody
    public PageInfo listPageJson(String funName, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<InterviewFun> pageInfo = new PageInfo<>();
        try{
            pageInfo = InterviewFunService.findListPage(new InterviewFun(funName  ), pageNum, pageSize);
        }catch (Exception ex){
            model.addAttribute("rst","-1");
            ex.printStackTrace();
        }
        return  pageInfo;
    }

    /**
     *根据id查询
     */
    @RequestMapping("/toAddOrUpdate")
    public String toAddOrUpdate(Integer id,Model model){
        if (id != null && id != null){
            InterviewFun one = new InterviewFun(id);
            try{
                InterviewFun interviewFun = InterviewFunService.findOne(one);
                model.addAttribute("interviewFun",interviewFun);
            }catch (Exception ex){
                ex.printStackTrace();
                model.addAttribute( "rst", "-1" );
            }
        }
        return "interviewFun/edit";
    }

    /**
     * 新增
     */
    @RequestMapping("/add")
    public String add(InterviewFun interviewFun, Model model){
        if(interviewFun == null || interviewFun.getFunName() == null || interviewFun.getFunName().trim().length() <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/interviewFun/add";
        }
        int cnt = -1;
        try{
             cnt = InterviewFunService.add(interviewFun);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(cnt > 0){
            return "redirect:/interviewFun/list";
        }else{
            model.addAttribute( "rst", "-1" );
            return "redirect:/interviewFun/toAddOrUpdate";
        }
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delById")
    @ResponseBody
    public Integer delById(Integer id, Model model){
        int rst = -1;
        try{
            rst = InterviewFunService.delById(id);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        if(ids == null || ids.trim().length() <= 0){
            return -1;
        }
        int rst = -1;
        try{
            rst = InterviewFunService.delByIds( Arrays.asList(ids.split( "," )));
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public String update(InterviewFun interviewFun, Model model){
        if(interviewFun == null || interviewFun.getFunName() == null || interviewFun.getFunName().trim().length() <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/interviewFun/toAddOrUpdate?id="+interviewFun.getId();
        }
        int cnt = -1;
        try{
            cnt = InterviewFunService.update(interviewFun);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(cnt <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/interviewFun/toAddOrUpdate?id="+interviewFun.getId();
        }else{
            return "redirect:/interviewFun/list";
        }
    }

    /**
     * 验证名字是否已经存在
     * @param name
     * @param model
     * @return 1表示名字已经存在，0表示不存在
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public Integer checkName(String name, Integer id,  Model model){
        int  rst = -1;
        try{
            id = (id == null || id == -1) ? null : id;
            rst = InterviewFunService.countByName( new InterviewFun( id, name ) );
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(rst > 0){
            return 1;
        }else{
            return 0;
        }

    }
}
