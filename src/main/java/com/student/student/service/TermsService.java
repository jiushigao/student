package com.student.student.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Producttype;
import com.student.student.pojo.School;
import com.student.student.pojo.Term;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学期管理
 * 作者:张文
 */
public interface TermsService {

    /**
     * 根据条件查询多个，分页
     * @param term
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<Term> findListPage(Term term, Integer pageNum, Integer pageSize);

    /**
     * 根据条件查询多个，不分页
     * @param term
     * @return
     */
    List<Term> findList(Term term);

    /**
     * 根据id删除产品
     * @param id
     */
    int deleteTermById(String id);
    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deleteTermByIds(List<String> ids);

    /**
     * 根据id查询单个产品信息进行修改
     * @param id
     * @return
     */
    Term findTermByIds(String id);

    /**
     * 获取实体类对象进行修改
     * @param term
     */
    int updateTerm(Term term);

    /**
     * 获取实体类对象进行新增
     * @param term
     */
    int insertTerm(Term term);
    /**
     * 根据条件统计
     * @param term
     * @return
     */
    int countByname(Term term);

    /**
     * 根据产品查询学期
     * @param id
     * @return
     */
    List<Term> findTermid(Integer id);
}
