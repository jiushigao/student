package com.student.student.service;

import com.student.student.pojo.Classes;
import com.student.student.pojo.Regulations;
import com.student.student.pojo.Violate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ViolateService {

    List<Classes> ClassName(String name);
    List<Violate> finvio( Integer id,String name);
    List<Violate> finvioid( Integer id);
    List<Regulations> finvioReg();
    void addVio(Violate violate);
    void delVio(Integer vid);
}
