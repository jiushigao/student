package com.student.student.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * interview
 *
 * @author laoK
 * @version 1.0.0 2019-05-27
 */
public class InterviewType implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -8910863403686889879L;

    /**
     * 主键
     */
    private Integer id;
    /**
     * 跟进类型
     */
    private String typeName;

    public InterviewType() {
    }
    /**
     * 根据id封装
     */
    public InterviewType(Integer id) {
        this.id = id;
    }
    public InterviewType(Integer id, String typeName) {
        this.id = id;
        this.typeName = typeName;
    }
    public InterviewType(String typeName) {
        this.typeName = typeName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
