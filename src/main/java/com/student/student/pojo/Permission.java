package com.student.student.pojo;

/**
 * 权限
 * 作者：Dora
 * 日期：2019-06-08 08:59
 */
public class Permission {
    /**主键*/
    private Integer id;
    /**权限名称*/
    private String name;
    /**第几级菜单：1表示1级菜单，2表示2级菜单*/
    private Integer level;
    /**访问路径，如果是一级目录是没有访问路径*/
    private String path;
    /**父id，-1表示一级*/
    private Integer parentId;
    /**排序*/
    private Integer sort;

    public Permission() { }
    public Permission(Integer id) {
        this.id = id;
    }
    public Permission(Integer id, String name, Integer level, Integer parentId) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.parentId = parentId;
    }
    public Permission(String name, String path, Integer parentId, Integer sort) {
        this.name = name;
        this.path = path;
        this.parentId = parentId;
        this.sort = sort;
    }
    public Permission(Integer id, String name, Integer level, String path, Integer parentId, Integer sort) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.path = path;
        this.parentId = parentId;
        this.sort = sort;
    }

    /**主键*/
    public Integer getId() {
        return id;
    }
    /**主键*/
    public void setId(Integer id) {
        this.id = id;
    }
    /**权限名称*/
    public String getName() {
        return name;
    }
    /**权限名称*/
    public void setName(String name) {
        this.name = name;
    }
    /**第几级菜单：1表示1级菜单，2表示2级菜单*/
    public Integer getLevel() {
        return level;
    }
    /**第几级菜单：1表示1级菜单，2表示2级菜单*/
    public void setLevel(Integer level) {
        this.level = level;
    }
    /**访问路径，如果是一级目录是没有访问路径*/
    public String getPath() {
        return path;
    }
    /**访问路径，如果是一级目录是没有访问路径*/
    public void setPath(String path) {
        this.path = path;
    }
    /**父id，-1表示一级*/
    public Integer getParentId() {
        return parentId;
    }
    /**父id，-1表示一级*/
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
    /**排序*/
    public Integer getSort() {
        return sort;
    }
    /**排序*/
    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
