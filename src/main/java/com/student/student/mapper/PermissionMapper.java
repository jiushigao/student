package com.student.student.mapper;

import com.student.student.pojo.Admin;
import com.student.student.pojo.Permission;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 权限
 * 作者：Dora
 * 日期：2019-06-08 08:59
 */
public interface PermissionMapper {

    /**
     * 根据条件查询所有状态信息
     */
    List<Permission> selectList(Permission permission);

    /**
     * 根据角色得到对应的权限
     * @param roleId
     * @return
     */
    List<Integer> selectListByRole(Integer roleId);

    /**
     * 根据主键查询数据
     */
    Permission selectOne(Permission permission);

    /**
     * 插入数据
     */
    int insert(Permission permission);

    /**
     * 修改数据
     */
    int update(Permission permission);

    /**
     * 根据主键删除数据
     */
    int delById(Integer id);

    /**
     * 根据父id删除
     * @param parentId
     * @return
     */
    int delByParentId(Integer parentId);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(@Param( "ids" ) List<String> ids);

    /**
     * 判断名字是否唯一
     */
    int findByName(Permission permission);


    /**
     * 插入数据
     */
    int inserts(List<Permission> list);

    /**
     * 修改数据
     */
    int updates(List<Permission> list);

}
