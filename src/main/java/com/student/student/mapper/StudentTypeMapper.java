package com.student.student.mapper;

import com.student.student.pojo.Studenttype;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学员状态管理--Dora
 */
@Mapper
public interface StudentTypeMapper {

    /**
     * 根据条件查询所有校区信息,返回List
     */
    List<Studenttype> selectList(Studenttype studenttype);

    /**
     * 根据主键查询数据，返回单个实体
     */
    Studenttype selectOne(Studenttype studenttype);

    /**
     * 插入数据
     */
    int insert(Studenttype studenttype);

    /**
     * 批量插入数据
     */
    int inserts(List<Studenttype> list);

    /**
     * 修改数据
     */
    int update(Studenttype studenttype);

    /**
     * 批量修改数据
     */
    int update(List<Studenttype> list);

    /**
     * 根据主键删除数据
     */
    int delById(Integer id);

    /**
     * 批量删除
     */
    int delByIds(@Param("ids") List<String> ids);

    /**
     * 统计校区名是否已经存在
     */
    int countByName(Studenttype studenttype);
}
