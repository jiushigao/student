package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.RegulationsMapper;
import com.student.student.pojo.Producttype;
import com.student.student.pojo.Regulations;
import com.student.student.service.RegulationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 校规实现类
 * 作者：张文
 */
@Service
public class RegulationsServiceImpl implements RegulationsService {
    @Autowired
    private RegulationsMapper regulationsMapper;

    @Override
    public PageInfo<Regulations> findListPage(Regulations regulations, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Regulations> list = regulationsMapper.selectList(regulations);
        PageInfo<Regulations> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public List<Regulations> findList(Regulations regulations) {
        return regulationsMapper.selectList(regulations);
    }

    @Override
    public int deleteRegulationsById(String id) {
        return regulationsMapper.deleteRegulationsById(id);
    }

    @Override
    public int deleteRegulationsByIds(List<String> ids) {
        return regulationsMapper.deleteRegulationsByIds(ids);
    }

    @Override
    public Regulations findRegulationsByIds(Regulations regulations) {
        return regulationsMapper.selectOne(regulations);
    }

    @Override
    public int updateRegulations(Regulations regulations) {
        return regulationsMapper.updateRegulations(regulations);
    }

    @Override
    public int insertRegulations(Regulations regulations) {
        return regulationsMapper.insertRegulations(regulations);
    }

    @Override
    public int countByname(Regulations regulations) {
        return regulationsMapper.countByname(regulations);
    }
}
