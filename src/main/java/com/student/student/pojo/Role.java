package com.student.student.pojo;

import java.util.List;

/**
 * 角色
 * 作者：Dora
 * 日期：2019-06-08 08:57
 */
public class Role {
    /**主键*/
    private Integer id;
    /**角色名称*/
    private String name;

    public Role() {
    }
    public Role(Integer id) {
        this.id = id;
    }
    public Role(String name) {
        this.name = name;
    }
    public Role(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    /**主键*/
    public Integer getId() {
        return id;
    }
    /**主键*/
    public void setId(Integer id) {
        this.id = id;
    }
    /**角色名称*/
    public String getName() {
        return name;
    }
    /**角色名称*/
    public void setName(String name) {
        this.name = name;
    }
}
