/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

import java.util.Date;

/**
 * 作业表(work)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Work implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 6773329197058013992L;

    /** 主键 */
    private Integer id;

    /** 对应学生id */
    private Long stuId;

    /** 作业的文件名 */
    private String filename;

    /** 上传时间 */
    private Date createdate;

    /**
     * 获取主键
     * 
     * @return 主键
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置主键
     * 
     * @param id
     *          主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取对应学生id
     * 
     * @return 对应学生id
     */
    public Long getStuId() {
        return this.stuId;
    }

    /**
     * 设置对应学生id
     * 
     * @param stuId
     *          对应学生id
     */
    public void setStuId(Long stuId) {
        this.stuId = stuId;
    }

    /**
     * 获取作业的文件名
     * 
     * @return 作业的文件名
     */
    public String getFilename() {
        return this.filename;
    }

    /**
     * 设置作业的文件名
     * 
     * @param filename
     *          作业的文件名
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * 获取上传时间
     * 
     * @return 上传时间
     */
    public Date getCreatedate() {
        return this.createdate;
    }

    /**
     * 设置上传时间
     * 
     * @param createdate
     *          上传时间
     */
    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }
}