package com.student.student.pojo;

/**
 * 管理员
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Admin implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -3054832580155529394L;
    /** id */
    private Integer id;
    /** 昵称 */
    private String name;
    /**登录信息*/
    private String phone;
    /**密码*/
    private String pwd;

    public Admin(Integer id) {
        this.id = id;
    }
    public Admin() {
    }
    public Admin(Integer id, String name, String phone, String pwd) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.pwd = pwd;
    }
    public Admin(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    /** id */
    public Integer getId() {
        return id;
    }
    /** id */
    public void setId(Integer id) {
        this.id = id;
    }
    /** 昵称 */
    public String getName() {
        return name;
    }
    /** 昵称 */
    public void setName(String name) {
        this.name = name;
    }
    /**登录信息*/
    public String getPhone() {
        return phone;
    }
    /**登录信息*/
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**密码*/
    public String getPwd() {
        return pwd;
    }
    /**密码*/
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}