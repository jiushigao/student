package com.student.student.mapper;

import com.student.student.pojo.Login;
import com.student.student.pojo.LoginIp;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 登录
 * 作者：Dora
 * 日期：2019-05-22 13:21
 */
@Mapper
public interface LoginMapper {

    /**
     * 根据条件查询数据,返回实体
     * @param login
     * @return
     */
    Login selectOne(Login login);

    /**
     * 根据条件查询数据，返回list集合
     * @param login
     * @return
     */
    List<Login> selectByConAll(Login login);

    /**
     * 更新
     * @param login
     */
    int updateById(Login login);

    /**
     * 添加登录数据
     * @param login
     * @return
     */
    int insert(Login login);

    /**
     * 根据条件统计
     * @param login
     * @return
     */
    int countByPhone(Login login);

    /**
     * 根据Id删除
     * @param id
     * @return
     */
    int delById(@Param("id") Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(@Param("ids")List<String> ids);

    /**
     * 根据条件统计
     * @param login
     * @return
     */
    int count(Login login);

    /**
     * 统计电话号码是否存在,必须要传入角色id和手机号码
     * @param login
     * @return
     */
    int cntPhone(Login login);
}
