/*
 * Welcome to use the TableGo Tools.
 *
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 *
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

import lombok.Data;

import java.sql.Timestamp;

/**
 * interview
 *
 * @author laoK
 * @version 1.0.0 2019-05-23
 */
@Data
public class Interview implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -8910863403686889879L;

    /** 主键 */
    private Integer id;
    /** 外键，关联Login表 */
    private Integer stuLoginId;
    /** 学员姓名 */
    private String stuName;
    /** 班级id */
    private Integer classesId;
    /**班级名称*/
    private String classesName;
    /**班级信息*/
    private Classes classes;
    /** 跟进类型：1出勤；2纪律；3作业情况；4面试题；5内部测试 */
    private Integer type;
    /** 跟踪方式：1学员当面；2学员电话；3学员QQ或微信；4家长当面；5家长电话 */
    private Integer fun;
    /** 跟踪内容 */
    private String content;
    /** 拜访时间 */
    private String  interviewTime;
    /**
     * 跟进类型名称
     */
    private String typeName;
    /**
     * 跟进类型
     */
    private InterviewType interviewType;
    /**
     * 跟进方式
     */
    private InterviewFun interviewFun;

    /**
     * 学员信息
     */
    private Student student;

    /**
     * 根据多个条件
     * @param stuLoginId
     * @param classesName
     * @param stuName
     * @param typeName
     */
    public Interview(Integer stuLoginId,String classesName,String stuName,String typeName) {
        if (stuLoginId != null){
            this.stuLoginId = stuLoginId;
        }
        if (classesName != null){
            this.classesName = classesName;
        }
        if (stuName != null){
            this.stuName = stuName;
        }
        if (typeName != null){
            this.typeName = typeName;
        }
    }
    public Interview() {
    }

    public Interview(Integer id) {
        this.id = id;
    }

    /**
     * 获取主键
     *
     * @return 主键
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置主键
     *
     * @param id
     *          主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取外键，关联Login表
     *
     * @return 外键
     */
    public Integer getStuLoginId() {
        return this.stuLoginId;
    }

    /**
     * 设置外键，关联Login表
     *
     * @param stuLoginId
     *          外键
     */
    public void setStuLoginId(Integer stuLoginId) {
        this.stuLoginId = stuLoginId;
    }

    /**
     * 获取班级id
     *
     * @return 班级id
     */
    public Integer getClassesId() {
        return this.classesId;
    }

    /**
     * 设置班级id
     *
     * @param classesId
     *          班级id
     */
    public void setClassesId(Integer classesId) {
        this.classesId = classesId;
    }

    /**
     * 获取跟进类型：1出勤；2纪律；3作业情况；4面试题；5内部测试
     *
     * @return 跟进类型
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 设置跟进类型：1出勤；2纪律；3作业情况；4面试题；5内部测试
     *
     * @param type
     *          跟进类型
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取跟踪方式：1学员当面；2学员电话；3学员QQ或微信；4家长当面；5家长电话
     *
     * @return 跟踪方式
     */
    public Integer getFun() {
        return this.fun;
    }

    /**
     * 设置跟踪方式：1学员当面；2学员电话；3学员QQ或微信；4家长当面；5家长电话
     *
     * @param fun
     *          跟踪方式
     */
    public void setFun(Integer fun) {
        this.fun = fun;
    }
    /**
     * 获取跟踪内容
     *
     * @return 跟踪内容
     */
    public String getContent() {
        return this.content;
    }
    /**
     * 设置跟踪内容
     *
     * @param content
     *          跟踪内容
     */
    public void setContent(String content) {
        this.content = content;
    }
    /**
     * 获取拜访时间
     *
     * @return 拜访时间
     */
    public String getInterviewTime() {
        return interviewTime;
    }
    /**
     * 设置拜访时间
     *
     * @param  interviewTime
     *          拜访时间
     */
    public void setInterviewTime(String interviewTime) {
        this.interviewTime = interviewTime;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}