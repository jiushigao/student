package com.student.student.pojo.stu;

/**
 * 学员信息，用于新增
 * @author Dora
 */
public class StudentAdd implements java.io.Serializable {
    /** 学员id */
    private Integer stuLoginId;
    /** 学员姓名 */
    private String stuName;
    /** 学员电话 */
    private String phone;
    /** 校区id */
    private Integer schoolId;
    /** 产品id */
    private Integer producttypesId;
    /** 学期id */
    private Integer termId;
    /** 班级id */
    private Integer classesId;
    /** 职位 */
    private Integer position;
    /** 年龄阶段 */
    private Integer ageId;
    /** 性别 */
    private String sex;
    /** QQ */
    private String qq;
    /** 学员基本情况描述 */
    private String describe;
    /** 家庭经济来源 */
    private String economy;
    /** 工作经历 */
    private String work;
    /** 游戏经历 */
    private String game;
    /** 推荐人 */
    private String referee;
    /** 学员分类 */
    private Integer studentTypeId;
    /** 学员状态id */
    private Integer status;
    /** 身份证 */
    private String idCard;
    /** 家庭住址 */
    private String address;
    /** 备注 */
    private String remarks;


    /** 学员id */
    public Integer getStuLoginId() {
        return stuLoginId;
    }
    /** 学员id */
    public void setStuLoginId(Integer stuLoginId) {
        this.stuLoginId = stuLoginId;
    }
    /** 学员姓名 */
    public String getStuName() {
        return stuName;
    }
    /** 学员姓名 */
    public void setStuName(String stuName) {
        this.stuName = stuName;
    }
    /** 学员电话 */
    public String getPhone() {
        return phone;
    }
    /** 学员电话 */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /** 校区id */
    public Integer getSchoolId() {
        return schoolId;
    }
    /** 校区id */
    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }
    /** 班级id */
    public Integer getClassesId() {
        return classesId;
    }
    /** 班级id */
    public void setClassesId(Integer classesId) {
        this.classesId = classesId;
    }
    /** 职位 */
    public Integer getPosition() {
        return position;
    }
    /** 职位 */
    public void setPosition(Integer position) {
        this.position = position;
    }
    /** 年龄阶段 */
    public Integer getAgeId() {
        return ageId;
    }
    /** 年龄阶段 */
    public void setAgeId(Integer ageId) {
        this.ageId = ageId;
    }
    /** 性别 */
    public String getSex() {
        return sex;
    }
    /** 性别 */
    public void setSex(String sex) {
        this.sex = sex;
    }
    /** QQ */
    public String getQq() {
        return qq;
    }
    /** QQ */
    public void setQq(String qq) {
        this.qq = qq;
    }
    /** 学员基本情况描述 */
    public String getDescribe() {
        return describe;
    }
    /** 学员基本情况描述 */
    public void setDescribe(String describe) {
        this.describe = describe;
    }
    /** 家庭经济来源 */
    public String getEconomy() {
        return economy;
    }
    /** 家庭经济来源 */
    public void setEconomy(String economy) {
        this.economy = economy;
    }
    /** 工作经历 */
    public String getWork() {
        return work;
    }
    /** 工作经历 */
    public void setWork(String work) {
        this.work = work;
    }
    /** 游戏经历 */
    public String getGame() {
        return game;
    }
    /** 游戏经历 */
    public void setGame(String game) {
        this.game = game;
    }
    /** 推荐人 */
    public String getReferee() {
        return referee;
    }
    /** 推荐人 */
    public void setReferee(String referee) {
        this.referee = referee;
    }
    /** 学员分类 */
    public Integer getStudentTypeId() {
        return studentTypeId;
    }
    /** 学员分类 */
    public void setStudentTypeId(Integer studentTypeId) {
        this.studentTypeId = studentTypeId;
    }
    /** 学员状态id */
    public Integer getStatus() {
        return status;
    }
    /** 学员状态id */
    public void setStatus(Integer status) {
        this.status = status;
    }
    /** 身份证 */
    public String getIdCard() {
        return idCard;
    }
    /** 身份证 */
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
    /** 家庭住址 */
    public String getAddress() {
        return address;
    }
    /** 家庭住址 */
    public void setAddress(String address) {
        this.address = address;
    }
    /** 备注 */
    public String getRemarks() {
        return remarks;
    }
    /** 备注 */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    /** 产品id */
    public Integer getProducttypesId() {
        return producttypesId;
    }
    /** 产品id */
    public void setProducttypesId(Integer producttypesId) {
        this.producttypesId = producttypesId;
    }
    /** 学期id */
    public Integer getTermId() {
        return termId;
    }
    /** 学期id */
    public void setTermId(Integer termId) {
        this.termId = termId;
    }
}