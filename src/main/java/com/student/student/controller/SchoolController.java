package com.student.student.controller;

/**
 * School的controller层
 */

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.InterviewFun;
import com.student.student.pojo.School;
import com.student.student.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@Controller
//@RestController
@RequestMapping("/school")
public class SchoolController {

    @Autowired
    private SchoolService schoolService;

    /**
     * 根据条件查询所有
     */
    @GetMapping("/list")
    public String list(School school, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<School> pageInfo = new PageInfo<>();
        try {
            pageInfo = schoolService.findListPage(school, pageNum, pageSize);
        }catch (Exception ex){
            model.addAttribute("rst","-1");
            ex.printStackTrace();
        }
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("school",school);
        return "school/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/school.json")
    @ResponseBody
    public PageInfo listPageJson(School school, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<School> pageInfo = new PageInfo<>();
        try {
            pageInfo = schoolService.findListPage(school, pageNum, pageSize);
        }catch (Exception ex){
            model.addAttribute("rst","-1");
            ex.printStackTrace();
        }
        return  pageInfo;
    }

    /**
     * 根据id查询
     */
    @GetMapping("/toAddOrUpdate")
    public String toUpdate(Integer id,Model model){
        if (id != null && id != null){
            School schoolCon = new School(id);
            try {
                School school = schoolService.findOne(schoolCon);
                model.addAttribute("school",school);
            }catch (Exception ex){
                ex.printStackTrace();
                model.addAttribute( "rst", "-1" );
            }
        }
        return "/school/edit";
    }

    /**
     * 新增
     */
    @RequestMapping("/add")
    public String add(School school,Model model){
        if(school == null || school.getName() == null || school.getName().trim().length() <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/school/add";
        }
        int cnt = -1;
        try{
            cnt = schoolService.insertSchool(school);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(cnt > 0){
            return "redirect:/school/list";
        }else{
            model.addAttribute( "rst", "-1" );
            return "redirect:/school/toAddOrUpdate";
        }
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delById")
    @ResponseBody
    public Integer delById(Integer id,Model model){
        int rst = -1;
        try{
            rst = schoolService.deleteSchoolById(id);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 批量删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        if(ids == null || ids.trim().length() <= 0){
            return -1;
        }
        int rst = -1;
        try{
            rst = schoolService.delByIds( Arrays.asList(ids.split( "," )));
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 根据Id修改
     */
    @RequestMapping("/update")
    public String updateSchool(School school,Model model){
        if(school == null || school.getName() == null || school.getName().trim().length() <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/school/toAddOrUpdate?id=" + school.getId();
        }
        int cnt = -1;
        try{
            cnt = schoolService.updateSchool(school);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(cnt <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/school/toAddOrUpdate?id="+school.getId();
        }else{
            return "redirect:/school/list";
        }
    }

    /**
     * 验证名字是否已经存在
     * @return 1表示名字已经存在，0表示不存在
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public Integer checkName(String name, Integer id,  Model model){
        int  rst = -1;
        try{
            id = (id == null || id == -1) ? null : id;
            rst = schoolService.countByName( new School( id, name ) );
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(rst > 0){
            return 1;
        }else{
            return 0;
        }

    }

}
