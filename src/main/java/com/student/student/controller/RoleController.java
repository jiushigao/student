package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Role;
import com.student.student.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色管理-Dora
 */
@Controller
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleService roleService; //角色


    /**
     * 根据条件查询所有
     */
    @RequestMapping("/list")
    public String list(Role role, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Role> pageInfo = new PageInfo<>(  );
        try {
            pageInfo = roleService.findListPage(role, pageNum, pageSize);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        model.addAttribute("pageInfo",pageInfo);

        model.addAttribute("status",role);
        return "role/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/role.json")
    @ResponseBody
    public PageInfo listPageJson(Role role, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<Role> pageInfo = new PageInfo<>(  );
        try {
            pageInfo = roleService.findListPage(role, pageNum, pageSize);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  pageInfo;
    }

    /**
     * 根据id查询
     */
    @RequestMapping("/toAddOrUpdate")
    public String selectById(Integer id,Model model){
        if (id != null){
            Map<String, Object> rstMap = new HashMap<>(  );
            try {
                rstMap = roleService.toUpdate(id);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            model.addAttribute("role", rstMap.get( "role" ));
            model.addAttribute("permissionList", rstMap.get( "permissionList" ));
        }else{
            Map<String, Object> rstMap = new HashMap<>(  );
            try {
                rstMap = roleService.toAdd();
            }catch (Exception ex){
                ex.printStackTrace();
            }
            model.addAttribute("permissionList", rstMap.get( "permissionList" ));
        }
        return "role/edit";
    }

    /**
     * 新增
     */
    @RequestMapping("/add")
    @ResponseBody
    public Integer add(String name, String permissions){
        int rst = -1;
        try {
            rst = roleService.insert(name, permissions);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delById")
    @ResponseBody
    public Integer delById(Integer id){
        int rst = -1;
        try {
            rst = roleService.delById(id);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }


    /**
     * 根据Id删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids){
        int rst = -1;
        try {
            List<String> idList = Arrays.asList(  ids.split( "," ) );
            rst = roleService.delByIds(idList);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }


    /**
     * 根据Id修改
     */
    @RequestMapping("/update")
    @ResponseBody
    public Integer update(Integer id, String name, String permissions){
        int rst = -1;
        try {
            rst = roleService.update(id, name, permissions);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }

    /**
     * 判断名字是否唯一
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public Integer findByName(String name, Integer id)  {
        int rst = -1;
        try {
            rst = roleService.findByName(name, id);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }
}
