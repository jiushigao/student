package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Admin;
import com.student.student.pojo.Login;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface AdminService {
    /**
     * 分页查询所有
     * @param admin
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<Admin> findListPage(Admin admin, Integer pageNum, Integer pageSize);

    /**
     * 查询List不分页
     * @param params
     * @return
     */
    List<Admin> findList(Admin admin);

    /**
     * 根据主键删除数据
     */
    int delById(Integer id);


    /**
     * 根据主键删除数据
     */
    int delByIds(List<String> ids);


    /**
     * 新增
     * @param admin
     * @return
     */
    int add(Admin admin);

    /**
     * 修改数据
     */
    int update(Admin admin);

    /**
     * 根据id查询
     */
    Admin findById(Integer id);


}
