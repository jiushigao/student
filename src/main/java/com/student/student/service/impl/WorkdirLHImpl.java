package com.student.student.service.impl;

import com.student.student.pojo.Workdir;
import com.student.student.service.WorkdirLHService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkdirLHImpl implements WorkdirLHService {

    @Autowired
    private com.student.student.mapper.workdirLHMapper workdirLHMapper;

    @Override
    public List<Workdir> findWorkdirAll(Workdir workdir) {
        return workdirLHMapper.findWorkdirAll(workdir);
    }
    @Override
    public Workdir findWorkdirById(Integer id) {
        return workdirLHMapper.findWorkdirById(id);
    }
    @Override
    public void insertWork(String stuId, String filename) {
        workdirLHMapper.insertWork(stuId,filename);
    }

    @Override
    public void insertWorkdir(String name, String type, String parentid) {
        workdirLHMapper.insertWorkdir(name,type,parentid);
    }
}
