package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.InterviewFunMapper;
import com.student.student.pojo.InterviewFun;
import com.student.student.service.InterviewFunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InterviewFunServiceImpl implements InterviewFunService {

    @Autowired
    private InterviewFunMapper interviewFunMapper;

    @Override
    public PageInfo<InterviewFun> findListPage(InterviewFun interviewFun, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<InterviewFun> list = interviewFunMapper.selectList(interviewFun);
        PageInfo<InterviewFun> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public InterviewFun findOne(InterviewFun interviewFun) {
        return interviewFunMapper.selectOne(interviewFun);
    }

    @Override
    public int add(InterviewFun interviewFun) {
        int rst = interviewFunMapper.countByName( interviewFun );
        if(rst > 0){
            return -2 ;// 说明类型名称已经存在
        }
        return interviewFunMapper.insert(interviewFun);
    }

    @Override
    public int delById(Integer id) {
        return interviewFunMapper.delById(id);
    }


    @Override
    public int delByIds(List<String> ids) {
        return interviewFunMapper.delByIds(ids);
    }

    @Override
    public int update(InterviewFun interviewFun) {
        int rst = interviewFunMapper.countByName( interviewFun );
        if(rst > 0){
            return -2 ;// 说明类型名称已经存在
        }
        return interviewFunMapper.update(interviewFun);
    }

    @Override
    public int countByName(InterviewFun interviewFun ){
        return interviewFunMapper.countByName( interviewFun );
    }
}
