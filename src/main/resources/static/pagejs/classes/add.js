/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;
    //判断操作的结果
    if(null != rst && undefined != rst && rst != '' && rst.length > 0){
        if(rst == -3){
            layer.msg('请填写完整的信息！', {time: 1000});
        } else if(rst == -2){
            layer.msg('类型已经存在，请重新输入！', {time: 1000});
        }else if(rst <= 0) {
            layer.msg('操作异常，请联系管理员！', {time: 1000});
        }
    }
    //验证名字是否已经存在
    $("#name").blur(function () {
        var name=$(this).val();
        $.post("/findsClassName","name="+name, function(data) {
            if (data > 0){
                $("#cname").text("班级名字存在");
                $("#cname").text("");
            }else{
                $("#cname").text("");
            }
        },"Text");
    })

    //产品的选择事件
    form.on('select(productType)', function(data){
        var ptId = data.elem.value;;
        $.post("/term/listByPtId", "ptId="+ptId, function (data1) {
            $("#termId").empty();
            var html = "<option value='0'>请选择学期</option>";
            $(data1).each(function () {
                html += "<option value='" + this.id + "'>" + this.name + "</option>";
            });
            //把遍历的数据放到select表里面
            $("#termId").append(html);
            //从新刷新了一下下拉框
            form.render('select');      //重新渲染
        }, "json");
    });
})