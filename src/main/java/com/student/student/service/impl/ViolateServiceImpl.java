package com.student.student.service.impl;

import com.student.student.mapper.ViolateMapper;
import com.student.student.pojo.Classes;
import com.student.student.pojo.Regulations;
import com.student.student.pojo.Violate;
import com.student.student.service.ViolateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ViolateServiceImpl implements ViolateService {
    @Autowired
    private ViolateMapper violateMapper;

    /**
     * 查询班级名称
     * @return
     */
    @Override
    public List<Classes> ClassName(String name) {
        return violateMapper.ClassName(  name);
    }

    @Override
    public List<Violate> finvio( Integer id,String name) {
        return violateMapper.finvio( id,name);
    }

    @Override
    public List<Violate> finvioid(Integer id) {
        return violateMapper.finvioid(id);
    }

    @Override
    public List<Regulations> finvioReg() {
        return violateMapper.finvioReg();
    }

    @Override
    public void addVio(Violate violate) {
          violateMapper.addVio(violate);
    }

    @Override
    public void delVio(Integer vid) {
        violateMapper.delVio(vid);
    }

}
