package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Admin;
import com.student.student.pojo.Login;
import com.student.student.service.AdminService;
import com.student.student.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 超级管理员
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private LoginService loginService; //登录信息

    //static Logger logger = Logger.getLogger(AdminController.class);

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/list")
    public String list(Admin admin, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Admin> pageInfo = new PageInfo<>(  );
        try{
            pageInfo = adminService.findListPage(admin, pageNum, pageSize);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        model.addAttribute("pageInfo",pageInfo);

        return "admin/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/admin.json")
    @ResponseBody
    public PageInfo listPageJson(String phone, String name, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Admin> pageInfo = new PageInfo<>(  );
        try{
            pageInfo = adminService.findListPage(new Admin(name,phone), pageNum, pageSize);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  pageInfo;
    }

    /**
     * 根据id查询
     */
    @RequestMapping("/toAddOrUpdate")
    public String toAddOrUpdate(Integer id,Model model){
        if (id != null){
            try{
                Admin admin = adminService.findById( id );
                model.addAttribute("admin", admin);
            }catch (Exception ex){
                ex.printStackTrace();
                model.addAttribute( "oper_rst", "-1" );
                return "redirect:/admin/list";
            }
        }
        return "admin/update";
    }

    /**
     * 新增
     */
    @RequestMapping("/add")
    public String add(Admin admin , Model model){
        //非空判断
        if(admin == null || admin.getPhone() == null || admin.getPhone().length() <= 0
                || admin.getName() == null || admin.getName().length() <= 0
                || admin.getPwd() == null || admin.getPwd().length() <= 0){
            model.addAttribute( "oper_rst", -3 ); //-3表示为null，-2表示号码已经存在
        }
        int rst = -1;
        try{
            rst = adminService.add( admin );  //没有传对象是因为这里有两个对象
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "oper_rst", rst );  //失败,rst=-2的时候表示手机号码已经存在
            return "admin/update";
        }else{
            return "redirect:/admin/list";
        }
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delById")
    public String delById(Integer id, Model model){
        int rst = 0;
        try{
            rst = adminService.delById(id);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "oper_rst", "-1" );  //删除失败
        }
        return "redirect:/admin/list";
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        int rst = 0;
        try{
            List<String> idsList  = Arrays.asList(ids.split( "," ));
            rst = adminService.delByIds(idsList);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "oper_rst", "-1" );  //删除失败
        }
        return rst;
    }

    /**
     * 根据Id修改
     */
    @RequestMapping("/update")
    public String update(Admin admin, Model model){
        //非空判断
        if(admin == null || admin.getPhone() == null || admin.getPhone().length() <= 0
                || admin.getName() == null || admin.getName().length() <= 0
                || admin.getPwd() == null || admin.getPwd().length() <= 0
                || admin.getId() == null || admin.getId() <= 0){
            model.addAttribute( "oper_rst", -3 ); //-3表示为null，-2表示号码已经存在
        }
        int rst = 0;
        try{
            rst = adminService.update(admin);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "oper_rst", "-1" );  //修改失败
            return "admin/update";
        }else{
            return "redirect:/admin/list";
        }
    }


    /**
     * 判断手机号码是否已经存在，存在返回>0
     * @param phone
     * @return
     */
    @RequestMapping("/checkPhone")
    @ResponseBody
    public Integer checkPhone(String phone, Integer loginId){
        Login login = new Login(  );
        login.setId( loginId );
        login.setPhone( phone );
        int rst = 0;
        try {
            rst = loginService.cntPhone( login );
        }catch (Exception ex){
            ex.printStackTrace();;
        }
        return rst;
    }
}
