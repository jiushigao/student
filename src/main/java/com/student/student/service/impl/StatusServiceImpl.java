package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.AfficheMapper;
import com.student.student.mapper.StatusMapper;
import com.student.student.pojo.School;
import com.student.student.pojo.Status;
import com.student.student.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {
    @Autowired
    private StatusMapper statusMapper;

    @Override
    public List<Status> findList(Status status) {
        return statusMapper.selectList(status);
    }
    @Override
    public PageInfo<Status> findListPage(Status status, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Status> list = statusMapper.selectList(status);
        PageInfo<Status> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public Status findOne(Status status) {
        return statusMapper.selectOne(status);
    }

    @Override
    public int insertStatus(Status status) {
        return statusMapper.insertStatus(status);
    }

    @Override
    public int updateStatus(Status status) {

        return statusMapper.updateStatus(status);
    }

    @Override
    public int deleteStatusById(Integer id) {

        return statusMapper.deleteStatusById(id);
    }

    @Override
    public int findByStatusName(String name) {

        return statusMapper.findByStatusName(name);
    }
}
