package com.student.student.mapper;

import com.student.student.pojo.Classes;
import com.student.student.pojo.Regulations;
import com.student.student.pojo.Violate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ViolateMapper {

    List<Classes> ClassName(@Param("name") String name);
    List<Violate> finvio(@Param("id") Integer id,@Param("name") String name);
    List<Violate> finvioid(@Param("id") Integer id);
    List<Regulations> finvioReg();
   void addVio(Violate violate);
   void delVio(@Param("vid") Integer vid);
}