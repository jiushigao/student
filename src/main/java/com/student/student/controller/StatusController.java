package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.School;
import com.student.student.pojo.Status;
import com.student.student.service.AgeService;
import com.student.student.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 学员状态管理
 */
@Controller
@RequestMapping("/status")
public class StatusController {
    @Autowired
    private StatusService statusService;


    /**
     * 根据条件查询所有
     */
    @GetMapping("/list")
    public String list(Status status, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Status> pageInfo = statusService.findListPage(status, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);

        model.addAttribute("status",status);
        return "status/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/status.json")
    @ResponseBody
    public PageInfo listPageJson(Status status, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<Status> pageInfo = statusService.findListPage(status, pageNum, pageSize);
        return  pageInfo;
    }


    /**
     * 根据id查询
     */
    @GetMapping("/toAddOrUpdate")
    public String selectById(Integer id,Model model){
        if (id != null){
            Status statusCon = new Status(id);
            Status status = statusService.findOne(statusCon);
            model.addAttribute("status",status);
        }
        return "status/update";
    }

    /**
     * 新增
     */
    @RequestMapping("/add")
    public String add(Status status){
        int cnt = statusService.insertStatus(status);
        return "redirect:/status/list";
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delById")
    public String delById(Integer id){
        int cnt = statusService.deleteStatusById(id);
        return "redirect:/status/list";
    }

    /**
     * 根据Id修改
     */
    @RequestMapping("/update")
    public String update(Status status){
        int cnt = statusService.updateStatus(status);
        return "redirect:/status/list";
    }

    /**
     * 判断状态名字是否唯一
     */
    @RequestMapping("/selByStatusName")
    public void selByStatusName(String name, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        int cnt = statusService.findByStatusName(name);
        if(cnt>0){
            out.print("on");
        }
        out.close();
    }
}
