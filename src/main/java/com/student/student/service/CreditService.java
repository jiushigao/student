package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface CreditService {
    /**
     * 根据条件查询学分
     */
    PageInfo<Credit>  selectList(Credit credit, Integer pageNum, Integer pageSize);
    /**
     * 新增
     * @param credit
     * @return
     */
    int insert(Credit credit);

    /**
     * 删除
     */
    int delete(Integer id);
    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deletes(@Param("ids") List<String> ids);

    /**
     * 查询所有的班级
     * @return
     */
    List<Classes> findByClassName ();

    /**
     * 根据条件查年级
     * @return
     */
    List<Classes> findByProdid(Classes classes);
    /**
     * 查询所有校规
     * @return
     */
    List<Regulations> findRegList();
    /**
     * 查询所有
     * @return
     */
    List<String> findStuList(Integer classesId);

    /**
     * 多条件查询班级扣分情况
     */
    List<Credit> selectPoints(Map<String, Object> condition);

    /**
     * 班级和年纪的下拉框数据
     * @return
     */
    Map<String, Object> selects();
}
