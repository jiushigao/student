/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;

    //判断操作的结果
    if(null != rst && undefined != rst && rst == -1){
        layer.msg('操作异常，请联系管理员！');
    }
    //验证电话号码是否已经存在
    $("#name").blur(function(){
        var name = $("#name").val();
        $.post("/age/selByName","name=" + name ,function (data) {
            if (data=="no") {
                layer.msg('该年龄段已存在！');
                $("#name").val("");
            }
        },"text")
    }) ;
    //表单的提交，非空验证
    $("form").submit(function () {
        var name = $("#name").val();
        if(name == null || name == undefined || name == '' || name.trim().length <= 0){
            layer.msg("昵称不能为空！");
            return false;
        }
    });
})