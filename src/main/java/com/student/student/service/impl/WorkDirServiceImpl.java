package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.WorkDirMapper;
import com.student.student.pojo.Classes;
import com.student.student.pojo.Student;
import com.student.student.pojo.Workdir;
import com.student.student.service.WorkDirService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 作业目录管理
 * 作者：Dora
 * 日期：2019-05-23 14:42
 */
@Service
public class WorkDirServiceImpl implements WorkDirService {
    @Autowired
    private WorkDirMapper workDirMapper;
    /**
     * 得到所有的班级信息
     * @return
     */
    @Override
    public PageInfo<Classes> getAllClasses(Integer pageNum, Integer pageSize) {
        List<Classes> list = new ArrayList<Classes>();
        list.add( new Classes(1L, "T01") );
        list.add( new Classes(2L, "T02") );
        list.add( new Classes(3L, "T03") );
        list.add( new Classes(4L, "T04") );
        list.add( new Classes(5L, "T04") );
        list.add( new Classes(6L, "T04") );
        list.add( new Classes(7L, "T04") );
        list.add( new Classes(8L, "T04") );
        list.add( new Classes(9L, "T04") );
        list.add( new Classes(10L, "T04") );
        if(pageNum == null){ //pageNum  当前的页码     pageSize：每页显示的条数
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 2;
        }
        PageHelper.startPage(pageNum, pageSize);
        List<Classes> sbList = list.subList( (pageNum-1)*pageSize, (pageNum-1)*pageSize + pageSize );
        PageInfo<Classes> pageInfo = new PageInfo<>(sbList);
        return pageInfo;
    }

    @Override
    public List<Workdir> selectByAll(String parentid,String id) {
        return workDirMapper.selectByAll(parentid,id);
    }

    @Override
    public int addWorkDir(String name, String type, String parentid) {
        return workDirMapper.addWorkDir(name,type,parentid);
    }

    @Override
    public int updateById(String id, String name) {
        return workDirMapper.updateById(id,name);
    }
    @Override
    public int delById(String id) {
        return workDirMapper.delById(id);
    }
}
