package com.student.student.mapper;

import com.student.student.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色
 * 作者：Dora
 * 日期：2019-06-08 08:59
 */
public interface RoleMapper {

    /**
     * 根据条件查询所有状态信息
     */
    List<Role> selectList(Role role);

    /**
     * 根据主键查询数据
     */
    Role selectOne(Role role);

    /**
     * 插入数据
     */
    int insert(Role role);

    /**
     * 修改数据
     */
    int update(Role role);

    /**
     * 根据主键删除数据
     */
    int delById(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(@Param( "ids" ) List<String> ids);

    /**
     * 判断名字是否唯一
     */
    int findByName(@Param( "name" ) String name, @Param( "id" ) Integer id);


}
