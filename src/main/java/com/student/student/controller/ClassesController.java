package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Classes;
import com.student.student.pojo.Producttype;
import com.student.student.pojo.Teacher;
import com.student.student.pojo.Term;
import com.student.student.service.ClassesService;
import com.student.student.service.ProducttypeService;
import com.student.student.service.TermsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/classes")
public class ClassesController {

    @Autowired
    private ClassesService classesService;
    @Autowired
    private TermsService termsService;

    @RequestMapping("/list")
    public  String list(Model model, Classes classes, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<Classes> pageInfo = classesService.findList(classes, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("classes",classes);
        return "classes/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/classes.json")
    @ResponseBody
    public PageInfo listPageJson( Classes classes, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo pageInfo = classesService.findList(classes, pageNum, pageSize);
        model.addAttribute("classes",classes);
        return  pageInfo;
    }


    /**
     * 进入新增页面
     * @return
     */
    @RequestMapping("/toAdd")
    public  String toAdd(Model model){
        int rst = -1;
        Map<String, Object>  teacherMap = new HashMap<>();
        try{
            teacherMap = classesService.findTeacherInfo();
            model.addAttribute("teacherMap",teacherMap);
            return "classes/class_upds";
        }catch (Exception ex){
            model.addAttribute("oper_rst",-1);
            ex.printStackTrace();
            return "classes/list";
        }
    }

    @RequestMapping("/findTermid")
    public  void toAdd( HttpServletResponse response ,Integer id){
        PrintWriter out = null;
        try {
            out = response.getWriter();
            List<Term> tlist = termsService.findTermid(id);
            System.out.println(tlist);
            out.print(tlist);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }



    /**
     * 新增
     * @param classes
     * @return
     */
    @RequestMapping("/add")
    public  String add(Model model, Classes classes){
        int rst = classesService.insertClasses(classes);
        if(rst > 0){
            model.addAttribute( "msg", "新增异常，请联系管理员" );
            return "redirect:/classes/list";
        }else{
            return "redirect:/classes/toAdd";
        }

    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delById")
    public  String delById(Integer id){
        int i = classesService.deleteClassesById(id);
        return "redirect:/classes/list";
    }

    /**
     * 批量删除
     * @param request
     * @param model
     * @return
     */
    @RequestMapping("/delByIds")
    public  String delByIds(HttpServletRequest request, Model model){
        String[] checked=  request.getParameterValues("checked");
        classesService.deleteClassesByIds(checked);
        return "redirect:/classes/list";
    }

    /**
     * 进入修改页面
     * @param classes
     * @param model
     * @return
     */
    @RequestMapping("/toUpdate")
    public  String toUpdate(Classes classes, Model model){
        Classes resultEntity = classesService.findOne(classes);
        Map<String,  Object>  teacherMap = classesService.findTeacherInfo();
        model.addAttribute("teacherMap",teacherMap);
        model.addAttribute("classes", resultEntity);
        return  "classes/update";
    }

    /**
     * 修改班级信息
     * @param classes
     * @return
     */
    @RequestMapping("/update")
    public  String update(Classes classes){
        classesService.updateClasses(classes);
        return "redirect:/classes/list";
    }

    /**
     * 查询班级列表
     * @param model
     * @return
     */
    @RequestMapping("/findAllClassName")
    public String findAllClassName(Model model){
        List<Classes> classesList = classesService.findByClassName();
        model.addAttribute("classesList",classesList);
        return "wokdir/classList";
    }


    @RequestMapping("/findsClassName")
    public void  findsClassName(String name, HttpServletResponse response)  {
        try {
            PrintWriter out = response.getWriter();
            int byClassName = classesService.findsClassName(name);
            System.out.println(byClassName);
            out.print(byClassName);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据年级得到班级的id和名字
     * @param termId
     * @return
     */
    @RequestMapping("/findClassesByTerm")
    @ResponseBody
    List<Classes> findClassesByTerm(Integer termId){
        List<Classes> list = new ArrayList<>();
        try {
            list = classesService.findClassesByTerm( termId );
        }catch (Exception ex){
            ex.printStackTrace();;
        }
        return list;
    }

}
