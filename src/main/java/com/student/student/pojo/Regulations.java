/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

/**
 * 校规管理(regulations)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Regulations implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -7638645985909463550L;

    /** 校规id */
    private Integer id;
    /** 名称 */
    private String name;
    /** 积分 */
    private Float integral;

    public Regulations(Integer id, String name, Float integral) {
        this.id = id;
        this.name = name;
        this.integral = integral;
    }
    public Regulations(Integer id) {
        this.id = id;
    }
    public Regulations(Integer id,String name) {
        this.id = id;
        this.name = name;
    }
    public Regulations() {
    }

    /**
     * 获取校规id
     * 
     * @return 校规id
     */
    public Integer getId() {
        return this.id;
    }
    /**
     * 设置校规id
     * 
     * @param id
     *          校规id
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取名称
     * 
     * @return 名称
     */
    public String getName() {
        return this.name;
    }
    /**
     * 设置名称
     * 
     * @param name
     *          名称
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 获取积分
     * 
     * @return 积分
     */
    public Float getIntegral() {
        return this.integral;
    }
    /**
     * 设置积分
     * 
     * @param integral
     *          积分
     */
    public void setIntegral(Float integral) {
        this.integral = integral;
    }
}