package com.student.student.service.impl;

/**
 * School的service实现层
 */

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.SchoolMapper;
import com.student.student.pojo.Regulations;
import com.student.student.pojo.School;
import com.student.student.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SchoolServiceImpl implements SchoolService {

    @Autowired
    private SchoolMapper schoolMapper;

    @Override
    public PageInfo<School> findListPage(School school, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<School> list = schoolMapper.selectList(school);
        PageInfo<School> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public List<School> findList(School school) {
        return schoolMapper.selectList(school);
    }

    @Override
    public School findOne(School school) {
        return schoolMapper.selectOne(school);
    }

    @Override
    public int insertSchool(School school) {
        int rst = schoolMapper.countByName(school);
        if (rst > 0){
            return -2;  //说明名称已存在
        }
        return schoolMapper.insertSchool(school);
    }

    @Override
    public int updateSchool(School school) {
        int rst = schoolMapper.countByName(school);
        if (rst > 0){
            return -2;  //说明名称已存在
        }
        return schoolMapper.updateSchool(school);
    }

    @Override
    public int deleteSchoolById(Integer id) {
        return schoolMapper.deleteSchoolById(id);
    }

    @Override
    public int delByIds(List<String> ids) {
        return schoolMapper.delByIds(ids);
    }

    @Override
    public int countByName(School school) {
        return schoolMapper.countByName(school);
    }

}
