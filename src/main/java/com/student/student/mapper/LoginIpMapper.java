package com.student.student.mapper;

import com.student.student.pojo.LoginIp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LoginIpMapper {

    /**
     * 根据条件查询
     * @param loginIp
     * @return
     */
    List<LoginIp> selectList(LoginIp loginIp);

    /**
     * 新增
     * @param loginIp
     */
    int add(LoginIp loginIp);
}
