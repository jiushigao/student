package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.LoginIp;

import java.util.List;

public interface LoginIpService {

    /**
     * 根据条件查询所有状态信息,不分页
     * @param loginIp
     * @return
     */
    List<LoginIp> findList(LoginIp loginIp);

    /**
     * 根据条件查询所有状态信息,分页
     */
    PageInfo<LoginIp> findListPage(LoginIp loginIp, Integer pageNum, Integer pageSize);

}
