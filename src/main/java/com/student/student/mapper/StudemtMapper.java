package com.student.student.mapper;

import com.student.student.pojo.Student;
import com.student.student.pojo.stu.StudentAdd;
import com.student.student.pojo.stu.StudentList;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 学员信息管理
 * 作者：Dora
 * 日期：2019-05-22 11:05
 */
@Mapper
public interface StudemtMapper {

    /**
     * 查询所有
     * @return
     */
    List<StudentList> findList();

    /**
     * 根据登录id查询对应的学员信息
     * @return
     */
    StudentAdd findStuById(@Param("loginId") Integer loginId);

    /**
     * 插入多条
     * @param stuList
     * @return
     */
    int insertStudents(List<Student> stuList);

    /**
     * 更新多条
     * @param stuList
     * @return
     */
    int updateStudents(List<Student> stuList);

    /**
     * 删除
     * @param stuLoginId
     * @return
     */
    int deleteStudentById(Integer stuLoginId);

    /**
     * 查询对应班级对应学生的职位信息
     * @param condition
     * @return
     */
    List<Student> findStuPosByClasses(Map<String, Object> condition);

    /**
     * 根据班级id查询对应学生
     */
    List<Student> findStuByCid(Integer cid);

    /**
     * 根据条件统计
     * @param idCard
     * @param loginId
     * @return
     */
    int countByCard(@Param( "idCard" ) String idCard, @Param( "stuLoginId" )Integer loginId);

    /**
     * 统计在校生
     * @return
     */
    int countByStatus();

    /**
     * 查询所有学员
     */
    List<String> findStudentByName(@Param("classesId")Integer classesId);

    /**
     * 判断身份证或手机号码是否已经存在
     * @param idCard
     * @param phone
     * @param id
     * @return
     */
    int countByIdCardAPhone(@Param("idCard")String idCard, @Param("phone")String phone, @Param("id")Integer id);

    /**
     * 更新
     * @param student
     * @return
     */
    int update(StudentAdd student);

    /**
     * 插入
     * @param student
     * @return
     */
    int insert(StudentAdd student);
}
