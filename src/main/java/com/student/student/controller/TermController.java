package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Producttype;
import com.student.student.pojo.Term;
import com.student.student.service.ProducttypeService;
import com.student.student.service.TermsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 张文_学期管理
 */
@Controller
@RequestMapping("/term")
public class TermController {
    @Autowired
    private TermsService termService;

    @Autowired
    private ProducttypeService producttypeService;

    /**
     * 根据条件查询所有
     */
    @GetMapping("/list")
    public String list(Term term, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Term> pageInfo = termService.findListPage(term, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);

        model.addAttribute("term",term);
        return "term/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/term.json")
    @ResponseBody
    public PageInfo listPageJson(Term term, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<Term> pageInfo = termService.findListPage(term, pageNum, pageSize);
        return  pageInfo;
    }

    /**
     * 根据id查询单个产品信息进行修改
     * @param id
     * @return
     */
    @RequestMapping("/toAddOrUpdate")
    public String toAddOrUpdate(String id , Model model) {
        if(id==null){
            List<Producttype> list = producttypeService.findList( null );
            model.addAttribute("producttype",list);
            return "term/add";
        }else{
            List<Producttype> list = producttypeService.findList( null );
            model.addAttribute("producttype",list);
            Term term = termService.findTermByIds(id);
            model.addAttribute("term",term);
            return "term/add";
        }
    }

    /**
     * 获取实体类对象进行修改
     * @param term
     */
    @RequestMapping("/update")
    public String update(Term term, Model model) {
        int count = termService.updateTerm(term);
        if(count>0){
            model.addAttribute("err","修改成功");
            return "redirect:/term/list";
        }else {
            model.addAttribute("err","修改失败");
            return "redirect:/term/list";
        }
    }
    /**
     * 获取实体类对象进行新增
     * @param term
     */
    @RequestMapping("/add")
    public String add(Term term, Model model) {
        int count = termService.insertTerm(term);
        if(count>0){
            model.addAttribute("err","新增成功");
            return "redirect:/term/list";
        }else {
            model.addAttribute("err","新增失败");
            return "redirect:/term/list";
        }
    }

    /**
     * 根据id删除产品
     * @param id
     */
    @RequestMapping("/delById")
    public String delById(String id, Model model) {
        int count = termService.deleteTermById(id);
        if(count>0){
            model.addAttribute("err","删除成功");
            return "redirect:/term/list";
        }else {
            model.addAttribute("err","删除失败");
            return "redirect:/term/list";
        }
    }
    /**
     * 根据Id删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        int rst = 0;
        try{
            List<String> idsList  = Arrays.asList(ids.split( "," ));
            rst = termService.deleteTermByIds(idsList);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //删除失败
        }
        return rst;
    }
    /**
     * 验证名字是否已经存在
     * @param name
     * @param model
     * @return 1表示名字已经存在，0表示不存在
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public Integer checkName(String name, Integer id, Model model){
        int  rst = -1;
        try{
            id = (id == null || id == -1) ? null : id;
            rst = termService.countByname( new Term( id, name) );
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(rst > 0){
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * 根据产品id查询
     */
    @RequestMapping("/listByPtId")
    @ResponseBody
    public  List<Term> listByPtId(Integer ptId, Model model){
        Term term = new Term();
        term.setProducttypeid( ptId );
        List<Term> termList = new ArrayList<>();
        try{
            termList = termService.findList( term );
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return termList;
    }
}
