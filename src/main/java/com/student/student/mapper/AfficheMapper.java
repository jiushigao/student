package com.student.student.mapper;

import com.student.student.pojo.Affiche;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AfficheMapper {
    /*查询公告信息*/
    List<Affiche> findAfficheAll(@Param("content") String content);

    void deleteAfficheById(@Param("id") Integer id);

    Affiche findAfficheByIds(@Param("id") Integer id);

     void deleteAfficheByIds(String[] stu);

    void updateAffiche(Affiche affiche);

    void insertAffiche(Affiche affiche);
}
