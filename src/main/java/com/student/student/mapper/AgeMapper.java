package com.student.student.mapper;

import com.student.student.pojo.Age;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AgeMapper {
    /*查询所有*/
    List<Age> selectAll(Age age);
    //删除
    int delAge(@Param("id") Integer id);
    //根据年龄查询
    Age selectById(@Param("id") Integer id);
    //修改
    int updateAge(Age age);
    //新增
    int insertAge(Age age);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(@Param("ids") List<String> ids);
    /**
     * 查询名称是否存在
     */
    int selectByName(Age age);
}
