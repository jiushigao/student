package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.mapper.ProducttypeMapper;
import com.student.student.pojo.Admin;
import com.student.student.pojo.Producttype;
import com.student.student.pojo.Regulations;
import com.student.student.service.ProducttypeService;
import com.student.student.service.RegulationsService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

/**
 * 张文_产品
 */
@Controller
@RequestMapping("/producttype")
public class ProducttypeController {
    @Autowired
    private ProducttypeService producttypeService;

    /**
     * 根据条件查询所有
     */
    @GetMapping("/list")
    public String list(Producttype producttype, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Producttype> pageInfo = producttypeService.findListPage(producttype, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);

        model.addAttribute("producttype",producttype);
        return "producttype/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/producttype.json")
    @ResponseBody
    public PageInfo listPageJson(Producttype producttype, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<Producttype> pageInfo = producttypeService.findListPage(producttype, pageNum, pageSize);
        //数据回显
        model.addAttribute("name",producttype.getName());
        return  pageInfo;
    }

    /**
     * 进入新增或修改页面
     * @param id
     * @return
     */
    @RequestMapping("/toAddOrUpdate")
    public String toAddOrUpdate(Model model,Integer id){
        if(id==null){
            return "producttype/add";
        }else{
            Producttype producttype = new Producttype(id);  //封装查询条件
            Producttype producttypeEntity = producttypeService.findOne(producttype);
            model.addAttribute("producttype",producttypeEntity);
            return "producttype/add";
        }
    }

    /**
     * 获取实体类对象进行修改
     * @param producttype
     */
    @RequestMapping("/update")
    public String update(Producttype producttype, Model model){
        int count = producttypeService.updateProducttype(producttype);
        if(count>0){
            model.addAttribute("err","修改成功");
            return "redirect:/producttype/list";
        }else {
            model.addAttribute("err","修改失败");
            return "redirect:/producttype/list";
        }
    }

    /**
     * 获取实体类对象进行新增
     * @param producttype
     */
    @RequestMapping("/add")
    public String add(Producttype producttype,Model model){
        int count = producttypeService.insertProducttype(producttype);
        if(count>0){
            model.addAttribute("err","添加成功");
            return "redirect:/producttype/list";
        }else {
            model.addAttribute("err","添加失败");
            return "redirect:/producttype/list";
        }
    }
    /**
     * 根据id删除产品
     * @param id
     */
    @RequestMapping("/delById")
    public String delById(Model model,String id){
        int count = producttypeService.deleteProducttypeById(id);
        if(count>0){
            model.addAttribute("err","删除成功");
            return "redirect:/producttype/list";
        }else {
            model.addAttribute("err","删除失败");
            return "redirect:/producttype/list";
        }
    }
    /**
     * 根据Id删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        int rst = 0;
        try{
            List<String> idsList  = Arrays.asList(ids.split( "," ));
            rst = producttypeService.deleteProducttypeByIds(idsList);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //删除失败
        }
        return rst;
    }
    /**
     * 验证名字是否已经存在
     * @param name
     * @param model
     * @return 1表示名字已经存在，0表示不存在
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public Integer checkName(String name, Integer id,  Model model){
        int  rst = -1;
        try{
            id = (id == null || id == -1) ? null : id;
            rst = producttypeService.countByname( new Producttype( id, name ) );
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(rst > 0){
            return 1;
        }else{
            return 0;
        }

    }

}
