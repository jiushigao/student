package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Affiche;
import com.student.student.service.AfficheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/affice")
public class AfficheController {

    @Autowired
    private AfficheService afficheService;

    @GetMapping("/list1")
    public  String list1(Model model,String content, Integer pageNum, Integer pageSize){

        return "affiche/list1";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/affice1.json")
    @ResponseBody
    public  Map<String, Object> listPageJson1(String content, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 2 : pageSize;

        PageInfo pageInfo = afficheService.findAfficheAll(content, pageNum, pageSize);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put( "code", 0 );
        resultMap.put( "msg", "" );
        resultMap.put( "count", pageInfo.getPages() );
        resultMap.put( "data", pageInfo.getList() );

        return  resultMap;
    }

    /**
     * 查询所有信息
     * @param model
     * @return
     */
    @GetMapping("/list")
    public  String list(Model model,String content, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Affiche> pageInfo = afficheService.findAfficheAll(content, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("content",content);
        return "affiche/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/affice.json")
    @ResponseBody
    public PageInfo listPageJson(String content, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo pageInfo = afficheService.findAfficheAll(content, pageNum, pageSize);
        return  pageInfo;
    }

    /**
     * 根据id删除信息
     * @param id
     * @return
     */
    @RequestMapping("/delById")
    public  String delById(Integer id){
        afficheService.deleteAfficheById(id);
        return "redirect:/affice/list";
    }

    /**
     * 进入修改页面
     * @param id
     * @return
     */
    @RequestMapping("/toUpdate")
    public  String findById(Integer id,Model model){
        Affiche afficheByIds = afficheService.findAfficheByIds(id);
        model.addAttribute("afficheByIds",afficheByIds);
        return  "affiche/afficheAdd";
    }

    @RequestMapping("/update")
    public  String update(Affiche affiche,Model model){
        afficheService.updateAffiche(affiche);
        return "redirect:/affice/list";
    }

    @RequestMapping("/delByIds")
    public  String delByIds(HttpServletRequest request, Model model){
        String[] checked=  request.getParameterValues("checked");
        afficheService.deleteAfficheByIds(checked);
        return "redirect:/affice/list";
    }

    @RequestMapping("/toAdd")
    public  String toAdd(){
        return  "affiche/AfficheUpd";
    }


    @RequestMapping("/add")
    public  String add(Affiche affiche,Model model){
        afficheService.insertAffiche(affiche);
        return "redirect:/affice/list";
    }


}
