package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.*;
import com.student.student.pojo.*;
import com.student.student.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CreditServiceImpl implements CreditService {

    @Autowired
    private CreditMapper creditMapper;  //管理员信息
    @Autowired
    private ClassesMapper classesMapper;  //管理员信息
    @Autowired
    private TermMapper termMapper;  //管理员信息
    @Autowired
    private StudemtMapper studemtMapper;  //管理员信息
    @Autowired
    private RegulationsMapper regulationsMapper;  //管理员信息
    @Override
    public PageInfo<Credit> selectList(Credit credit,Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Credit> list = creditMapper.selectList(credit );
        PageInfo<Credit> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public int insert(Credit credit) {
        return creditMapper.insert(credit);
    }

    @Override
    public int delete(Integer id) {
        return creditMapper.delete(id);
    }

    @Override
    public int deletes(List<String> ids) {
        return creditMapper.deletes(ids);
    }

    @Override
    public List<Classes> findByClassName() {
        return classesMapper.findByClassName();
    }

    @Override
    public List<Classes> findByProdid(Classes classes) {
        return classesMapper.findByProdid(classes);
    }

    @Override
    public List<Regulations> findRegList() {
        return regulationsMapper.selectList(null);
    }

    @Override
    public List<String> findStuList(Integer classesId) {
        return studemtMapper.findStudentByName(classesId);
    }

    @Override
    public List<Credit> selectPoints(Map<String, Object> condition) {
        return creditMapper.selectPoints(condition);
    }

    @Override
    public Map<String, Object> selects() {
        Map<String, Object> rstMap = new HashMap<>();
        //班级信息
        List<Classes>  cList = classesMapper.findByClassName();
        rstMap.put( "cList", cList );
        //学期信息
        List<Term> tList = termMapper.findTermAll(null);
        rstMap.put("tList",tList);
        return rstMap;
    }
}
