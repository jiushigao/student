/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

/**
 * 学员类型(studenttype)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Studenttype implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 2285374412627763752L;

    /** 学员类型id */
    private Integer id;

    /** 类型种类 */
    private String name;

    /**
     * 获取学员类型id
     * 
     * @return 学员类型id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置学员类型id
     * 
     * @param id
     *          学员类型id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取类型种类
     * 
     * @return 类型种类
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置类型种类
     * 
     * @param name
     *          类型种类
     */
    public void setName(String name) {
        this.name = name;
    }
}