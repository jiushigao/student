package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.InterviewTypeMapper;
import com.student.student.pojo.InterviewType;
import com.student.student.service.InterviewTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InterviewTypeServiceImpl implements InterviewTypeService {

    @Autowired
    private InterviewTypeMapper interviewTypeMapper;

    @Override
    public PageInfo<InterviewType> selectList(InterviewType interviewType, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<InterviewType> list = interviewTypeMapper.selectList(interviewType);
        PageInfo<InterviewType> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public InterviewType selectOne(InterviewType interviewType) {
        return interviewTypeMapper.selectOne(interviewType);
    }

    @Override
    public int insertInterviewType(InterviewType interviewType) {
        int rst = interviewTypeMapper.countByName(interviewType);
        if(rst > 0){
            return -2 ;// 说明类型名称已经存在
        }
        return interviewTypeMapper.insertInterviewType(interviewType);
    }

    @Override
    public int deleteInterviewType(Integer id) {
        return interviewTypeMapper.deleteInterviewType(id);
    }

    @Override
    public int delByIds(List<String> ids) {
        return interviewTypeMapper.delByIds(ids);
    }

    @Override
    public int updateInterviewType(InterviewType interviewType) {
        int rst = interviewTypeMapper.countByName(interviewType);
        if(rst > 0){
            return -2 ;// 说明类型名称已经存在
        }
        return interviewTypeMapper.updateInterviewType(interviewType);
    }

    @Override
    public int countByName(InterviewType interviewType) {
        return interviewTypeMapper.countByName(interviewType);
    }
}
