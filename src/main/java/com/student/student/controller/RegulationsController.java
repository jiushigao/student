package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Producttype;
import com.student.student.pojo.Regulations;
import com.student.student.service.RegulationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

/**
 * 校规
 * @author 张文
 */
@Controller
@RequestMapping("/regulations")
public class RegulationsController {
    @Autowired
    private RegulationsService regulationsService;


    /**
     * 根据条件查询所有
     */
    @GetMapping("/list")
    public String list(Regulations regulations, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Regulations> pageInfo = regulationsService.findListPage(regulations, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);

        model.addAttribute("regulations",regulations);
        return "regulations/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/regulations.json")
    @ResponseBody
    public PageInfo listPageJson(Regulations regulations, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Regulations> pageInfo = regulationsService.findListPage(regulations, pageNum, pageSize);
        return  pageInfo;
    }

    /**
     * 根据页面返回id判断新增修改
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/toAddOrUpdate")
    public  String toAddOrUpdate(Model model, Integer id){
        if(id==null){
            return "regulations/add";
        }else{
            Regulations regulations = new Regulations(id);
            Regulations regulationsEntity = regulationsService.findRegulationsByIds(regulations);
            model.addAttribute("regulations",regulationsEntity);
            return "regulations/add";
        }
    }

    /**
     * 根据页面返回数据修改校规
     * @param model
     * @param regulations
     * @return
     */
    @RequestMapping("/update")
    public  String update(Model model, Regulations regulations){
        int affected = regulationsService.updateRegulations(regulations);
        if(affected>0){
            model.addAttribute("err","修改成功");
            return "redirect:/regulations/list";
        }else{
            model.addAttribute("err","修改失败");
            return "redirect:/regulations/list";
        }
    }
    /**
     * 根据页面返回数据添加
     * @param model
     * @param regulations
     * @return
     */
    @RequestMapping("/add")
    public  String add(Model model, Regulations regulations){
        int affected = regulationsService.insertRegulations(regulations);
        if(affected>0){
            model.addAttribute("err","新增成功");
            return "redirect:/regulations/list";
        }else{
            model.addAttribute("err","新增失败");
            return "redirect:/regulations/list";
        }
    }

    /**
     * 根据id删除校规信息
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/delById")
    public  String delById(Model model, String id){
        int affected = regulationsService.deleteRegulationsById(id);
        if(affected>0){
            model.addAttribute("err","删除成功");
            return "redirect:/regulations/list";
        }else{
            model.addAttribute("err","删除失败");
            return "redirect:/regulations/list";
        }
    }
    /**
     * 根据Id删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        int rst = 0;
        try{
            List<String> idsList  = Arrays.asList(ids.split( "," ));
            rst = regulationsService.deleteRegulationsByIds(idsList);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //删除失败
        }
        return rst;
    }
    /**
     * 验证名字是否已经存在
     * @param name
     * @param model
     * @return 1表示名字已经存在，0表示不存在
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public Integer checkName(String name, Integer id,  Model model){
        int  rst = -1;
        try{
            id = (id == null || id == -1) ? null : id;
            rst = regulationsService.countByname( new Regulations( id, name ) );
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(rst > 0){
            return 1;
        }else{
            return 0;
        }

    }


}
