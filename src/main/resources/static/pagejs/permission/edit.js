/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;
    var delLevel2Ids = ""; //被删除的id集合
    //验证名字是否已经存在，一级
    //一级目录
    $("[name=name]").blur(function(){
        var name = $(this).val();
        if(name == null || name == undefined || name.length <= 0){
            return;
        }
        var id = $("[name=id]").val() ;
        $.post("/permission/checkName","name=" + name + "&id=" + id + "&parentId=-1",function (data) {
            if (data > 0) {
                $("[name=name]").val($("[name=hiddenName]").val());
                layer.msg('该角色名称已存在！', {time: 700});
            }
        },"text")
    }) ;

    //子级目录
    $("[name=nameLevel2]").live("blur", function(){
        var node = $(this);
        var name = node.text();  //当前输入的
        var hiddenName = node.next().text();  //隐藏的
        if(name == null || name == undefined || name.trim().length <= 0){
            return;
        }
        $(".news_content tr td[name=nameLevel2]").not(this).each(function (i) {
            if(name == $(this).text()){
                node.text(hiddenName);
                layer.msg('该角色名称已存在！', {time: 700});
            }
        });
        node.next().text(name);
    }) ;

    //数据可编辑
    $(".My_edit").live("click",function(){
        $(this).toggle(function(){
            var text=$(this).html();
            $(this).html('<input type="text" class="layui-input layui-table-edit" value="'+text+'">');
            $(this).children("input").val("").focus().val(text);
        },function(){
            $(this).html($(this).children("input").val());
        });
        $(this).trigger("click");
    });
    //数据编辑失去焦点失效
    $(".layui-table-edit").live("blur",function(){
        var text=$(this).val();
        $(this).parent().html(text);
    })

    //删除行
    $("body").on("click",".news_del",function(){  //删除
        var node = $(this);
        layer.confirm('确定删除此信息？',{icon:3, title:'提示信息'},function(index){
            var id = node.attr("value");
            delLevel2Ids += id + ",";
            node.parents("tr").remove();
            layer.close(index);
        });

    })

    //表单的提交，非空验证
    $(".submit").click(function () {
        var name = $("[name=name]").val();
        if(null == name || name == undefined ||  name.trim().length <= 0){
            layer.msg("角色名不能为空！", {time: 700});
            return false;
        }
        var sort = $("[name=sort]").val();
        if(null == sort || sort == undefined ||sort.trim().length <= 0){
            layer.msg("排序不能为空！", {time: 700});
            return false;
        }
        if( $(".news_content tr").length <= 0){
            layer.msg("请输入对应的二级菜单！", {time: 700});
            return false;
        }
        var list = "[";
        var checkLevel2 = true;
        $(".news_content tr").each(function (i) {
            var id2 = $(this).children("td[name=idLevel2]").text();
            var status2 = $(this).children("td[name=idLevel2]").attr("data-status");
            var name2 = $(this).children("td[name=nameLevel2]").text();
            var path2 = $(this).children("td[name=pathLevel2]").text();
            var sort2 = $(this).children("td[name=sortLevel2]").text();
            if(null == name2 || name2 == undefined ||  name2.trim().length <= 0 ||
                null == path2 || path2 == undefined ||  path2.trim().length <= 0 ||
                null == sort2 || sort2 == undefined ||  sort2.trim().length <= 0 ){

                layer.msg("子级权限菜单中，第" + i + "行有空数据，请仔细检查", {time: 700});
                checkLevel2 = false;
                return;
            }
            list += '{"id":"'+id2+'","name":"'+name2+'","path":"'+path2+'","sort":"'+sort2+'","status":"'+status2+'"},';
        });
        if(checkLevel2 == false){
            return false;
        }
        list = list.substring(0, list.length - 1);
        list += ']';

        var parentId = $("[name=id]").val();
        if(parentId == null || parentId == undefined || parentId.trim().length <= 0){ //表示新增
            var permission = '{"name":"'+name+'","sort":"'+sort+'"}';
            $.post("/permission/add","permission="+permission+"&list="+list,function (data) {
                addCallBack(data);
            },"text");
        }else{ //修改
            var permission = '{"id":"'+parentId+'","name":"'+name+'","sort":"'+sort+'"}';
            $.post("/permission/updateLevel1","permission="+permission+"&list="+list+"&delLevel2Ids="+delLevel2Ids,function (data) {
                addCallBack(data);
            },"text");
        }
    });
});
//添加行
function addLeve2() {
    var html = '<tr>' +
        '   <td  hidden="hidden" name="idLevel2" data-status="1"></td>' +
        '    <td class="My_edit" name="nameLevel2"></td>' +
        '    <td class="My_edit" hidden="hidden" name="hiddenNameLevel2"></td>' +
        '    <td class="My_edit" name="pathLevel2"></td>' +
        '    <td class="My_edit" name="sortLevel2"></td>' +
        '    <td>' +
        '        <a class="layui-btn layui-btn-danger layui-btn-mini news_del">' +
        '            <i class="layui-icon"></i> 删除' +
        '        </a>' +
        '    </td>' +
        '</tr>';
    $(".news_content").append(html);
}
//新增或修改的回调函数
function  addCallBack(data) {
    if (data > 0) {
        layer.msg('成功！', {time: 700}, function () {
            location.href = "/permission/list";
        });
    }else{
        if(data == -5){
            layer.msg('有数据为空，请检查！', {time: 700});
        }else if(data == -6){
            layer.msg('一级权限目录名称已经存在！', {time: 700});
        }else{
            layer.msg('系统异常，请联系管理员！', {time: 700});
        }
    }
}