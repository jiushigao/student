package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.TermMapper;
import com.student.student.pojo.Term;
import com.student.student.service.TermsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学期管理
 * 作者：张文
 */
@Service
public class TermsServiceImpl implements TermsService {
    @Autowired
    private TermMapper termMapper;

    @Override
    public PageInfo<Term> findListPage(Term term, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Term> list = termMapper.findTermAll(term);
        PageInfo<Term> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public List<Term> findList(Term term) {
        return termMapper.findTermAll(term);
    }

    @Override
    public int deleteTermById(String id) {
        return termMapper.deleteTermById(id);
    }

    @Override
    public int deleteTermByIds(List<String> ids) {
        return termMapper.deleteTermByIds(ids);
    }

    @Override
    public Term findTermByIds(String id) {
        return termMapper.findTermByIds(id);
    }

    @Override
    public int updateTerm(Term term) {
        return termMapper.updateTerm(term);
    }

    @Override
    public int insertTerm(Term term) {
        return termMapper.insertTerm(term);
    }

    @Override
    public int countByname(Term term) {
        return termMapper.countByname(term);
    }


    @Override
    public List<Term> findTermid(Integer id) {
        return termMapper.selectTermid(id);
    }
}
