package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Student;
import com.student.student.pojo.Term;
import com.student.student.pojo.stu.StudentAdd;
import com.student.student.pojo.stu.StudentList;

import java.util.List;
import java.util.Map;

/**
 * 作者：Dora
 * 日期：2019-05-22 11:53
 * 学生信息管理
 */
public interface StudentService {

    /**
     * 查询所有
     * @return
     */
    PageInfo<StudentList> findStudentAll(Student student, Integer pageNum, Integer pageSize);

    /**
     * 插入
     * @param student
     * @return
     */
    int  add(StudentAdd student);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteStudentById(Integer id);

    /**
     * 根据条件统计
     * @param idCard
     * @param loginId
     * @return
     */
    int  countByCard(String idCard, Integer loginId);

    /**
     * 进入新增或修改页面
     * @return
     */
    Map<String, Object> toAddOrupdate(Integer loginId);

    /**
     * 修改
     * @param student
     * @return
     */
    int update(StudentAdd student);
}
