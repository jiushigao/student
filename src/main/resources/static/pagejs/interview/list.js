/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;

    //判断操作的结果
    if(null != rst && undefined != rst && rst == -1){
        layer.msg('操作异常，请联系管理员！');
    }
    //全选
    form.on('checkbox(allChoose)', function(data){
        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
        child.each(function(index, item){
            item.checked = data.elem.checked;
        });
        form.render('checkbox');
    });

    //分页
    laypage({
        cont: 'page',
        pages: pages, //总页数
        groups: 5, //连续显示分页数
        limit: limit,  //每页显示条数
        jump: function (obj, first) {
            if(!first){
                var typeName = $("[name=typeName]").val();
                $.post("/interview/interview.json", "pageNum="+obj.curr+"&pageSize="+obj.limit,function(data){
                    $(".news_content").html('');
                    var info = '';
                    $(data.list).each(function(){
                        info += '<tr>';
                        info += '<td><input name="checked" lay-skin="primary" lay-filter="choose" type="checkbox"><div class="layui-unselect layui-form-checkbox"><span>勾选</span><i class="layui-icon"></i></div></td>';
                        info += '<td>' + this.classes.name + '</td>';
                        info += '<td>' + this.stuName + '</td>';
                        info += '<td>' + this.interviewType.typeName + '</td>';
                        info += '<td>' + this.interviewFun.funName + '</td>';
                        info += '<td>' + this.content + '</td>';
                        info += '<td>' + this.interviewTime + '</td>';
                        info += '<td> <a class="layui-btn layui-btn-mini news_edit" href="/interview/toAddOrUpdate?id='+this.id+'"><i class="iconfont icon-edit"></i> 编辑</a>';
                        info += '<a class="layui-btn layui-btn-danger layui-btn-mini news_del splitJS" data-id="1"  value="' + this.id + '"><i class="layui-icon"></i> 删除</a>';
                        info += '</td></tr>';
                    });
                    $(".news_content").html(info);
                    $("[name=typeName]").html(typeName);
                    $('.news_list thead input[type="checkbox"]').prop("checked",false);
                    form.render();
                },"json");
            }
        }
    });

    //批量删除
    $(".batchDel").click(function(){
        var $checkbox = $('.news_list tbody input[type="checkbox"][name="checked"]');
        var $checked = $('.news_list tbody input[type="checkbox"][name="checked"]:checked');
        if($checkbox.is(":checked")){
            layer.confirm('确定删除选中的信息？',{icon:3, title:'提示信息'},function(index){
                var ids = "";
                for(var j = 0; j < $checked.length; j++){
                    var id = $checked.eq(j).parents("tr").find(".news_del").attr("value");
                    ids += id + ",";
                }
                ids = ids.substring(0, ids.length - 1);
                $.post("/interview/delByIds", "ids="+ ids, function (data) {
                    if(data > 0){
                        layer.msg('删除成功！', {time: 1000}, function(){
                            location.href= "/interview/list";
                        });
                    }else{
                        layer.msg("删除失败，请联系管理员", {time: 1000});
                    }
                }, "text");
            })
        }else{
            layer.msg("请选择需要删除的信息");
        }
    })

    //单个删除
    $("body").on("click",".news_del",function(){  //删除
        var id = $(this).attr("value");
        layer.confirm('确定删除此信息？',{icon:3, title:'提示信息'},function(index){

            $.post("/interview/delById", "id="+ id, function (data) {
                if(data > 0){
                    layer.msg('删除成功！', {time: 1000}, function(){
                        location.href= "/interview/list";
                    });
                }else{
                    layer.msg("删除失败，请联系管理员", {time: 1000});
                }
            }, "text");

        });
    })

    //绑定查询条件
    $.post("/interview/getSelectDate.json", "", function (data) {
        //班级,classesId
        var classesId = $("[name=classesId]").val(); //班级
        var html = "<option value='0'>请选择班级</option>";
        $(data.cList).each(function () {
            if(classesId == this.id){
                html += "<option value='" + this.id + "' selected='selected'>" + this.name + "</option>";
            }else{
                html += "<option value='" + this.id + "'>" + this.name + "</option>";
            }
        });
        $("#classes").empty();
        $("#classes").append(html);
        //类型，typeList
        var type = $("[name=type]").val();
        html = "";
        html = "<option value='0'>请选择类型</option>";
        $(data.typeList).each(function () {
            if(type == this.id){
                html += "<option value='" + this.id + "' selected='selected'>" + this.typeName + "</option>";
            }else{
                html += "<option value='" + this.id + "'>" + this.typeName + "</option>";
            }

        });
        $("#type").empty();
        $("#type").append(html);
        //方式，funList
        var fun = $("[name=fun]").val();
        html = "";
        html = "<option value='0'>请选择类型</option>";
        $(data.funList).each(function () {
            if(fun == this.id){
                html += "<option value='" + this.id + "' selected='selected'>" + this.funName + "</option>";
            }else{
                html += "<option value='" + this.id + "'>" + this.funName + "</option>";
            }
        });
        $("#fun").empty();
        $("#fun").append(html);
    }, "json")

    //类型的选中事件
    $("#type").change(function(){
        $("[name=type]").val(this.value);
    });
    //方式的选中事件
    $("#fun").change(function(){
        $("[name=fun]").val(this.value);
    });
    //班级的选中事件
    $("#classes").click(function(){
        var cId=this.value;
        $("[name=classesId]").val(cId);
        bindStuDate(cId);
    });
    //学员的选中事件
    $("#stuLogin").change(function(){
        $("[name=stuLoginId]").val(this.value);
    });

    var cId=$("[name=classesId]").val();
    if(cId > 0){
        bindStuDate(cId);
    }
    //绑定学员列表
    function bindStuDate(cId) {
        var stuLoginId = $("[name=stuLoginId]").val();
        $.post("/interview/getStu", "cId="+cId, function (data) {
            $("#stuLogin").empty();
            var html = "<option value='0'>请选择学员</option>";
            $(data).each(function () {
                if(stuLoginId == this.stuLoginId){
                    html += "<option value='" + this.stuLoginId + "' selected='selected'>" + this.name + "</option>";
                }else{
                    html += "<option value='" + this.stuLoginId + "'>" + this.name + "</option>";
                }
            });
            //把遍历的数据放到select表里面
            $("#stuLogin").append(html);
            //从新刷新了一下下拉框
            form.render('select');      //重新渲染
        }, "json");
    }
})