package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.*;
import com.student.student.service.ClassesService;
import com.student.student.service.InterviewFunService;
import com.student.student.service.InterviewService;
import com.student.student.service.InterviewTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 学生访谈
 * @author 资凯
 */
@Controller
@RequestMapping("/interview")
public class InterviewController {

    @Autowired
    private InterviewService interviewService; //访谈

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/list")
    public String list(Interview interview, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Interview> pageInfo = interviewService.findListPage(interview, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);

        model.addAttribute("interview",interview);
        return "interview/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/interview.json")
    @ResponseBody
    public PageInfo listPageJson(Interview interview, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo pageInfo = interviewService.findListPage(interview, pageNum, pageSize);
        return  pageInfo;
    }

    /**
     * 新增访谈记录
     */
    @RequestMapping("/add")
    public String add(Interview interview){
        int cnt = interviewService.insertInterview(interview);
        //return "redirect:/interview/selInterviewByStuId?stuLoginId=" + interview.getStuLoginId();
        return "redirect:/interview/list";
    }

    /**
     * 进入编辑页面
     */
    @RequestMapping("/toAddOrUpdate")
    public String toAddOrUpdate(Integer id,Integer stuLoginId,Model model){
       /* if (id != null){
            Interview interview1 = interviewService.findInterviewById(id);
            model.addAttribute("interview",interview1);
        }*/

        Map<String, Object> rstMap = interviewService.toAdd();
        model.addAttribute("cList",rstMap.get( "cList" ));
        model.addAttribute("typeList",rstMap.get( "typeList" ));
        model.addAttribute("funList",rstMap.get( "funList" ));

        model.addAttribute("stuLoginId",stuLoginId);
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//可以方便地修改日期格式
        String nowTime = dateFormat.format( now );
        model.addAttribute("nowTime",nowTime);
        return "interview/add";
    }

    /**
     * 根据访谈id删除访谈记录
     */
    @RequestMapping("/delById")
    @ResponseBody
    public Integer delById(Integer id,Integer stuLoginId,Model model){
        int rst = -1;
        try{
            rst = interviewService.delById(id);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 根据Id批量删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        if(ids == null || ids.trim().length() <= 0){
            return -1;
        }
        int rst = -1;
        try{
            rst = interviewService.delByIds( Arrays.asList(ids.split( "," )));
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 根据访谈id修改访谈记录
     */
    @RequestMapping("/update")
    public String update(Interview interview){
        int cnt = interviewService.updateInterviewById(interview);
        return "redirect:/interview/selInterviewByStuId?stuLoginId=" + interview.getStuLoginId();
    }

    /**
     * 根据学员id查询访谈记录
     */
    @RequestMapping("/selInterviewByStuId")
    public String selInterviewByStuId(Integer stuLoginId,String classesName,String stuName,String typeName, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        Interview interview = new Interview(stuLoginId,classesName,stuName,typeName);
        PageInfo<Interview> pageInfo = interviewService.findListPage(interview, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("interview",interview);
        if (stuLoginId != null){
            model.addAttribute("stuLoginId",stuLoginId);
        }
        return "interview/list";
    }


    /**
     * 班级学生联动
     */
    @ResponseBody
    @RequestMapping("/getStu")
    public List<Student> getStu(Integer cId){
        return interviewService.findStudentByCid(cId);
    }

    /**
     * 列表页下拉框查询条件
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping("/getSelectDate.json")
    public Map<String, Object> getSelectDate( Model model){
        Map<String, Object> rstMap = interviewService.toAdd();
        model.addAttribute("cList",rstMap.get( "classesList" ));
        model.addAttribute("typeList",rstMap.get( "typeList" ));
        model.addAttribute("funList",rstMap.get( "funList" ));
        return rstMap;
    }

    /**
     * 查看详情
     */
    @RequestMapping("/show")
    public String show(Integer id,Model model){
        if (id != null && id != null){
            Interview one = new Interview(id);
            try{
                Interview interview = interviewService.selectOne(one);
                model.addAttribute("interview",interview);
            }catch (Exception ex){
                ex.printStackTrace();
                model.addAttribute( "rst", "-1" );
            }
        }
        return "interview/show";
    }


}
