package com.student.student.mapper;

import com.student.student.pojo.Permission;
import com.student.student.pojo.RolePermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 权限
 * 作者：Dora
 * 日期：2019-06-08 08:59
 */
public interface RolePermissionMapper {
    /**
     * 批量删除
     * @param roleId
     * @return
     */
    int delByRoleId(@Param("roleId") Integer roleId);


    /**
     * 插入数据
     */
    int inserts(List<RolePermission> list);

}
