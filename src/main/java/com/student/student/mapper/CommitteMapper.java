package com.student.student.mapper;

import com.student.student.pojo.Committe;
import com.student.student.pojo.InterviewFun;
import com.student.student.pojo.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 班委管理
 * @author 资凯
 */
public interface CommitteMapper {

    /**
     * 返回单个实体
     * @param committe
     * @return
     */
    Committe selectOne(Committe committe);

    /**
     * 根据条件查询班级职位
     */
    List<Committe> selectList(Committe committe);

    /**
     * 新增班级职位
     */
    int insertCommitte(Committe committe);

    /**
     * 删除班级职位
     */
    int deleteCommitteById(Integer id);

    /**
     * 批量删除
     */
    int delByIds(@Param("ids") List<String> ids);

    /**
     * 修改班级职位名称
     */
    int updateCommitteById(Committe committe);

    /**
     * 根据学生id修改班级职位
     */
    int updateCommitteByStu(Integer stuId, Integer zwId);

    /**
     * 统计类型名是否已经存在
     * @param committe
     * @return
     */
    int countByName(Committe committe);


}
