/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

import java.util.List;

/**
 * 学期，关联产品表(term)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Term implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -4877209145049980459L;

    /** 主键 */
    private Integer id;

    /** 学期名 */
    private String name;

    /** 对应产品类型 */
    private Integer producttypeid;

    /** 产品对象 */
    private Producttype producttype;

    public Term() {}
    public Term(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    /** 获取产品对象 */
    public Producttype getProducttype() {
        return producttype;
    }

    public void setProducttype(Producttype producttype) {
        this.producttype = producttype;
    }

    /**
     * 获取主键
     * 
     * @return 主键
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置主键
     * 
     * @param id
     *          主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取学期名
     * 
     * @return 学期名
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置学期名
     * 
     * @param name
     *          学期名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取对应产品类型
     * 
     * @return 对应产品类型
     */
    public Integer getProducttypeid() {
        return this.producttypeid;
    }

    /**
     * 设置对应产品类型
     * 
     * @param producttypeid
     *          对应产品类型
     */
    public void setProducttypeid(Integer producttypeid) {
        this.producttypeid = producttypeid;
    }
}