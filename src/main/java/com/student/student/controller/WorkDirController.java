package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Age;
import com.student.student.pojo.Classes;
import com.student.student.pojo.Workdir;
import com.student.student.service.WorkDirService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 作业目录管理
 * 作者：Dora
 * 日期：2019-05-23 14:40
 */
@Controller
@RequestMapping("/workDir")
public class WorkDirController {

    @Autowired
    private WorkDirService workDirService;

    /**
     * 查询所有信息
     * @param model
     * @return
     */
    @GetMapping("/selectAll")
    public  String selectAll(Model model, Integer pageNum, Integer pageSize){
        PageInfo pageInfo = workDirService.getAllClasses(pageNum, pageSize);
        model.addAttribute("pageInfo", pageInfo);
        return  "wokdir/list";
    }

    /**
     * 查询所有信息
     * @param pageNum
     * @return
     */
    @RequestMapping("/workDir.json")
    @ResponseBody
    public  PageInfo selectJson(Integer pageNum, Integer pageSize){
        PageInfo pageInfo = workDirService.getAllClasses(pageNum, pageSize);
        return  pageInfo;
    }

    @RequestMapping("/selectGrade")
    public String selectGrade(Model model,String parentid){

        System.out.println("班级："+parentid);
        List<Workdir> workdirs = workDirService.selectByAll(parentid,null);

        model.addAttribute("workdirs",workdirs);
        return "wokdir/gradename";
    }

    /**
     * 根据Id查询
     * @param id
     * @param parentid
     * @param model
     * @return
     */
    @RequestMapping("/deleteById")
    public String deleteById(String id,String parentid,Model model){
        int count = workDirService.delById(id);
        if (count>0){
            System.out.println("删除成功！");
        }else {
            System.out.println("删除失败！");
        }
        List<Workdir> workdirs = workDirService.selectByAll(parentid,null);
        model.addAttribute("workdirs",workdirs);
        return "wokdir/gradename";
    }

    /**
     * 根据Id查询
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/selectById")
    public String selectById(String id,Model model){
        List<Workdir> workdirs = workDirService.selectByAll(null,id);
        model.addAttribute("selsecById",workdirs);
        return "wokdir/edit";
    }

    @RequestMapping("/updById")
    public String updById(String id,String name,String parentid,Model model){
       int count = workDirService.updateById(id,name);
        System.out.println("状态："+count);
        List<Workdir> workdirs = workDirService.selectByAll(parentid,null);
        model.addAttribute("workdirs",workdirs);
        return "wokdir/gradename";
    }
}
