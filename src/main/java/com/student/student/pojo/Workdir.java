/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

/**
 * 作业目录(workdir)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Workdir implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -6638491236115586718L;

    /** 主键 */
    private Integer id;

    /** 名称 */
    private String name;

    /** 类型:0年级1书2目录3作业内容 */
    private Integer type;

    /** 父id，如果是年级那么对应班级id，其他的就对应本表的id */
    private Integer parentid;

    /**
     * 获取主键
     * 
     * @return 主键
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置主键
     * 
     * @param id
     *          主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名称
     * 
     * @return 名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置名称
     * 
     * @param name
     *          名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取类型:0年级1书2目录3作业内容
     * 
     * @return 类型
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 设置类型:0年级1书2目录3作业内容
     * 
     * @param type
     *          类型
     */
    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }
}