package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.InterviewFun;
import com.student.student.pojo.InterviewType;

import java.util.List;

public interface InterviewTypeService {

    /**
     *根据条件查询所有
     */
    PageInfo<InterviewType> selectList(InterviewType interviewType, Integer pageNum, Integer pageSize);

    /**
     * 查询单个
     */
    InterviewType selectOne(InterviewType interviewType);

    /**
     * 新增
     */
    int insertInterviewType(InterviewType interviewType);

    /**
     * 删除
     */
    int deleteInterviewType(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(List<String> ids);

    /**
     * 修改
     */
    int updateInterviewType(InterviewType interviewType);

    /**
     * 统计类型名称是否已经存在
     * @param interviewType
     * @return
     */
    int countByName(InterviewType interviewType);
}
