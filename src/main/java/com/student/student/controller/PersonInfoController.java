package com.student.student.controller;

import com.student.student.pojo.Login;
import com.student.student.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 个人信息修改
 */
@Controller
@RequestMapping("/person")
public class PersonInfoController {

    @Autowired
    private LoginService loginService;

    @RequestMapping("/info")
    public  String toPersonList(HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        Login login = (Login)session.getAttribute("login");
        model.addAttribute("login",login);
        return "person/info";
    }

    @RequestMapping("/updatePwd")
    public  String  loginPwd(Integer id, String newPwd, Model model, HttpServletRequest request){
        int rst = 0;
        Login login = (Login)request.getSession().getAttribute("login");
        try{
            rst = loginService.updatePwd(login.getId(), newPwd);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        if(rst > 0){
            return  "person/exit";
        }else{
            model.addAttribute( "rst", rst);
            return  "person/toUpdatePwd";
        }
    }

    @RequestMapping("/toUpdatePwd")
    public  String toPersonpwd(HttpServletRequest request, Model model){
        Login login = (Login)request.getSession().getAttribute("login");
        if(login == null || login.getPhone() == null){  //如果没有登录，那么直接进入登录页面
            return "login";
        }
        return "person/update_pwd";
    }
}
