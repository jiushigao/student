package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.*;
import com.student.student.pojo.*;
import com.student.student.service.InterviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 学员访谈
 * @author 资凯
 */
@Service
public class InterviewServiceImpl implements InterviewService {

    @Autowired
    private InterviewMapper interviewMapper;
    @Autowired
    private InterviewTypeMapper interviewTypeMapper;
    @Autowired
    private InterviewFunMapper interviewFunMapper;
    @Autowired
    private StudemtMapper studemtMapper; //学员
    @Autowired
    private ClassesMapper classesMapper; //班级

    @Override
    public PageInfo<Interview> findListPage(Interview interview, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Interview> list = interviewMapper.selectListAStuName(interview);
        for (Interview item : list) {
            item.setContent( item.getContent().length() > 15 ? item.getContent().substring( 0,15 ) + "..." : item.getContent() );
        }
        PageInfo<Interview> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public List<Interview> findList(Interview interview) {
        List<Interview> list = interviewMapper.selectListAStuName(interview);
        for (Interview item : list) {
            item.setContent( item.getContent().length() > 15 ? item.getContent().substring( 0,15 ) + "..." : item.getContent() );
        }
        return list;
    }

    @Override
    public int insertInterview(Interview interview) {
        return interviewMapper.insert(interview);
    }


    @Override
    public List<Student> findStudentByCid(Integer cid) {
        return studemtMapper.findStuByCid(cid);
    }

   /* @Override
    public Interview findInterviewById(Integer id) {
        return interviewMapper.findInterviewById(id);
    }*/

    @Override
    public int delById(Integer id) {
        return interviewMapper.delById(id);
    }

    @Override
    public int delByIds(List<String> ids) {
        return interviewMapper.delByIds(ids);
    }

    @Override
    public int updateInterviewById(Interview interview) {
        return interviewMapper.update(interview);
    }

    @Override
    public Interview selectOne(Interview interview) {
        return interviewMapper.selectOne(interview);
    }


    /*@Override
    public Map<String, Object> toUpdate(Interview id) {
        //根据id得到访谈
        Interview interview1 = interviewMapper.selectOne(id);
        return null;
    }*/
    @Override
    public Map<String, Object> toAdd() {
        Map<String, Object> rstMap = new HashMap<>();
        //班级信息
        List<Classes>  cList = classesMapper.findByClassName();
        rstMap.put( "cList", cList );
        //访谈类型
        List<InterviewType> typeList =  interviewTypeMapper.selectList(null);
        rstMap.put( "typeList", typeList );
        //访谈方式
        List<InterviewFun>  funList = interviewFunMapper.selectList(null);
        rstMap.put( "funList", funList );
        return rstMap;
    }
}
