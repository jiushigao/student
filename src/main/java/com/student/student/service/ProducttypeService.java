package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.mapper.ProducttypeMapper;
import com.student.student.pojo.Producttype;
import com.student.student.pojo.Regulations;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ProducttypeService {

    /**
     * 查询全部产品,分页
     * @param pt
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<Producttype> findListPage(Producttype pt, Integer pageNum, Integer pageSize);

    /**
     * 查询全部产品,不分页
     * @param producttype
     * @return
     */
    List<Producttype> findList(Producttype producttype);
    /**
     * 根据id删除产品
     * @param id
     */
    int deleteProducttypeById(String id);
    /**
     * 根据id删除产品
     * @param
     */
    int deleteProducttypeByIds(List<String> ids);

    /**
     * 根据条件查询返回单个实体
     * @param producttype
     * @return
     */
    Producttype findOne(Producttype producttype);

    /**
     * 获取实体类对象进行修改
     * @param producttype
     */
    int updateProducttype(Producttype producttype);

    /**
     * 获取实体类对象进行新增
     * @param producttype
     */
    int insertProducttype(Producttype producttype);
    /**
     * 根据条件统计
     * @param producttype
     * @return
     */
    int countByname(Producttype producttype);

}
