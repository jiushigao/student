package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.ClassesMapper;
import com.student.student.mapper.ProducttypeMapper;
import com.student.student.mapper.TeacherMapper;
import com.student.student.pojo.Admin;
import com.student.student.pojo.Classes;
import com.student.student.pojo.Producttype;
import com.student.student.pojo.Teacher;
import com.student.student.service.ClassesService;
import com.student.student.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ClassesServiceImpl implements ClassesService {

    @Autowired
    private ClassesMapper classesMapper;    //班级信息查询
    @Autowired
    private TeacherMapper teacherMapper; //老师信息查询
    @Autowired
    private ProducttypeMapper producttypeMapper; //产品

    @Override
    public PageInfo<Classes> findList(Classes classes, Integer pageNum, Integer pageSize) {

        PageHelper.startPage(pageNum, pageSize);
        List<Classes> list = classesMapper.selectByConAll(classes);
        PageInfo<Classes> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public Classes findOne(Classes classes) {
        return classesMapper.selectByConOne(classes);
    }

    /**
     * 得到所有的老师信息
     * @return
     */
    @Override
    public Map<String,  Object> findTeacherInfo() {
        List<Teacher> list = teacherMapper.selectList( null );

        List<Teacher> lifeTeacher = new ArrayList<>();//班主任
        List<Teacher> technologyTeacher = new ArrayList<>();//教员
        for (Teacher teacher : list) {
            if(teacher.getRoleId() == 2){ //班主任
                lifeTeacher.add( teacher );
            }else{
                technologyTeacher.add( teacher );
            }
        }

        List<Producttype> ptList = producttypeMapper.selectList( null );
        Map<String,  Object> map = new HashMap<>();
        map.put( "lifeTeacher", lifeTeacher );
        map.put( "technologyTeacher", technologyTeacher );
        map.put( "ptList", ptList );
        return map;
    }


    @Override
    public int insertClasses(Classes classes) {
        return classesMapper.insertClasses(classes);
    }

    @Override
    public int deleteClassesById(Integer id) {
       return classesMapper.deleteClassesById(id);
    }


    @Override
    public int updateClasses(Classes classes) {
        return classesMapper.updateClasses(classes);
    }

    /**
     * 查找所有的班级名称
     * @return
     */
    @Override
    public List<Classes> findByClassName() {
        return classesMapper.findByClassName();
    }

    @Override
    public void deleteClassesByIds(String[] stu) {
        classesMapper.deleteClassesByIds(stu);
    }

    @Override
    public int findsClassName(String name) {
        return classesMapper.countByName(name);
    }

    /**
     * 根据年级得到班级的id和名字
     * @param termId
     * @return
     */
    @Override
    public List<Classes> findClassesByTerm(Integer termId) {
        return classesMapper.findClassesByTerm( termId );
    }
}
