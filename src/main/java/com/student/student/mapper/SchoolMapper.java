package com.student.student.mapper;

import com.student.student.pojo.InterviewFun;
import com.student.student.pojo.School;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SchoolMapper {

    /**
     * 根据条件查询所有校区信息,返回List
     */
    List<School> selectList(School school);

    /**
     * 根据主键查询数据，返回单个实体
     */
    School selectOne(School school);

    /**
     * 插入数据
     */
    int insertSchool(School school);

    /**
     * 修改数据
     */
    int updateSchool(School school);

    /**
     * 根据主键删除数据
     */
    int deleteSchoolById(Integer id);

    /**
     * 批量删除
     */
    int delByIds(@Param("ids") List<String> ids);

    /**
     * 统计校区名是否已经存在
     */
    int countByName(School school);
}
