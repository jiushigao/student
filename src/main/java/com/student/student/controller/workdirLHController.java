package com.student.student.controller;

import com.student.student.pojo.Workdir;
import com.student.student.service.WorkdirLHService;
import org.mybatis.logging.Logger;
import org.mybatis.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Controller
@RequestMapping("/workdirLH")
public class workdirLHController {
    private static final Logger LOGGER = LoggerFactory.getLogger(workdirLHController.class);

    @Autowired
    private WorkdirLHService workdirLHService;

    /**
     * 查询全部作业列表
     * @return
     */
    @RequestMapping("/selAll")
    public String findWorkdirAll(Workdir workdir, Model model) {
        if(workdir.getType()==null||workdir.getType()==-1){
            workdir.setType(0);
            workdir.setParentid(76);
            workdir.setId(null);
        }
        if(workdir.getId()!=null){
            Workdir workdir1= workdirLHService.findWorkdirById(workdir.getId());
            workdir.setParentid(workdir1.getParentid());
        }

        if(workdir.getType()==5){
            Workdir workdir1= workdirLHService.findWorkdirById(workdir.getParentid());
            workdir.setParentid(workdir1.getParentid());
            workdir.setType(4);
        }

        List<Workdir> workdirs=workdirLHService.findWorkdirAll(workdir);
        model.addAttribute("workdirs",workdirs);
        model.addAttribute("Name",workdir.getName());
        model.addAttribute("type",workdir.getType());
        model.addAttribute("parentid",workdir.getParentid());
        return "workdirl/list";
    }

    /**
     * 批量下载
     * @param response
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/down")
    public String down(HttpServletResponse response,HttpServletRequest request) throws Exception {
        String[]   list = request.getParameterValues("checked");//获取复选框文件
        if(list.length>0){
           List files = new ArrayList();//保存要要下载的文件
           File file=null;
           for (String item :list) {
               file = new File("H:/git/ss/src/main/resources/file/"+item);
               System.out.println( file.getName());
               files.add(file);
           }
           downLoadFiles(files, response);
       }else {

        }
        return null;
    }

    public static HttpServletResponse downLoadFiles(List<File> files, HttpServletResponse response) throws Exception {

        try {
            // List<File> 作为参数传进来，就是把多个文件的路径放到一个list里面
            // 创建一个临时压缩文件

            // 临时文件可以放在CDEF盘中，但不建议这么做，因为需要先设置磁盘的访问权限，最好是放在服务器上，方法最后有删除临时文件的步骤

            String zipFilename = "D:/tempFile.zip";
            File file = new File(zipFilename);
            file.createNewFile();
            if (!file.exists()) {
                file.createNewFile();
            }
            response.reset();
            // response.getWriter()
            // 创建文件输出流
            FileOutputStream fous = new FileOutputStream(file);
            ZipOutputStream zipOut = new ZipOutputStream(fous);
            zipFile(files, zipOut);
            zipOut.close();
            fous.close();
            return downloadZip(file, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /**
     * 把接受的全部文件打成压缩包
     *
     */
    public static void zipFile(List files, ZipOutputStream outputStream) {
        int size = files.size();
        for (int i = 0; i < size; i++) {
            File file = (File) files.get(i);
            zipFile(file, outputStream);
        }
    }

    /**
     * 根据输入的文件与输出流对文件进行打包
     *
     */
    public static void zipFile(File inputFile, ZipOutputStream ouputStream) {
        try {
            if (inputFile.exists()) {
                if (inputFile.isFile()) {
                    FileInputStream IN = new FileInputStream(inputFile);
                    BufferedInputStream bins = new BufferedInputStream(IN, 512);
                    ZipEntry entry = new ZipEntry(inputFile.getName());
                    ouputStream.putNextEntry(entry);
                    // 向压缩文件中输出数据
                    int nNumber;
                    byte[] buffer = new byte[512];
                    while ((nNumber = bins.read(buffer)) != -1) {
                        ouputStream.write(buffer, 0, nNumber);
                    }
                    // 关闭创建的流对象
                    bins.close();
                    IN.close();
                } else {
                    try {
                        File[] files = inputFile.listFiles();
                        for (int i = 0; i < files.length; i++) {
                            zipFile(files[i], ouputStream);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static HttpServletResponse downloadZip(File file, HttpServletResponse response) {
        if (file.exists() == false) {
            System.out.println("待压缩的文件目录：" + file + "不存在.");
        } else {
            try {
                // 以流的形式下载文件。
                InputStream fis = new BufferedInputStream(new FileInputStream(file.getPath()));
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);
                fis.close();
                // 清空response
                response.reset();

                OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
                response.setContentType("application/octet-stream");

                // 如果输出的是中文名的文件，在此处就要用URLEncoder.encode方法进行处理
                response.setHeader("Content-Disposition",
                        "attachment;filename=" + new String(file.getName().getBytes("GB2312"), "ISO8859-1"));
                toClient.write(buffer);
                toClient.flush();
                toClient.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    File f = new File(file.getPath());
                    f.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }
    //实现Spring Boot 的文件下载功能，映射网址为/download
    @RequestMapping("/download")
    public String downloadFile(HttpServletRequest request, HttpServletResponse response, String name) throws UnsupportedEncodingException {
        // 获取指定目录下的第一个文件
        File scFileDir = new File("H:/id/student/src/main/resources/FILE/预习/"+name);
        /*File TrxFiles[] = scFileDir.listFiles();*/
        // System.out.println("要下载的文件"+TrxFiles[0]);
        String fileName = scFileDir.getName(); //下载的文件名
        // 如果文件名不为空，则进行下载

        // 配置文件下载
        response.setHeader("content-type", "application/octet-stream");
        response.setContentType("application/octet-stream");
        // 下载文件能正常显示中文
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));

        // 实现文件下载
        byte[] buffer = new byte[1024];
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try {
            fis = new FileInputStream(scFileDir);
            bis = new BufferedInputStream(fis);
            OutputStream os = response.getOutputStream();
            int i = bis.read(buffer);
            while (i != -1) {
                os.write(buffer, 0, i);
                i = bis.read(buffer);
            }
            System.out.println("Download the song successfully!");
        }
        catch (Exception e) {
            System.out.println("Download the song failed!");
        }
        finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
    /**
     * 上传作业
     * @param file
     * @param stuId
     * @param id
     * @param type
     * @return
     */
    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file, String stuId, String id, String type) {
        String fileName = file.getOriginalFilename();
        String filePath = "H:/git/student/src/main/resources/FILE/预习/";
        File dest = new File(filePath + fileName);
        //添加交作业的学生信息
        workdirLHService.insertWork(stuId,fileName);
        //查询该文件的上一级
        //添加交作业的类型
        workdirLHService.insertWorkdir(fileName,type,id);


        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/workdirLH/selAll?type="+type+"&parentid="+id;
    }
}
