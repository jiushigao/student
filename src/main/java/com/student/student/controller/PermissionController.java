package com.student.student.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.TypeReference;
import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Permission;
import com.student.student.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * 角色管理-Dora
 */
@Controller
@RequestMapping("/permission")
public class PermissionController {
    @Autowired
    private PermissionService permissionService; //权限

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/list")
    public String list(Permission permission, Model model){
        //默认查询一级菜单
        if(null == permission || permission.getLevel() == null){
            permission = new Permission();
            permission.setLevel( 1 );
        }
        List<Permission> list = new ArrayList<>();
        try {
            list = permissionService.findList(permission);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        model.addAttribute("list",list);
        model.addAttribute("permission",permission);
        return "permission/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/permission.json")
    @ResponseBody
    public PageInfo listPageJson(Permission permission, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<Permission> pageInfo = new PageInfo<>(  );
        try {
            pageInfo = permissionService.findListPage(permission, pageNum, pageSize);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  pageInfo;
    }

    /**
     * 进入一级菜单的修改页面
     */
    @RequestMapping("/toLevel1Update")
    public String toLevel1Update(Integer id,Model model){
        if (id != null){
            Permission permission = new Permission(id);
            Map<String, Object> rstMap = new HashMap<>();
            try {
                rstMap = permissionService.toLevel1Update(permission);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            model.addAttribute("nextPermission", rstMap.get( "nextLevel" ));
            model.addAttribute("permission", rstMap.get( "pEntity" ));
        }
        return "permission/editLevel1";
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delById")
    @ResponseBody
    public Integer delById(Integer id){
        int rst = -1;
        try {
            rst = permissionService.delById(id);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids){
        int rst = -1;
        try {
            List<String> idList = Arrays.asList(  ids.split( "," ) );
            rst = permissionService.delByIds(idList);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }

    /**
     * 根据Id修改一级菜单信息
     * @param permission 一级菜单
     * @param list  对应的二级菜单
     * @param delLevel2Ids 被删除的二级菜单id
     * @return
     */
    @RequestMapping("/updateLevel1")
    @ResponseBody
    public Integer updateLevel1(String permission,String list, String delLevel2Ids){
        int rst = -1;
        try {
            rst = permissionService.updateLevel1(permission, list, delLevel2Ids);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }


    /**
     * 新增
     */
    @RequestMapping("/add")
    @ResponseBody
    public Integer add(String permission,String list ){
        int rst = -1;
        try {
            rst = permissionService.insert(permission, list);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }

    /**
     * 判断名字是否唯一
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public Integer findByName(String name, Integer id, Integer level, Integer parentId)  {
        int rst = -1;
        try {
            Permission permission = new Permission(id,name,level,parentId);
            rst = permissionService.findByName(permission);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }
}
