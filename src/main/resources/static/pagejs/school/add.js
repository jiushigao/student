/*
* @Author: zikai
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;

    //判断操作的结果
    if(null != rst && undefined != rst && rst != '' && rst.length > 0){
        if(rst == -3){
            layer.msg('请填写完整的信息！', {time: 1000});
        } else if(rst == -2){
            layer.msg('该校区已经存在，请重新输入！', {time: 1000});
        }else if(rst <= 0) {
            layer.msg('操作异常，请联系管理员！', {time: 1000});
        }
    }
    //验证名字是否已经存在
    $("[name=name]").blur(function(){
        var name = $(this).val();
        if(name == null || name == undefined || name.length <= 0){
            return;
        }
        var id = $("[name=id]").val() ;
        id = id == null || id == undefined ? -1 :id;
        $.post("/school/checkName","name=" + name + "&id="+id,function (data) {
            if (data > 0) {
                layer.msg('该校区名称已存在！', {time: 1000}, function(){
                    $("[name=name]").val($("[name=hiddenSchoolName]").val());
                });
            }
        },"text")
    }) ;
    //表单的提交，非空验证
    $("form").submit(function () {
        var name = $("[name=name]").val();
        if(name == null || name == undefined || name == '' || name.trim().length <= 0){
            layer.msg("校区名称不能为空！", {time: 1000});
            return false;
        }
    });
})