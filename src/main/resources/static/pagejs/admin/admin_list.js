/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;

    //判断操作的结果
    if(null != rst && undefined != rst && rst == -1){
        layer.msg('操作异常，请联系管理员！');
    }
    //全选
    form.on('checkbox(allChoose)', function(data){
        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
        child.each(function(index, item){
            item.checked = data.elem.checked;
        });
        form.render('checkbox');
    });

    //分页
    laypage({
        cont: 'page',
        pages: pages, //总页数
        groups: 5, //连续显示分页数
        limit: limit,  //每页显示条数
        jump: function (obj, first) {
            if(!first){
                var name = $("[name=name]").val();
                var phone = $("[name=phone]").val();
                $.post("/admin/admin.json", "pageNum="+obj.curr+"&pageSize="+obj.limit+"&name="+name+"&phone="+phone,function(data){
                    $(".news_content").html('');
                    var info = '';
                    $(data.list).each(function(){
                        info += '<tr>';
                        info += '<td><input name="checked" lay-skin="primary" lay-filter="choose" type="checkbox"><div class="layui-unselect layui-form-checkbox"><span>勾选</span><i class="layui-icon"></i></div></td>';
                        info += '<td>' + this.name + '</td>';
                        info += '<td>' + this.phone + '</td>';
                        info += '<td> <a class="layui-btn layui-btn-mini news_edit" href="/admin/toEdit?id="'+this.id+'><i class="iconfont icon-edit"></i> 编辑</a>';
                        info += '<a class="layui-btn layui-btn-danger layui-btn-mini news_del splitJS" data-id="1" href="/admin/del?id="'+this.id+' value="' + this.id + '"><i class="layui-icon"></i> 删除</a>';
                        info += '</td></tr>';
                    });
                    $(".news_content").html(info);
                    $('.news_list thead input[type="checkbox"]').prop("checked",false);
                    form.render();
                },"json");
            }
        }
    });

    //批量删除
    $(".batchDel").click(function(){
        var $checkbox = $('.news_list tbody input[type="checkbox"][name="checked"]');
        var $checked = $('.news_list tbody input[type="checkbox"][name="checked"]:checked');
        if($checkbox.is(":checked")){
            layer.confirm('确定删除选中的信息？',{icon:3, title:'提示信息'},function(index){
                var ids = "";
                for(var j = 0; j < $checked.length; j++){
                    var id = $checked.eq(j).parents("tr").find(".news_del").attr("value");
                    ids += id + ",";
                }
                ids = ids.substring(0, ids.length - 1);
                $.post("/admin/delByIds", "ids="+ ids, function (data) {
                    if(data > 0){
                        layer.msg("删除成功");
                        location.href= "/admin/list";
                    }else{
                        layer.msg("删除失败，请联系管理员");
                    }
                }, "text");
            })
        }else{
            layer.msg("请选择需要删除的文章");
        }
    })
})