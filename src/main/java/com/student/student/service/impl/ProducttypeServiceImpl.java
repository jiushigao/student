package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.ProducttypeMapper;
import com.student.student.pojo.Committe;
import com.student.student.pojo.Producttype;
import com.student.student.service.ProducttypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 产品实现类
 */
@Service
public class ProducttypeServiceImpl implements ProducttypeService {
    @Autowired
    private ProducttypeMapper producttypeMapper;

    @Override
    public PageInfo<Producttype> findListPage(Producttype producttype, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Producttype> list = producttypeMapper.selectList(producttype);
        PageInfo<Producttype> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public List<Producttype> findList(Producttype producttype) {
        return producttypeMapper.selectList(producttype);
    }
    @Override
    public int deleteProducttypeById(String id) {
        return producttypeMapper.deleteProducttypeById(id);
    }

    @Override
    public int deleteProducttypeByIds(List<String> ids) {
        return producttypeMapper.deleteProducttypeByIds(ids);
    }

    @Override
    public Producttype findOne(Producttype producttype) {
        return producttypeMapper.selectOne(producttype);
    }

    @Override
    public int updateProducttype(Producttype regulations) {
        return producttypeMapper.updateProducttype(regulations);
    }

    @Override
    public int insertProducttype(Producttype regulations) {
        return producttypeMapper.insertProducttype(regulations);
    }

    @Override
    public int countByname(Producttype producttype) {
        return producttypeMapper.countByname(producttype);
    }
}
