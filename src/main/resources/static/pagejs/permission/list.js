/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element',  'layer', 'table'],function(){
    var form = layui.form(),
        table = layui.table,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;

    $("body").on("click",".news_del",function(){  //删除
        var id = $(this).attr("value");
        layer.confirm('确定删除此信息？',{icon:3, title:'提示信息'},function(index){
            $.post("/permission/delById", "id="+ id, function (data) {
                if(data > 0){
                    layer.msg('删除成功！', {time: 1000}, function(){
                        location.href= "/permission/list";
                    });
                }else{
                    layer.msg("删除失败，请联系管理员", {time: 1000}, function(){
                        location.href= "/permission/list";
                    });
                }
            }, "text");
        });
    })
})