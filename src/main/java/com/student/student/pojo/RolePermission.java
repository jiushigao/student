package com.student.student.pojo;

import java.util.List;

/**
 * 角色所对应权限
 * 作者：Dora
 * 日期：2019-06-08 09:37
 */
public class RolePermission {
    /**角色id*/
    private Integer roleId;
    /**权限id*/
    private Integer permissionId;

    public RolePermission() {
    }
    public RolePermission(Integer roleId, Integer permissionId) {
        this.roleId = roleId;
        this.permissionId = permissionId;
    }

    /**角色id*/
    public Integer getRoleId() {
        return roleId;
    }
    /**角色id*/
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
    /**权限id*/
    public Integer getPermissionId() {
        return permissionId;
    }
    /**权限id*/
    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }
}
