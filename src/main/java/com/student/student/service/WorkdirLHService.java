package com.student.student.service;

import com.student.student.pojo.Workdir;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WorkdirLHService {
    /**
     * 查询全部作业
     * @return
     */
    List<Workdir> findWorkdirAll(Workdir workdir);

    /**
     * 查询父级id
     * @param id
     * @return
     */
    Workdir findWorkdirById(Integer id);

    /**
     * 添加学员信息
     * @param stuId
     * @param filename
     */
    void insertWork(@Param("stuId") String stuId, @Param("filename") String filename);

    /**
     * 添加作业
     * @param name
     * @param type
     * @param parentid
     */
    void insertWorkdir(@Param("name") String name, @Param("type") String type, @Param("parentid") String parentid);

}
