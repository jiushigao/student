package com.student.student.mapper;

import com.student.student.pojo.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 教质管理
 */
public interface TeacherMapper {

    /**
     * 查询所有,有登录的手机号码，有角色名称
     * @param teacher
     * @return
     */
    List<Teacher> selectListExp(Teacher teacher);

    /**
     * 查询所有,只有teacher表的基本信息
     * @param teacher
     * @return
     */
    List<Teacher> selectList(Teacher teacher);

    /**
     * 根据Id查询教职信息,有登录的手机号码，有角色名称
     * @param teacher
     * @return
     */
    Teacher selectOneExp(Teacher teacher);

    /**
     * 新增
     * @param teacher
     * @return
     */
    int insert(Teacher teacher);

    /**
     * 根据id修改教职信息
     * @param teacher
     * @return
     */
    int update(Teacher teacher);

    /**
     * 根据Id删除教职信息
     * @param loginId
     * @return
     */
    int delById(@Param("loginId") Integer loginId);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(@Param("ids")List<String> ids);
}
