package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @BelongsProject: student
 * @BelongsPackage: com.student.student.service
 * @Author: Joe
 * @CreateTime: 2019-05-22 15:39
 * @Description: ${Description}
 */
public interface TeacherService {


    /**
     * 多条件查询兼并查询所有,不分页
     * @param teacher
     * @return
     */
    List<Teacher> findList(Teacher teacher);

    /**
     * 多条件查询兼并查询所有,分页
     * @param teacher
     * @return
     */
    PageInfo<Teacher> findListPage(Teacher teacher, Integer pageNum, Integer pageSize);

    /**
     * 根据Id查询教职信息
     * @param teacher
     * @return
     */
    Teacher  findOne(Teacher teacher);

    /**
     * 根据Id查询教职信息
     * @param teacher
     * @return
     */
    int add(Teacher teacher);

    /**
     * 根据id修改教职信息
     * @param teacher
     * @return
     */
    int update(Teacher teacher);

    /**
     * 根据Id删除教职信息
     * @param loginId
     * @return
     */
    int delById(Integer loginId);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(List<String> ids);
}
