package com.student.student.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.PermissionMapper;
import com.student.student.pojo.Permission;
import com.student.student.service.PermissionService;
import com.student.student.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 作者：Dora
 * 日期：2019-06-08 13:03
 */
@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private PermissionMapper permissionMapper; //权限

    @Override
    public List<Permission> findList(Permission permission) {
        return permissionMapper.selectList( permission);
    }

    @Override
    public PageInfo<Permission> findListPage(Permission permission, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Permission> list = permissionMapper.selectList(permission);
        PageInfo<Permission> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public Permission findOne(Permission permission) {
        return permissionMapper.selectOne( permission );
    }

    @Override
    public Map<String, Object> toLevel1Update(Permission permission) {
        Map<String, Object> rstMap = new HashMap<>();
        //得到下一级菜单
        Permission pLevelNext = new Permission();
        pLevelNext.setParentId( permission.getId() );
        List<Permission> list = permissionMapper.selectList(pLevelNext);
        rstMap.put( "nextLevel", list );

        Permission pEntity = permissionMapper.selectOne( permission );
        rstMap.put( "pEntity", pEntity );
        return rstMap;
    }

    @Override
    public int insert(String perStr,String perLevel2list) {

        //新增
        Permission permission = JSON.parseObject(perStr, new TypeReference<Permission>() {});
        if(null == permission || null == permission.getName() || permission.getName().trim().length() <= 0
                || null == permission.getSort() || permission.getSort() <= 0){
            return -5;
        }
        permission.setLevel( 1 );
        permission.setParentId( -1 );
        int rst = permissionMapper.findByName( permission );
        if(rst > 0){
            return -6;
        }
        rst = permissionMapper.insert( permission );
        if(rst < 0){
            return rst;
        }
        List<Permission> addList = new ArrayList<>();
        Permission pLevel2 = null;
        JSONArray perLevel2 = JSON.parseArray(perLevel2list);
        for (Object obj : perLevel2) {
            JSONObject jobj = (JSONObject) obj;
            pLevel2 = new Permission(jobj.getString( "name" ),jobj.getString( "path" ),
                    permission.getId(), jobj.getInteger( "sort" ));
            if(null == pLevel2 || null == pLevel2.getName() || pLevel2.getName().trim().length() <= 0
                    || null == pLevel2.getSort() || pLevel2.getSort() <= 0
                    || null == pLevel2.getPath() || pLevel2.getPath().trim().length() <= 0){
                return -5;
            }
            pLevel2.setLevel(2);
            addList.add( pLevel2 );
        }
        if(addList.size() > 0){
            rst = permissionMapper.inserts( addList );
            if(rst <= 0){
                return rst;
            }
        }

        return rst;
    }


    /**
     *
     * @param perStr
     * @param perLevel2list
     * @param delLevel2Ids 被删除的二级菜单id的集合
     * @return -5表示数据为null,-6表示一级菜单的名字已经存在
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateLevel1(String perStr,String perLevel2list, String delLevel2Ids) {
        //封装需要添加的数据
        Permission permission = JSON.parseObject(perStr, new TypeReference<Permission>() {});
        if(null == permission || null == permission.getName() || permission.getName().trim().length() <= 0
                || null == permission.getSort() || permission.getSort() <= 0){
            return -5;
        }
        int rst = permissionMapper.findByName( permission );
        if(rst > 0){
            return -6;
        }
        List<Permission> addList = new ArrayList<>();
        List<Permission> updateList = new ArrayList<>();
        Permission pLevel2 = null;
        JSONArray perLevel2 = JSON.parseArray(perLevel2list);
        for (Object obj : perLevel2) {
            JSONObject jobj = (JSONObject) obj;
            pLevel2 = new Permission(jobj.getString( "name" ),jobj.getString( "path" ),
                    permission.getId(), jobj.getInteger( "sort" ));
            if(null == pLevel2 || null == pLevel2.getName() || pLevel2.getName().trim().length() <= 0
                    || null == pLevel2.getSort() || pLevel2.getSort() <= 0
                    || null == pLevel2.getPath() || pLevel2.getPath().trim().length() <= 0){
                return -5;
            }
            if(jobj.getInteger( "status" ) == 1){ //表示新增
                pLevel2.setLevel(2);
                addList.add( pLevel2 );
            }else{ //表示修改
                pLevel2.setId( jobj.getInteger( "id" ) );
                updateList.add(pLevel2 );
            }
        }

        //更新第一级
        rst = rst = permissionMapper.update( permission );
        if(rst <= 0){
            return rst;
        }
        //删除
        if(delLevel2Ids != null && delLevel2Ids.trim().length() > 0){
            List<String> ids = Arrays.asList( delLevel2Ids.substring( 0, delLevel2Ids.length() -1 ).split( "," ) );
            rst = permissionMapper.delByIds( ids );
            if(rst <= 0){
                return rst;
            }
        }
        //对于新增二级权限目录的，进行insert
        if(addList.size() > 0){
            rst = permissionMapper.inserts( addList );
            if(rst <= 0){
                return rst;
            }
        }
        //对于已经存在的二级权限目录进行update
        if(updateList.size() > 0){
            rst = permissionMapper.updates( updateList );
        }
        return rst;
    }

    @Override
    public int delById(Integer id) {
        //先判断是否是一级目录
        Permission p = new Permission( id );
        p.setParentId( -1 );
        int rst = permissionMapper.findByName( p );
        if(rst <= 0){
            return -5;
        }
        //再删除对应的子级目录
        rst = permissionMapper.delByParentId(id);
        if(rst <= 0){
            return rst;
        }
        //再删除本身
        return permissionMapper.delById( id );
    }

    @Override
    public int delByIds(List<String> ids) {
        return permissionMapper.delByIds( ids );
    }

    @Override
    public int findByName( Permission permission) {
        return permissionMapper.findByName( permission );
    }
}
