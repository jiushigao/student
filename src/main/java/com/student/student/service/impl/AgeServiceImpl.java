package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.AgeMapper;
import com.student.student.pojo.Admin;
import com.student.student.pojo.Age;
import com.student.student.service.AgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgeServiceImpl implements AgeService {
    @Autowired
    private AgeMapper ageMapper;

    @Override
    public  PageInfo<Age> findListPage(Age age, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Age> list = ageMapper.selectAll(age);
        PageInfo<Age> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public  List<Age> fingList(Age age) {
        return ageMapper.selectAll(age);
    }
    @Override
    public int delAge(Integer id)  {
        return  ageMapper.delAge(id);
    }


    @Override
    public Age selectById(Integer id) {
        return ageMapper.selectById(id);
    }

    @Override
    public int updateAge(Age age) {
        return ageMapper.updateAge(age);
    }

    @Override
    public int insertAge(Age age) {
        return ageMapper.insertAge(age);
    }

    @Override
    public int delByIds(List<String> ids) {
        return ageMapper.delByIds(ids);
    }

    @Override
    public int selectByName(Age age) {
        return ageMapper.selectByName(age);
    }
}
