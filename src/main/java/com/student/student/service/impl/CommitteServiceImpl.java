package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.CommitteMapper;
import com.student.student.mapper.StudemtMapper;
import com.student.student.pojo.Classes;
import com.student.student.pojo.Committe;
import com.student.student.pojo.Student;
import com.student.student.service.CommitteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 班委管理
 * @author 资凯
 */
@Service
public class CommitteServiceImpl implements CommitteService {

    @Autowired
    private CommitteMapper committeMapper;  //班委管理
    @Autowired
    private StudemtMapper studemtMapper;  //学员管理

    /**
     * 根据条件查询班级职位,返回list集合
     */
    @Override
    public PageInfo<Committe> findList(Committe committe, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Committe> list = committeMapper.selectList(committe);
        PageInfo<Committe> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public int updateCommitteById(Committe committe) {
        return committeMapper.updateCommitteById(committe);
    }


    /**
     * 根据学生id修改班级职位
     * @param committe
     * @return
     */
    @Override
    public int updateCommitteByStu(Integer committeId, Integer studentId) {
        Student stu = new Student();
        stu.setStuLoginId( studentId );
       // stu.set( studentId );
        return 0; //studemtMapper.update(committe);
    }

    @Override
   public Committe selectOne(Committe committe) {
       return committeMapper.selectOne(committe);
   }

    @Override
    public int countByName(Committe committe) {
        return committeMapper.countByName(committe);
    }

    @Override
    public List<Student> findAllByClassesId(Map<String, Object> condition) {
        return studemtMapper.findStuPosByClasses( condition );
    }

    /**
     * 查询全部，不分页
     * @param committe
     * @return
     */
    @Override
    public List<Committe> findList(Committe committe) {
        return committeMapper.selectList(committe);
    }
/*

    @Override
    public List<Student> findStuByClassesId(Integer ClassesId) {
        return committeMapper.findStuByClassesId(ClassesId);
    }



    @Override
    public Student findCommitteByStuId(Integer stuId) {
        return committeMapper.findCommitteByStuId(stuId);
    }
*/

    @Override
    public int insertCommitte(Committe committe) {
        return committeMapper.insertCommitte(committe);
    }

    @Override
    public int deleteCommitteById(Integer id) {
        return committeMapper.deleteCommitteById(id);
    }

    @Override
    public int delByIds(List<String> ids) {
        return committeMapper.delByIds(ids);
    }


}
