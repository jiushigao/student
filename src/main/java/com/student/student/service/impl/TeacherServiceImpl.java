package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.LoginMapper;
import com.student.student.mapper.TeacherMapper;
import com.student.student.pojo.Login;
import com.student.student.pojo.School;
import com.student.student.pojo.Teacher;
import com.student.student.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.rmi.runtime.Log;

import java.util.List;

/**
 * 教质管理
 * @Author: Joe
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherMapper teacherMapper;  //老师
    @Autowired
    private LoginMapper loginMapper;     //登录表信息

    @Override
    public List<Teacher> findList(Teacher teacher) {
        return teacherMapper.selectListExp(teacher);
    }

    @Override
    public PageInfo<Teacher> findListPage(Teacher teacher, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Teacher> list = teacherMapper.selectListExp(teacher);
        PageInfo<Teacher> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public Teacher findOne(Teacher teacher) {
        return teacherMapper.selectOneExp(teacher);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int add(Teacher teacher) {
        Login login = new Login();
        login.setName( teacher.getName() );
        login.setPhone( teacher.getPhone() );
        login.setRoleId( teacher.getRoleId() );
        int rst = loginMapper.insert( login );
        if(rst <= 0){
            return rst;
        }
        teacher.setLoginId( login.getId() );
        return teacherMapper.insert(teacher);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(Teacher teacher) {
        Login login = new Login();
        login.setName( teacher.getName() );
        login.setPhone( teacher.getPhone() );
        login.setId( teacher.getLoginId() );
        int rst = loginMapper.updateById( login );
        if(rst <= 0){
            return rst;
        }
        return teacherMapper.update(teacher);
    }

    /**
     * 删除职工信息
     * @param loginId
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int delById(Integer loginId) {
        int rst = teacherMapper.delById(loginId);  //删除员工基本信息
        if(rst <= 0){
            return rst;
        }
        return loginMapper.delById( loginId );//删除登录信息
    }

    /**
     * 批量删除职工信息
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int delByIds(List<String> ids) {
        int rst = teacherMapper.delByIds(ids);  //删除员工基本信息
        if(rst <= 0){
            return rst;
        }
        return loginMapper.delByIds( ids );//删除登录信息
    }
}
