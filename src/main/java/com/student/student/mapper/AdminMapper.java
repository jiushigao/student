package com.student.student.mapper;

import com.student.student.pojo.Admin;
import com.student.student.pojo.Affiche;
import com.student.student.pojo.Login;
import com.student.student.pojo.School;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 管理员
 */
public interface AdminMapper {

    /**
     * 查询List
     * @param admin
     * @return
     */
    List<Admin> selectList(Admin admin);

    /**
     * 查询Entity
     */
    Admin selectOne(Admin admin);

    /**
     * 根据主键删除数据
     */
    int delById(@Param("id")Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(@Param("ids")List<String> ids);

    /**
     * 插入数据
     */
    int insert(Admin admin);

    /**
     * 修改数据
     */
    int update(Admin admin);
}
