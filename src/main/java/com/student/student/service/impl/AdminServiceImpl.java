package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.AdminMapper;
import com.student.student.mapper.AgeMapper;
import com.student.student.mapper.LoginMapper;
import com.student.student.pojo.Admin;
import com.student.student.pojo.Login;
import com.student.student.service.AdminService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;  //管理员信息
    @Autowired
    private LoginMapper loginMapper;  //登录

    /**
     * 分页查询，List
     * @param admin
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageInfo<Admin> findListPage(Admin admin, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Admin> list = adminMapper.selectList(admin);
        PageInfo<Admin> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * 查询List，不分页
     * @param admin
     * @return
     */
    @Override
    public List<Admin> findList(Admin admin) {
        return adminMapper.selectList(admin);
    }

    /**
     * 删除管理员
     * @param id
     * @return
     */
    @Override
    public int delById(Integer id) {
        return adminMapper.delById(id); //删除管理基本信息
    }

    /**
     * 批量删除管理员
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int delByIds(List<String> ids) {
        return adminMapper.delByIds(ids); //删除管理基本信息
    }

    /**
     * 更新，根据id更新昵称
     * @param admin
     * @return
     */
    @Override
    public int update(Admin admin) {
        return adminMapper.update(admin);
    }

    /**
     * 根据id查询管理的基本信息和登录信息
     * @param id
     * @return
     */
    @Override
    public Admin findById(Integer id) {
        return adminMapper.selectOne(new Admin(id));
    }

    /**
     * 新增管理员信息
     * @param admin
     * @return
     */
    public int add(Admin admin){
        //判断手机号码是否已经存在
        Integer id = null;
        int rst = loginMapper.countByPhone( new Login(-1, admin.getPhone(), 1 ) );
        if(rst > 0){
            return  -2;
        }
        return  adminMapper.insert( admin );
    }
}
