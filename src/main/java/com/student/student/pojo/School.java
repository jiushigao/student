/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * 校区(school)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
/*@AllArgsConstructor
@NoArgsConstructor*/
public class School implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 7261468245591851531L;
    /** 校区编号 */
    private Integer id;
    /** 校区名字 */
    private String name;
    public School(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    public School(Integer id) {
        this.id = id;
    }
    public School() {
    }
    /**
     * 获取校区编号
     * 
     * @return 校区编号
     */
    public Integer getId() {
        return this.id;
    }
    /**
     * 设置校区编号
     * 
     * @param id
     *          校区编号
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取校区名字
     * 
     * @return 校区名字
     */
    public String getName() {
        return this.name;
    }
    /**
     * 设置校区名字
     * 
     * @param name
     *          校区名字
     */
    public void setName(String name) {
        this.name = name;
    }
}