package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Admin;
import com.student.student.pojo.Age;
import com.student.student.service.AgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/age")
public class AgeController {
    @Autowired
    private AgeService ageService;
    /**
     * 查询所有信息
     * @param model
     * @return
     */
    @GetMapping("/list")
    public  String list(Model model, Age age, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Age> pageInfo = ageService.findListPage(age, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("agename",age.getName());
        return "age/age_list";
    }

    /**
     * 分页
     */
    @RequestMapping("/age.json")
    @ResponseBody
    public PageInfo listPageJson(Age age,  Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo pageInfo = ageService.findListPage(age, pageNum, pageSize);
        return  pageInfo;
    }

    /**
     * 根据id删除信息
     * @param id
     * @return
     */
    @RequestMapping("/delAge")
    @ResponseBody
    public  int Affdelid(Integer id,Model model){
        int rst = 0;
        try{
            rst = ageService.delAge(id);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //删除失败
        }
        return rst;
    }

    /**
     * 批量删除
     * @param model
     * @return
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public  Integer delByIds(Model model,String ids){
        int rst = 0;
        try{
            List<String> idsList  = Arrays.asList(ids.split( "," ));
            rst = ageService.delByIds(idsList);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //删除失败
        }
        return rst;
    }

    /**
     * 修改
     * @param age
     * @param model
     * @return
     */
    @RequestMapping("/updateAge")
    public  String updateAge(Age age, Model model){
        int rst = 0;
        try{
            rst = ageService.updateAge(age);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //删除失败
        }
        return "redirect:/age/list";
    }

    /**
     * 根据id查询信息
     * @param id
     * @return
     */
    @RequestMapping("/selectAgeById")
    public  String selectById(Integer id,Model model){
        if(id!=null){
            try{
                Age ageById = ageService.selectById(id);
                model.addAttribute("ageById",ageById);
            }catch (Exception ex){
                ex.printStackTrace();
                model.addAttribute( "rst", "-1" );
                return "redirect:/admin/list";
            }

        }
        return  "../templates/age/age_edit";
    }

    /**
     * 新增
     * @param age
     * @param model
     * @return
     */
    @RequestMapping("/insertAge")
    public  String insertAge(Age age,Model model){
        int rst = 0;
        try{
            rst =  ageService.insertAge(age);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //新增失败
        }
        return  "redirect:/age/list";
    }

    /**
     * 判断名字是否存在
     * @param age
     * @param response
     */
    @RequestMapping("/selByName")
    public void selName(Age age, HttpServletResponse response){
        try {
            PrintWriter out=response.getWriter();
            int i=ageService.selectByName(age);
            if(i>0){
                out.print("no");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
