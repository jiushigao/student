package com.student.student.mapper;

import com.student.student.pojo.Classes;
import com.student.student.pojo.Interview;
import com.student.student.pojo.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学员访谈
 * @author 资凯
 */
public interface InterviewMapper {

    /**
     * 根据条件查询，返回List
     */
    List<Interview> selectListAStuName(Interview interview);

    /**
     * 新增访谈记录
     */
    int insert(Interview interview);


    /**
     * 查询单个
     */
    Interview selectOne(Interview interview);

    /**
     * 根据访谈id删除访谈记录
     */
    int delById(Integer id);


    /**
     * 批量删除
     */
    int delByIds(@Param("ids") List<String> ids);

    /**
     * 根据访谈id修改访谈记录
     */
    int update(Interview interview);
}
