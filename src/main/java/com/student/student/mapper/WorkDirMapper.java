package com.student.student.mapper;

import com.student.student.pojo.Workdir;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 作业目录管理
 * 作者：Dora
 * 日期：2019-05-23 14:44
 */
@Mapper
public interface WorkDirMapper {
    /**
     * 查询所有目录信息（可多条件）
     * @return
     */
    List<Workdir> selectByAll(@Param("parentid")String parentid,@Param("id")String id);

    /**
     * 添加目录信息
     * @return
     */
    int addWorkDir(@Param("name")String name,@Param("type")String type,@Param("parentid")String parentid);

    /**
     * 修改目录信息
     * @return
     */
    int updateById(@Param("id")String id,@Param("name")String name);

    /**
     * 删除目录
     * @param id
     * @return
     */
    int delById(@Param("id")String id);
}
