package com.student.student.mapper;

import com.student.student.pojo.Status;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface StatusMapper {
    /**
     * 根据条件查询所有状态信息
     */
    List<Status> selectList(Status status);

    /**
     * 根据主键查询数据
     */
    Status selectOne(Status status);

    /**
     * 插入数据
     */
    int insertStatus(Status status);

    /**
     * 修改数据
     */
    int updateStatus(Status status);

    /**
     * 根据主键删除数据
     */
    int deleteStatusById(Integer id);

    /**
     * 判断校区名字是否唯一
     */
    int findByStatusName(String name);
}
