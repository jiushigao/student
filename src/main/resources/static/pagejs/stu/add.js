/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;
    //验证身份证是否已经存在
    $("#idCard").blur(function(){
        var idCard = $("#idCard").val();
        if(idCard == null || idCard == undefined || idCard.length <= 0){
            return;
        }
        var loginId = $("[name=stuLoginId]").val() ;
        $.post("/stus/checkidCard","idCard=" + idCard + "&loginId="+loginId,function (data) {
            if (data > 0) {
                layer.msg('该身份证已存在！', function(){
                    $("#idCard").val($("#hiddenIdCard").val());
                });
            }
        },"text")
    }) ;
    //验证电话号码是否已经存在
    $("#phone").blur(function(){
        var phone = $("#phone").val();
        if(phone == null || phone == undefined || phone.length <= 0){
            return;
        }
        var loginId = $("[name=stuLoginId]").val() ;
        $.post("/admin/checkPhone","phone=" + phone + "&loginId="+loginId,function (data) {
            if (data > 0) {
                layer.msg('该电话已存在！', function(){
                    $("#phone").val($("#hiddenPhone").val());
                });
            }
        },"text")
    }) ;
    //产品的选择事件，得到对应产品下的学期
    form.on('select(producttypesId)', function(data){
        var ptId = data.elem.value;;
        $.post("/term/listByPtId", "ptId="+ptId, function (rstData) {
            $("#termId").empty();
            var html = "<option value='0'>请选择学期</option>";
            $(rstData).each(function () {
                html += "<option value='" + this.id + "'>" + this.name + "</option>";
            });
            //把遍历的数据放到select表里面
            $("#termId").append(html);
            //从新刷新了一下下拉框
            form.render('select');      //重新渲染
        }, "json");
    });
    //学期选择事件，得到当前学期下的班级
    form.on('select(termId)', function(data){
        var termId = data.elem.value;;
        $.post("/classes/findClassesByTerm", "termId="+termId, function (rstData) {
            $("#classesId").empty();
            var html = "<option value='0'>请选择班级</option>";
            $(rstData).each(function () {
                html += "<option value='" + this.id + "'>" + this.name + "</option>";
            });
            //把遍历的数据放到select表里面
            $("#classesId").append(html);
            //从新刷新了一下下拉框
            form.render('select');      //重新渲染
        }, "json");
    });
    //表单的提交，非空验证
    $("form").submit(function () {
        var stuName = $("[name=stuName]").val();
        if(stuName == null || stuName == undefined || stuName == '' || stuName.trim().length <= 0){
            layer.msg("姓名不能为空！", {time: 700}, function () {
                $("[name=stuName]").focus();
            });
            return false;
        }
        var phone = $("[name=phone]").val();
        if(phone == null || phone == undefined || phone == '' || phone.trim().length <= 0){
            layer.msg("电话不能为空！", {time: 700}, function () {
                $("[name=phone]").focus();
            });
            return false;
        }
        var classesId = $("[name=classesId]").val();
        if(classesId == null || classesId == undefined || classesId == '' || classesId == '-1'){
            layer.msg("请选择班级！", {time: 700}, function () {
                $("[name=classesId]").focus();
            });
            return false;
        }
    });
})