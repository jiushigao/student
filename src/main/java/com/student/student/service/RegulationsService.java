package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Regulations;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 校规Service
 */
public interface RegulationsService {
    /**
     * 根据条件查询，分页显示
     * @param regulations
     * @return
     */
    PageInfo<Regulations> findListPage(Regulations regulations, Integer pageNum, Integer pageSize);
    /**
     * 根据条件查询
     * @param regulations
     * @return
     */
    List<Regulations> findList(Regulations regulations);

    /**
     * 根据id删除校规
     * @param id
     */
    int deleteRegulationsById(String id);
    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deleteRegulationsByIds(List<String> ids);

    /**
     * 返回单个实体
     * @param regulations
     * @return
     */
    Regulations findRegulationsByIds(Regulations regulations);

    /**
     * 获取实体类对象进行修改
     * @param regulations
     */
    int updateRegulations(Regulations regulations);

    /**
     * 获取实体类对象进行新增
     * @param regulations
     */
    int insertRegulations(Regulations regulations);
    /**
     * 根据条件统计
     * @param regulations
     * @return
     */
    int countByname(Regulations regulations);
}
