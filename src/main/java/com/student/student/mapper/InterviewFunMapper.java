package com.student.student.mapper;

import com.student.student.pojo.InterviewFun;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InterviewFunMapper {

    /**
     *根据条件查询所有
     */
    List<InterviewFun> selectList(InterviewFun InterviewFun);

    /**
     * 查询单个
     */
    InterviewFun selectOne(InterviewFun InterviewFun);

    /**
     * 新增
     */
    int insert(InterviewFun InterviewFun);

    /**
     * 删除
     */
    int delById(Integer id);

    /**
     * 批量删除
     */
    int delByIds(@Param("ids") List<String> ids);

    /**
     * 修改
     */
    int update(InterviewFun InterviewFun);

    /**
     * 统计类型名是否已经存在
     * @param InterviewFun
     * @return
     */
    int countByName(InterviewFun InterviewFun);
}
