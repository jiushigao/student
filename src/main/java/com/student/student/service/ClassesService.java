package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Classes;

import java.util.List;
import java.util.Map;

public interface ClassesService {
    /**
     * 查询多个
     * @param classes
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<Classes> findList(Classes classes, Integer pageNum, Integer pageSize);

    /**
     * 查询单个
     * @param classes
     * @return
     */
    Classes findOne(Classes classes);

    /**
     * 得到所有的老师信息
     * @return
     */
    Map<String, Object> findTeacherInfo();

    int  insertClasses(Classes classes);//添加
    int  deleteClassesById(Integer id);//删除
    int  updateClasses(Classes classes);//修改

    /**
     * 查询所有的班级
     * @return
     */
    List<Classes> findByClassName ();
    void  deleteClassesByIds(String[] stu);
    int findsClassName(String name);

    /**
     * 根据年级得到班级的id和名字
     * @param termId
     * @return
     */
    List<Classes> findClassesByTerm(Integer termId);
}
