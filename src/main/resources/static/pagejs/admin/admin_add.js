/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;

    //判断操作的结果
    if(null != rst && undefined != rst && rst != '' && rst.length > 0){
        //-3表示为null，-2表示号码已经存在
        if(rst == -3){
            layer.msg('请填写完整的信息！');
        }else if(rst == -2){
            layer.msg('手机号码已经存在，请重新输入！');
        } else {
            layer.msg('操作异常，请联系管理员！');
        }
    }
    //验证电话号码是否已经存在
    $("#phone").blur(function(){
        var phone = $("#phone").val();
        if(phone == null || phone == undefined || phone.length <= 0){
            return;
        }
        var id = $("[name=id]").val() ;
        $.post("/admin/checkPhone","phone=" + phone + "&loginId="+id,function (data) {
            if (data > 0) {
                layer.msg('该电话已存在！', function(){
                    $("#phone").val($("#hiddenPhone").val());
                });
            }
        },"text")
    }) ;
    //表单的提交，非空验证
    $("form").submit(function () {
        var name = $("#name").val();
        if(name == null || name == undefined || name == '' || name.trim().length <= 0){
            layer.msg("昵称不能为空！");
            return false;
        }
        var phone = $("#phone").val();
        if(phone == null || phone == undefined || phone == '' || phone.trim().length <= 0){
            layer.msg("电话不能为空！");
            return false;
        }
    });
})