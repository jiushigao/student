package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.*;
import com.student.student.pojo.*;
import com.student.student.pojo.stu.StudentAdd;
import com.student.student.pojo.stu.StudentList;
import com.student.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者：Dora
 * 日期：2019-05-22 11:53
 * 学生信息管理
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudemtMapper studemtMapper;   //学员管理
    @Autowired
    private LoginMapper loginMapper;//登陆表
    @Autowired
    private SchoolMapper schoolMapper;             //校区管理
    @Autowired
    private AgeMapper ageMapper;                   //年龄阶段管理
    @Autowired
    private StatusMapper statusMapper;              //状态管理
    @Autowired
    private CommitteMapper committeMapper;          //班级职位管理
    @Autowired
    private ProducttypeMapper producttypeMapper;    //产品管理
    @Autowired
    private StudentTypeMapper studentTypeMapper;    //学员分类管理
    @Autowired
    private  ClassesMapper classesMapper;           //班级管理

    /**
     * 进入新增或修改的页面
     * @param loginId
     * @return
     */
    @Override
    public Map<String, Object> toAddOrupdate(Integer loginId){
        Map<String, Object> rstMap = new HashMap<>();
        //学员基本信息
        if(loginId != null && loginId > 0){
            StudentAdd stu = studemtMapper.findStuById(loginId);
            rstMap.put( "student", stu );
        }
        //校区
        List<School> schoolList = schoolMapper.selectList( null );
        rstMap.put( "schoolList", schoolList );
        //产品
        List<Producttype> producttypesList  = producttypeMapper.selectList( null );
        rstMap.put( "producttypesList", producttypesList );
        //班级
        List<Classes> classesList = classesMapper.findByClassName();
        rstMap.put( "classesList", classesList );
        //年龄阶段
        List<Age> ageList = ageMapper.selectAll( null );
        rstMap.put( "ageList", ageList );
        //学员状态管理
        List<Status> statusList  =  statusMapper.selectList( null );
        rstMap.put( "statusList", statusList );
        //职位
        List<Committe> positionList  = committeMapper.selectList( null );
        rstMap.put( "positionList", positionList );
        //学员分类
        List<Studenttype> studenttypeList = studentTypeMapper.selectList( null );
        rstMap.put( "studenttypeList", studenttypeList );

        return rstMap;
    }


    /**
     * 查询所有
     * @return
     */
    @Override
    public PageInfo<StudentList> findStudentAll(Student student, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<StudentList> list = studemtMapper.findList();
        if(list != null && list.size() > 0){
            for (StudentList stu : list) {
                if(stu.getDescribe() != null && stu.getDescribe().trim().length() > 10 ){
                    stu.setDescribe( stu.getDescribe().substring( 0,10 ) + "..."  );
                }
                if(stu.getRemarks() != null && stu.getRemarks().trim().length() > 10 ){
                    stu.setRemarks( stu.getRemarks().substring( 0,10 ) + "..." );
                }
            }
        }
        PageInfo<StudentList> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteStudentById(Integer stuLoginId) {
        int rst = studemtMapper.deleteStudentById(stuLoginId);
        if(rst <= 0){
            return rst;
        }
        /*删除login中对应数据*/
        return loginMapper.delById(stuLoginId);
    }

    @Override
    public int countByCard(String idCard, Integer loginId) {
        return studemtMapper.countByCard(idCard, loginId);
    }


    /**
     * 插入学员信息
     * @param student
     * @return -3表示为null，-2表示号码已经存在
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int add(StudentAdd student) {
        //再次phone,idCard
        int rst = studemtMapper.countByIdCardAPhone(student.getIdCard(), student.getPhone(), student.getStuLoginId());
        if(rst > 0){
            return -2;
        }
        //添加登录信息
        Login login = new Login(  );
        login.setPhone( student.getPhone() );
        login.setName( student.getStuName() );
        login.setRoleId( 4 );
        rst = loginMapper.insert( login );
        if(rst < 0){
            return rst;
        }
        //添加学员基本信息
        student.setStuLoginId( login.getId() );
        return studemtMapper.insert(student);
    }

    /**
     * 修改
     * @param student
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(StudentAdd student) {
        //添加登录信息
        Login login = new Login(  );
        login.setPhone( student.getPhone() );
        login.setName( student.getStuName() );
        login.setId( student.getStuLoginId() );
        login.setRoleId( 4 );
        int rst = loginMapper.updateById( login );
        if(rst < 0){
            return rst;
        }
        //添加学员基本信息
        return studemtMapper.update( student );
    }

}
