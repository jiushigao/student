package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Permission;

import java.util.List;
import java.util.Map;

/**
 * 权限
 * 作者：Dora
 * 日期：2019-06-08 13:02
 */
public interface PermissionService {
    List<Permission> findList(Permission permission);


    /**
     * 根据条件查询所有状态信息,分页
     */
    PageInfo<Permission> findListPage(Permission permission, Integer pageNum, Integer pageSize);

    /**
     * 根据主键查询数据
     */
    Permission findOne(Permission permission);

    /**
     * 进入一级菜单的编辑界面
     * @param permission
     * @return
     */
    Map<String, Object> toLevel1Update(Permission permission);

    /**
     * 插入数据
     */
    int insert(String permission,String list);

    /**
     * 根据主键删除数据
     */
    int delById(Integer id);

    /**
     * 批量删除数据
     */
    int delByIds(List<String> ids);

    /**
     * 判断名字是否唯一
     */
    int findByName( Permission permission);

    /**
     * 更新一级权限菜单
     * @param perStr
     * @param perLevel2list
     * @param delLevel2Ids 被删除的二级菜单id的集合
     * @return
     */
    int updateLevel1(String perStr,String perLevel2list, String delLevel2Ids);
}
