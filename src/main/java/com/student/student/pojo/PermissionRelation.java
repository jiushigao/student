package com.student.student.pojo;

import java.util.List;

/**
 * 角色所对应权限
 * 作者：Dora
 * 日期：2019-06-08 09:37
 */
public class PermissionRelation {
    /**权限id*/
    private Integer permissionId;
    /**权限名字*/
    private String permissionName;
    /**是否选中*/
    private Boolean hasCheck;
    /** 对应的二级菜单*/
    private List<PermissionRelation> level2List;

    public PermissionRelation() {
    }
    public PermissionRelation(Integer permissionId, String permissionName, Boolean hasCheck) {
        this.permissionId = permissionId;
        this.permissionName = permissionName;
        this.hasCheck = hasCheck;
    }

    /**权限id*/
    public Integer getPermissionId() {
        return permissionId;
    }
    /**权限id*/
    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }
    /**权限名字*/
    public String getPermissionName() {
        return permissionName;
    }
    /**权限名字*/
    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }
    /**是否选中*/
    public Boolean getHasCheck() {
        return hasCheck;
    }
    /**是否选中*/
    public void setHasCheck(Boolean hasCheck) {
        this.hasCheck = hasCheck;
    }
    /** 对应的二级菜单*/
    public List<PermissionRelation> getLevel2List() {
        return level2List;
    }
    /** 对应的二级菜单*/
    public void setLevel2List(List<PermissionRelation> level2List) {
        this.level2List = level2List;
    }
}
