package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.School;
import com.student.student.pojo.Status;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StatusService {
    /**
     * 根据条件查询所有状态信息,不分页
     */
    List<Status> findList(Status status);

   /**
     * 根据条件查询所有状态信息,分页
     */
    PageInfo<Status> findListPage(Status status, Integer pageNum, Integer pageSize);

    /**
     * 返回单个实体
     */
    Status findOne(Status status);

    /**
     * 插入数据
     */
    int insertStatus(Status status);

    /**
     * 修改数据
     */
    int updateStatus(Status status);

    /**
     * 根据主键删除数据
     */
    int deleteStatusById(Integer id);

    /**
     * 判断校区名字是否唯一
     */
    int findByStatusName(String name);
}
