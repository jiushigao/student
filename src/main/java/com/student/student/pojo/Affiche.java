/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

import java.util.Date;

/**
 * 公告(affiche)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Affiche implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -3855357246874851242L;

    /** 公告id */
    private Integer id;

    /** 公告标题 */
    private String title;

    /** 公告内容 */
    private String content;

    /** 公告发布时间 */
    private String  time;

    /** 公告权限 0=学生看 1=教职看 */
    private Integer permission;

    /**
     * 获取公告id
     * 
     * @return 公告id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置公告id
     * 
     * @param id
     *          公告id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取公告标题
     * 
     * @return 公告标题
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * 设置公告标题
     * 
     * @param title
     *          公告标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取公告内容
     * 
     * @return 公告内容
     */
    public String getContent() {
        return this.content;
    }

    /**
     * 设置公告内容
     * 
     * @param content
     *          公告内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取公告发布时间
     * 
     * @return 公告发布时间
     */
    public String getTime() {
        return this.time;
    }

    /**
     * 设置公告发布时间
     * 
     * @param time
     *          公告发布时间
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * 获取公告权限 0=学生看 1=教职看
     * 
     * @return 公告权限 0=学生看 1=教职看
     */
    public Integer getPermission() {
        return this.permission;
    }

    /**
     * 设置公告权限 0=学生看 1=教职看
     * 
     * @param permission
     *          公告权限 0=学生看 1=教职看
     */
    public void setPermission(Integer permission) {
        this.permission = permission;
    }
}