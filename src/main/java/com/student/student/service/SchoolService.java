package com.student.student.service;

/**
 * School的service接口层
 */

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.InterviewFun;
import com.student.student.pojo.School;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SchoolService {
    /**
     * 根据条件查询所有校区信息,不分页
     */
    List<School> findList(School school);

    /**
     * 根据条件查询所有校区信息,分页
     */
    PageInfo<School> findListPage(School school, Integer pageNum, Integer pageSize);

    /**
     * 返回单个实体
     */
    School findOne(School school);

    /**
     * 插入数据
     */
    int insertSchool(School school);

    /**
     * 修改数据
     */
    int updateSchool(School school);

    /**
     * 根据主键删除数据
     */
    int deleteSchoolById(Integer id);

    /**
     * 批量删除
     */
    int delByIds(List<String> ids);

    /**
     * 统计校区名称是否已经存在
     */
    int countByName(School school);

}
