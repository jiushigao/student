package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Committe;
import com.student.student.pojo.InterviewType;
import com.student.student.pojo.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 班委管理
 * @author 资凯
 */
public interface CommitteService {

    /**
     * 根据条件查询班级职位,返回list集合
     */
    PageInfo<Committe> findList(Committe committe, Integer pageNum, Integer pageSize);
    /**
     * 根据学生id修改班级职位
     */
    int updateCommitteByStu(Integer committeId, Integer studentId);
    /**
     * 修改班级职位名称
     */
    int updateCommitteById(Committe committe);
    /**
     * 根据班级id查询对应班委信息
     */
    List<Student> findAllByClassesId(Map<String, Object> condition);

    /**
     * 不分页查询全部
     */
    List<Committe> findList(Committe committe);






    /**
     * 查询班级对应的学生
     */
    //List<Student> findStuByClassesId(Integer ClassesId);


    /**
     * 根据学生id查询其职位
     */
    //Student findCommitteByStuId(Integer stuId);

    /**
     * 新增班级职位
     */
    int insertCommitte(Committe committe);

    /**
     * 删除班级职位
     */
    int deleteCommitteById(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(List<String> ids);

    /**
     * 根据职位id查询
     */
    Committe selectOne(Committe committe);

    /**
     * 统计类型名称是否已经存在
     * @param committe
     * @return
     */
    int countByName(Committe committe);
}
