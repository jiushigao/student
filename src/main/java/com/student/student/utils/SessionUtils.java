package com.student.student.utils;

import com.student.student.pojo.Login;
import com.student.student.pojo.Permission;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 将数据保存到session的工具类
 * 作者：Dora
 * 日期：2019-06-08 11:12
 */
public class SessionUtils {

    /**
     * 将登录数保存到session中
     * @param session
     * @param login
     */
    public static void saveLoginToSession(HttpSession session,  Login login){
        // login = new Login( 1,"admin", "admin", 1 ); //直接把登录用户写死，方便测试
        //将登录数据保存到session中
        session.setAttribute( "login", login );

        List<Permission> oneMenu = new ArrayList<>();
        Map<Integer, List<Permission>> twoMenu = new HashMap<>();
        for (Permission permission : login.getPermissionList()){

            if(permission.getLevel() == 1){ //第一级菜单
                oneMenu.add( permission );
                twoMenu.put( permission.getId(), new ArrayList<>() );
            }else{ //第二级菜单
                List<Permission> menu = new ArrayList<>();
                if(twoMenu.containsKey( permission.getParentId())){
                    menu = twoMenu.get( permission.getParentId() );
                }
                menu.add( permission );
                twoMenu.put( permission.getParentId(), menu );
            }
        }
        session.setAttribute( "oneMenu", oneMenu );
        session.setAttribute( "twoMenu", twoMenu );
    }

    /**
     * 将数据从session中移除
     * @param session
     */
    public static void removeLoginFromSession(HttpSession session){
        session.removeAttribute( "login" );
        session.removeAttribute( "oneMenu" );
        session.removeAttribute( "twoMenu" );
    }
}
