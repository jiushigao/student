package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.LoginIp;
import com.student.student.service.LoginIpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

@Controller
@RequestMapping("/loginIp")
public class LoginIpController {

    @Autowired
    private LoginIpService loginIpService;



    /**
     * 根据条件查询所有
     */
    @RequestMapping("/list")
    public String list(LoginIp loginIp, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<LoginIp> pageInfo = loginIpService.findListPage(loginIp, pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);

        model.addAttribute("status",loginIp);
        return "loginip/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/loginIp.json")
    @ResponseBody
    public PageInfo listPageJson(LoginIp loginIp, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<LoginIp> pageInfo = loginIpService.findListPage(loginIp, pageNum, pageSize);
        return  pageInfo;
    }

}
