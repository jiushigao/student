/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;


import java.sql.Date;

/**
 * 学员信息(student)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Student implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -4575804369667527771L;

    /** 主键 */
    private Integer id;

    /** 学员id */
    private Integer stuLoginId;

    /** 校区；建表 */
    private School school;

    /** 班级 */
    private Classes classes;
    /** 班级Id */
    private Integer classesId;

    /** 教员老师 */
    private Teacher technologyTeacher;

    /** 班主任老师 */
    private Teacher lifeTeacher;

    /** 学员姓名 */
    private String name;

    /** 性别 */
    private String sex;

    /** QQ */
    private String qq;

    /** 学员基本情况描述 */
    private String describe;

    /** 家庭经济来源 */
    private String economy;

    /** 工作经历 */
    private String work;

    /** 游戏经历 */
    private String game;

    /** 推荐人 */
    private String referee;

    /** 学员状态0、就读1、毕业2、退学3、休学，*/
    private Integer status;
    /** 身份证 */
    private String idCard;

    /** 家庭住址 */
    private String address;

    /** 备注 */
    private String remarks;

    /** 创建人 */
    private String creater;

    /** 学员信息创建时间 */
    private Date createTime;

    /** 学员信息最近的修改时间 */
    private Date updateTime;


    /** 年龄阶段*/
    private Age age;

    /**所读产品 */
    private Producttype producttype;
    /**所在学期 */
    private Term term;
    /**学员分类*/
    private Studenttype studenttype;
    /**学员职位*/
    private Committe committe;

    public Student() {}
    public Student(Integer id, String idCard) {
        this.id = id;
        this.idCard = idCard;
    }

    /**
     * 获取主键
     * 
     * @return 主键
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置主键
     * 
     * @param id
     *          主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取学员id
     * 
     * @return 学员id
     */
    public Integer getStuLoginId() {
        return this.stuLoginId;
    }

    /**
     * 设置学员id
     * 
     * @param stuLoginId
     *          学员id
     */
    public void setStuLoginId(Integer stuLoginId) {
        this.stuLoginId = stuLoginId;
    }

    /**
     * 获取学员姓名
     * 
     * @return 学员姓名
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置学员姓名
     * 
     * @param name
     *          学员姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取性别
     * 
     * @return 性别
     */
    public String getSex() {
        return this.sex;
    }

    /**
     * 设置性别
     * 
     * @param sex
     *          性别
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 获取QQ
     * 
     * @return QQ
     */
    public String getQq() {
        return this.qq;
    }

    /**
     * 设置QQ
     * 
     * @param qq
     *          QQ
     */
    public void setQq(String qq) {
        this.qq = qq;
    }

    /**
     * 获取学员基本情况描述
     * 
     * @return 学员基本情况描述
     */
    public String getDescribe() {
        return this.describe;
    }

    /**
     * 设置学员基本情况描述
     * 
     * @param describe
     *          学员基本情况描述
     */
    public void setDescribe(String describe) {
        this.describe = describe;
    }

    /**
     * 获取家庭经济来源
     * 
     * @return 家庭经济来源
     */
    public String getEconomy() {
        return this.economy;
    }

    /**
     * 设置家庭经济来源
     * 
     * @param economy
     *          家庭经济来源
     */
    public void setEconomy(String economy) {
        this.economy = economy;
    }

    /**
     * 获取工作经历
     * 
     * @return 工作经历
     */
    public String getWork() {
        return this.work;
    }

    /**
     * 设置工作经历
     * 
     * @param work
     *          工作经历
     */
    public void setWork(String work) {
        this.work = work;
    }

    /**
     * 获取游戏经历
     * 
     * @return 游戏经历
     */
    public String getGame() {
        return this.game;
    }

    /**
     * 设置游戏经历
     * 
     * @param game
     *          游戏经历
     */
    public void setGame(String game) {
        this.game = game;
    }

    /**
     * 获取推荐人
     * 
     * @return 推荐人
     */
    public String getReferee() {
        return this.referee;
    }

    /**
     * 设置推荐人
     * 
     * @param referee
     *          推荐人
     */
    public void setReferee(String referee) {
        this.referee = referee;
    }

    /**
     * 获取学员状态
0、就读1、毕业2、退学3、休学，


     * 
     * @return 学员状态
0、就读1、毕业2、退学3、休学
     */
    public Integer getStatus() {
        return this.status;
    }

    /**
     * 设置学员状态
0、就读1、毕业2、退学3、休学，


     * 
     * @param status
     *          学员状态
0、就读1、毕业2、退学3、休学
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取身份证
     * 
     * @return 身份证
     */
    public String getIdCard() {
        return this.idCard;
    }

    /**
     * 设置身份证
     * 
     * @param idCard
     *          身份证
     */
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    /**
     * 获取家庭住址
     * 
     * @return 家庭住址
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * 设置家庭住址
     * 
     * @param address
     *          家庭住址
     */
    public void setAddress(String address) {
        this.address = address;
    }



    /**
     * 获取备注
     * 
     * @return 备注
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * 设置备注
     * 
     * @param remarks
     *          备注
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取创建人
     * 
     * @return 创建人
     */
    public String getCreater() {
        return this.creater;
    }

    /**
     * 设置创建人
     * 
     * @param creater
     *          创建人
     */
    public void setCreater(String creater) {
        this.creater = creater;
    }

    /**
     * 获取学员信息创建时间
     * 
     * @return 学员信息创建时间
     */
    public Date getCreateTime() {
        return this.createTime;
    }

    /**
     * 设置学员信息创建时间
     * 
     * @param createTime
     *          学员信息创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取学员信息最近的修改时间
     * 
     * @return 学员信息最近的修改时间
     */
    public Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * 设置学员信息最近的修改时间
     * 
     * @param updateTime
     *          学员信息最近的修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 校区
     * @return
     */
    public School getSchool() {
        return school;
    }
    /**
     * 校区
     * @return
     */
    public void setSchool(School school) {
        this.school = school;
    }

    /**班级*/
    public Classes getClasses() {
        return classes;
    }

    /**班级*/
    public void setClasses(Classes classes) {
        this.classes = classes;
    }
    /**教员*/
    public Teacher getTechnologyTeacher() {
        return technologyTeacher;
    }
    /**教员*/
    public void setTechnologyTeacher(Teacher technologyTeacher) {
        this.technologyTeacher = technologyTeacher;
    }
    /**班主任*/
    public Teacher getLifeTeacher() {
        return lifeTeacher;
    }
    /**班主任*/
    public void setLifeTeacher(Teacher lifeTeacher) {
        this.lifeTeacher = lifeTeacher;
    }
    /**年龄阶段 */
    public Age getAge() {
        return age;
    }
    /** 年龄阶段*/
    public void setAge(Age age) {
        this.age = age;
    }
    /**所读产品 */
    public Producttype getProducttype() {
        return producttype;
    }
    /** 所读产品*/
    public void setProducttype(Producttype producttype) {
        this.producttype = producttype;
    }
    /** 所在学期*/
    public Term getTerm() {
        return term;
    }
    /** 所在学期*/
    public void setTerm(Term term) {
        this.term = term;
    }
    /** 学员分类*/
    public Studenttype getStudenttype() {
        return studenttype;
    }
    /** 学员分类*/
    public void setStudenttype(Studenttype studenttype) {
        this.studenttype = studenttype;
    }
    /** 学员职位*/
    public Committe getCommitte() {
        return committe;
    }
    /**  学员职位*/
    public void setCommitte(Committe committe) {
        this.committe = committe;
    }
    /**  班级id*/
    public Integer getClassesId() {
        return classesId;
    }
    /**  班级id*/
    public void setClassesId(Integer classesId) {
        this.classesId = classesId;
    }
}