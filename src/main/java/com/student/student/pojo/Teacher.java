/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

/**
 * 教职工表(teacher)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Teacher implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -7360842925947463949L;
    /** 职工id */
    private Integer loginId;
    /** 员工姓名 */
    private String name;
    /** 电话 */
    private String phone;
    /** 角色id */
    private Integer roleId;
    /** 角色名称 */
    private String roleName;
    /** 性别 */
    private String sex;
    /** 家庭住址 */
    private String address;

    public Teacher() {
    }
    public Teacher(Integer loginId) {
        this.loginId = loginId;
    }

    /** 员工姓名 */
    public String getName() {
        return this.name;
    }
    /** 员工姓名 */
    public void setName(String name) {
        this.name = name;
    }
    /** 电话 */
    public String getPhone() {
        return phone;
    }
    /** 电话 */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /** 职工id */
    public Integer getLoginId() {
        return loginId;
    }
    /** 职工id */
    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }
    /**
     * 获取性别
     * 
     * @return 性别
     */
    public String getSex() {
        return this.sex;
    }
    /**
     * 设置性别
     * 
     * @param sex
     *          性别
     */
    public void setSex(String sex) {
        this.sex = sex;
    }
    /**
     * 获取家庭住址
     * 
     * @return 家庭住址
     */
    public String getAddress() {
        return this.address;
    }
    /**
     * 设置家庭住址
     * 
     * @param address
     *          家庭住址
     */
    public void setAddress(String address) {
        this.address = address;
    }
    /** 角色id */
    public Integer getRoleId() {
        return roleId;
    }
    /** 角色id */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
    /** 角色名称 */
    public String getRoleName() {
        return roleName;
    }
    /** 角色名称 */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}