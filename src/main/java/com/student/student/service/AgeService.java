package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Age;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AgeService {
    /*查询所有*/
    PageInfo<Age> findListPage(Age age, Integer pageNum, Integer pageSize);
    /*查询所有*/
    List<Age> fingList(Age age);
    //删除
    int delAge(Integer id);
    //根据年龄查询
    Age selectById(Integer id);
    //修改
    int updateAge(Age age);
    //新增
    int insertAge(Age age);
    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(List<String> ids);
    /**
     * 查询名称是否存在
     */
    int selectByName(Age age);
}
