/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;

    //判断操作的结果
    if(null != rst && undefined != rst && rst == -1){
        layer.msg('操作异常，请联系管理员！', {time: 500});
    }
    //全选
    form.on('checkbox(allChoose)', function(data){
        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
        child.each(function(index, item){
            item.checked = data.elem.checked;
        });
        form.render('checkbox');
    });

    //分页
    laypage({
        cont: 'page',
        pages: pages, //总页数
        groups: 5, //连续显示分页数
        limit: limit,  //每页显示条数
        jump: function (obj, first) {
            if(!first){
                var name = $("[name=name]").val();
                var phone = $("[name=phone]").val();
                var roleId = $("[name=roleId]").val();
                $.post("/teacher/teacher.json", "pageNum="+obj.curr+"&pageSize="+obj.limit+"&name="+name+"&phone="+phone+"&roleId="+roleId,function(data){
                    $(".news_content").html('');
                    var info = '';
                    $(data.list).each(function(){
                        info += '<tr>';
                        info += '<td><input name="checked" lay-skin="primary" lay-filter="choose" type="checkbox"><div class="layui-unselect layui-form-checkbox"><span>勾选</span><i class="layui-icon"></i></div></td>';
                        info += '<td>' + this.loginId + '</td>';
                        info += '<td>' + this.name + '</td>';
                        info += '<td>' + this.roleName + '</td>';
                        info += '<td>' + this.sex + '</td>';
                        info += '<td>' + this.phone + '</td>';
                        info += '<td>' + this.address + '</td>';
                        info += '<td> <a class="layui-btn layui-btn-mini news_edit" href="/teacher/toAddOrUpdate?loginId='+this.loginId+'"><i class="iconfont icon-edit"></i> 编辑</a>';
                        info += '<a class="layui-btn layui-btn-danger layui-btn-mini news_del splitJS" value="' + this.loginId + '"><i class="layui-icon">&#xe640;</i> 删除</a>';
                        info += '</td>';
                        info += '</tr>';
                    });
                    $(".news_content").html(info);
                },"json");
            }
        }
    });

    //批量删除
    $(".batchDel").click(function(){
        var $checkbox = $('.news_list tbody input[type="checkbox"][name="checked"]');
        var $checked = $('.news_list tbody input[type="checkbox"][name="checked"]:checked');
        if($checkbox.is(":checked")){
            layer.confirm('确定删除选中的信息？',{icon:3, title:'提示信息'},function(index){
                var ids = "";
                for(var j = 0; j < $checked.length; j++){
                    var id = $checked.eq(j).parents("tr").find(".news_del").attr("value");
                    ids += id + ",";
                }
                ids = ids.substring(0, ids.length - 1);
                $.post("/teacher/delByIds", "ids="+ ids, function (data) {
                    if(data > 0){
                        layer.msg('删除成功！', {time: 500}, function(){
                            location.href= "/teacher/list";
                        });
                    }else{
                        layer.msg('删除失败，请联系管理员！', {time: 500});
                    }
                }, "text");
            })
        }else{
            layer.msg("请选择需要删除信息");
        }
    })
    //删除
    $("body").on("click",".news_del",function(){
        var loginId = $(this).attr("value");
        layer.confirm('确定删除此信息？',{icon:3, title:'提示信息'},function(index){
            $.post("/teacher/delById", "loginId="+ loginId, function (data) {
                if(data > 0){
                    layer.msg('删除成功！', {time: 500}, function(){
                        location.href= "/teacher/list";
                    });
                }else{
                    layer.msg("删除失败，请联系管理员", {time: 500});
                }
            }, "text");
        });
    })
})