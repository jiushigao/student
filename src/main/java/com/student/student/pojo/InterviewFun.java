package com.student.student.pojo;

/**
 * interview
 *
 * @author laoK
 * @version 1.0.0 2019-05-27
 */
public class InterviewFun implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -8910863403686889879L;

    /**
     * 主键
     */
    private Integer id;
    /**
     * 跟进方式
     */
    private String funName;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getFunName() {
        return funName;
    }
    public void setFunName(String funName) {
        this.funName = funName;
    }


    public InterviewFun() {
    }
    public InterviewFun(Integer id) {
        this.id = id;
    }
    public InterviewFun(String funName) {
        this.funName = funName;
    }
    public InterviewFun(Integer id, String funName) {
        this.id = id;
        this.funName = funName;
    }
}
