/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;

    //判断操作的结果
    if(null != rst && undefined != rst && rst != '' && rst.length > 0){
        if(rst == -3){
            layer.msg('请填写完整的信息！', {time: 1000});
        } else if(rst == -2){
            layer.msg('类型已经存在，请重新输入！', {time: 1000});
        }else if(rst <= 0) {
            layer.msg('操作异常，请联系管理员！', {time: 1000});
        }
    }
    //验证名字是否已经存在
    $("[name=typeName]").blur(function(){
        var typeName = $(this).val();
        if(typeName == null || typeName == undefined || typeName.length <= 0){
            return;
        }
        var id = $("[name=id]").val() ;
        id = id == null || id == undefined ? -1 :id;
        $.post("/interviewType/checkName","name=" + typeName + "&id="+id,function (data) {
            if (data > 0) {
                layer.msg('该类型已存在！', {time: 1000}, function(){
                    $("[name=typeName]").val($("[name=hiddenTypeName]").val());
                });
            }
        },"text")
    }) ;
    //表单的提交，非空验证
    $("form").submit(function () {
        var name = $("[name=typeName]").val();
        if(name == null || name == undefined || name == '' || name.trim().length <= 0){
            layer.msg("类型不能为空！", {time: 1000});
            return false;
        }
    });
})