/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

/**
 * 年龄阶段(age)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Age implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 8144119221637547960L;

    /** 年龄阶段id */
    private Integer id;

    /** 年龄阶段名称 */
    private String name;

    /**
     * 获取年龄阶段id
     * 
     * @return 年龄阶段id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置年龄阶段id
     * 
     * @param id
     *          年龄阶段id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取年龄阶段名称
     * 
     * @return 年龄阶段名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置年龄阶段名称
     * 
     * @param name
     *          年龄阶段名称
     */
    public void setName(String name) {
        this.name = name;
    }
}