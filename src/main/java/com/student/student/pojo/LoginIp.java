package com.student.student.pojo;


import java.sql.Timestamp;

/**
 * 登录日志
 */
public class LoginIp {

    private  Integer id;
    private  Integer loginId;
    private  String ip;
    private  String address;
    private Timestamp loginTime;
    private  String phone;

    public LoginIp() {
    }
    public LoginIp(Integer loginId, String ip, String address, Timestamp loginTime) {
        this.loginId = loginId;
        this.ip = ip;
        this.address = address;
        this.loginTime = loginTime;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public Integer getLoginId() {
        return loginId;
    }
    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }
    public Timestamp getLoginTime() {
        return loginTime;
    }
    public void setLoginTime(Timestamp loginTime) {
        this.loginTime = loginTime;
    }
}
