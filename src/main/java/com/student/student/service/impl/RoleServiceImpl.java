package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.PermissionMapper;
import com.student.student.mapper.RoleMapper;
import com.student.student.mapper.RolePermissionMapper;
import com.student.student.pojo.Permission;
import com.student.student.pojo.Role;
import com.student.student.pojo.PermissionRelation;
import com.student.student.pojo.RolePermission;
import com.student.student.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 作者：Dora
 * 日期：2019-06-08 13:03
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper; //角色
    @Autowired
    private PermissionMapper permissionMapper; //权限
    @Autowired
    private RolePermissionMapper rolePermissionMapper;  //角色对应全新啊

    @Override
    public List<Role> findList(Role role) {
        return roleMapper.selectList( role);
    }

    @Override
    public PageInfo<Role> findListPage(Role role, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Role> list = roleMapper.selectList(role);
        PageInfo<Role> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public Map<String, Object> toUpdate(int roleId) {
        //得到所有的权限
        List<Permission> perList = permissionMapper.selectList( null ); //所有的权限
        List<Integer> myPerList = permissionMapper.selectListByRole( roleId ); //当前角色对应的权限

        List<PermissionRelation> rstList = new ArrayList<>();
        Map<Integer, List<PermissionRelation>> level2Map= new HashMap<>();
        for (Permission p : perList) {
            boolean hasCheck = myPerList.contains(p.getId()) ? true : false;
            if(p.getParentId() == -1){ //一级目录
                rstList.add(new PermissionRelation(p.getId(), p.getName(), hasCheck) );
            }else{
                if(level2Map.containsKey( p.getParentId() )){
                    level2Map.get(p.getParentId()).add(new PermissionRelation(p.getId(), p.getName(), hasCheck)  );
                }else{
                    List<PermissionRelation> list = new ArrayList<>();
                    list.add( new PermissionRelation(p.getId(), p.getName(), hasCheck) );
                    level2Map.put(p.getParentId(), list);
                }
            }
        }
        for (PermissionRelation rp : rstList) {
            rp.setLevel2List( level2Map.get( rp.getPermissionId() ) );
        }
        Map<String, Object> rstMap = new HashMap<>();
        rstMap.put( "permissionList" , rstList);
        rstMap.put( "role", roleMapper.selectOne( new Role( roleId ) )  );
        return rstMap;
    }

    @Override
    public Map<String, Object> toAdd() {
        //得到所有的权限
        List<Permission> perList = permissionMapper.selectList( null ); //所有的权限

        List<PermissionRelation> rstList = new ArrayList<>();
        Map<Integer, List<PermissionRelation>> level2Map= new HashMap<>();
        for (Permission p : perList) {
            if(p.getParentId() == -1){ //一级目录
                rstList.add(new PermissionRelation(p.getId(), p.getName(), false) );
            }else{
                if(level2Map.containsKey( p.getParentId() )){
                    level2Map.get(p.getParentId()).add(new PermissionRelation(p.getId(), p.getName(), false)  );
                }else{
                    List<PermissionRelation> list = new ArrayList<>();
                    list.add( new PermissionRelation(p.getId(), p.getName(), false) );
                    level2Map.put(p.getParentId(), list);
                }
            }
        }
        for (PermissionRelation rp : rstList) {
            rp.setLevel2List( level2Map.get( rp.getPermissionId() ) );
        }
        Map<String, Object> rstMap = new HashMap<>();
        rstMap.put( "permissionList" , rstList);
        return rstMap;
    }

    @Override
    public int insert(String name, String permissions) {

        int rst = roleMapper.findByName( name, null );
        if(rst > 0){
            return -2;
        }
        //插入角色
        Role role = new Role(name);
        rst = roleMapper.insert( role );
        if(rst <= 0){
            return  rst;
        }
        //插入角色对应的权限
        if(permissions.length() > 0){
            if(permissions.lastIndexOf( "," ) == permissions.length() - 1){
                permissions = permissions.substring( 0, permissions.length() -1 );
            }
            String[] perIds = permissions.split( "," );
            List<RolePermission> rpList = new ArrayList<>();
            for (String perId : perIds) {
                rpList.add( new RolePermission(role.getId(), Integer.parseInt( perId )) );
            }
            rst = rolePermissionMapper.inserts( rpList );
        }
        return rst;
    }

    @Override
    public int update(Integer id, String name, String permissions) {
        int rst = roleMapper.findByName( name, id );
        if(rst > 0){
            return -2;
        }
        rst = roleMapper.update( new Role(id, name) );
        if(rst <=  0){
            return rst;
        }

        //先删除对应角色的所有权限，不用判断是否大于0，因为可以之前的没有权限
        rolePermissionMapper.delByRoleId( id );
        //在把所有的权限添加
        if(permissions.length() > 0){
            if(permissions.lastIndexOf( "," ) == permissions.length() - 1){
                permissions = permissions.substring( 0, permissions.length() -1 );
            }
            String[] perIds =  permissions.split( "," );
            List<RolePermission> rpList = new ArrayList<>();
            for (String perId : perIds) {
                rpList.add( new RolePermission(id, Integer.parseInt( perId )) );
            }
            rst = rolePermissionMapper.inserts( rpList );
        }
        return rst;
    }

    @Override
    public int delById(Integer id) {
        return roleMapper.delById( id );
    }

    @Override
    public int delByIds(List<String> ids) {
        return roleMapper.delByIds( ids );
    }

    @Override
    public int findByName(String name, Integer id) {
        return roleMapper.findByName( name, id );
    }
}
