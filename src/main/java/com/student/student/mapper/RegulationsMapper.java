package com.student.student.mapper;

import com.student.student.pojo.Regulations;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 校规Mapper
 */
@Mapper
public interface RegulationsMapper {
    /**
     * 返回list
     * @param regulations
     * @return
     */
    List<Regulations> selectList(Regulations regulations);

    /**
     * 返回单个实体
     * @param regulations
     * @return
     */
    Regulations selectOne(Regulations regulations);


    /**
     * 根据id删除校规
     * @param id
     */
    int deleteRegulationsById(@Param("id") String id);
    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deleteRegulationsByIds(@Param("ids") List<String> ids);

    /**
     * 获取实体类对象进行修改
     * @param regulations
     */
    int updateRegulations(Regulations regulations);

    /**
     * 获取实体类对象进行新增
     * @param regulations
     */
    int insertRegulations(Regulations regulations);
    /**
     * 根据条件统计
     * @param regulations
     * @return
     */
    int countByname(Regulations regulations);
}
