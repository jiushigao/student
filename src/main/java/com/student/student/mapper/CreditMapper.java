package com.student.student.mapper;

import com.student.student.pojo.Committe;
import com.student.student.pojo.Credit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CreditMapper {

    /**
     * 根据条件查询学分
     */
    List<Credit> selectList(Credit credit);
    /**
     * 新增
     * @param credit
     * @return
     */
    int insert(Credit credit);
    /**
     * 删除
     */
    int delete(Integer id);
    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deletes(@Param("ids") List<String> ids);

    /**
     * 多条件查询班级扣分情况
     */
    List<Credit> selectPoints(Map<String, Object> condition);
}
