package com.student.student.controller;

import com.student.student.pojo.Login;
import com.student.student.pojo.Role;
import com.student.student.service.LoginService;
import com.student.student.service.RoleService;
import com.student.student.utils.IPUtils;
import com.student.student.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * 登录
 * 作者：Dora
 * 日期：2019-05-22 13:27
 */
@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;  //登录
    @Autowired
    private RoleService roleService;    //角色

    /**
     * 进入登录页面
     * @return
     */
    @RequestMapping("/toLogin")
    public  String toLogin(Model model){
        List<Role> roleList = new ArrayList<>();
        try{
            roleList = roleService.findList( null );
            model.addAttribute( "roleLsit", roleList );
        }catch (Exception ex){
            ex.printStackTrace();;
        }
        return  "login";
    }

    /**
     * 登录
     * @return
     */
    @RequestMapping("/toIndex")
    public  String toIndex(){
        return "index";
    }

    /**
     * 登录
     * @param phone  电话
     * @param pwd 密码
     * @param roleId 角色id
     * @param roleName 角色名称
     * @param request
     * @return
     */
    @RequestMapping("/login.json")
    @ResponseBody
    public  String loginJson(String phone, String pwd, Integer roleId, String roleName, HttpServletRequest request){
        if(phone == null || phone.trim().length() <= 0 || pwd == null || pwd.trim().length() <= 0
            || roleId == null || roleId <= 0){ //非空判断
            return "非法请求路径";
        }
        int rst = -1;
        try {
            Map<String, String> ipAddressMap = IPUtils.getIpAndAddress( request );
            Login login = loginService.login( new Login(phone, pwd, roleId), ipAddressMap );
            if(login != null && login.getPhone() != null){
                rst = 1;
                login.setRoleName( roleName );
                SessionUtils.saveLoginToSession(request.getSession(), login  );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rst + "";
    }

    /**
     * 退出
     * @return
     */
    @RequestMapping("/exit")
    public  String exit(HttpServletRequest request){
        SessionUtils.removeLoginFromSession( request.getSession());
        return  "redirect:toLogin";
    }

    /**
     * 进入main页面
     * @return
     */
    @RequestMapping("/toMain")
    public String toMain(Model model){
        Map<String, Object> cntMap = new HashMap<>();
        try{
            cntMap = loginService.indexInfo();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        model.addAttribute( "cntMap", cntMap );
        return "main";
    }

    /**
     * 判断手机号码是否已经存在，存在返回>0
     * @param phone
     * @return
     */
    @RequestMapping("/checkPhone")
    @ResponseBody
    public Integer checkPhone(String phone, Integer id, Integer type){
        int rst = this.loginService.countByPhone( new Login(id, phone,type) );
        return rst;
    }

}
