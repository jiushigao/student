package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.*;
import com.student.student.pojo.stu.StudentAdd;
import com.student.student.pojo.stu.StudentList;
import com.student.student.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;
import java.util.Map;

/**
 * 学员信息管理
 * 作者：Dora
 * 日期：2019-05-22 11:31
 */
@Controller
@RequestMapping("/stus")
public class StuController {

    @Autowired
    private StudentService studentService; //学员管理

    /**
     * 进入学员列表页面
     * @return
     */
    @RequestMapping("/list")
    public  String index(Model model,Student student,Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<StudentList> pageInfo = new PageInfo<>(  );
        try {
            pageInfo = studentService.findStudentAll(student,pageNum,pageSize);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        model.addAttribute("pageInfo",pageInfo);
        return  "stu/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/student.json")
    @ResponseBody
    public PageInfo listPageJson(Student student, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<StudentList> pageInfo = new PageInfo<>(  );
        try {
            pageInfo = studentService.findStudentAll(student,pageNum,pageSize);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  pageInfo;
    }

    /**
     * 进入新增或修改页面
     * @param model
     * @return
     */
    @RequestMapping("/toAddOrUpdate")
    public String toAddOrUpdate(Model model, Integer loginId){
        Map<String, Object> rstMap = new HashMap<>();

        try{
            rstMap = studentService.toAddOrupdate(loginId);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        model.addAttribute( "method", 0 );
        if(null != loginId && loginId > 0){
            model.addAttribute( "method", 1);
        }else{
            rstMap.put( "student", new StudentAdd() );
        }
        model.addAttribute( "rstMap", rstMap );
        return  "stu/add";
    }

    /**
     * 根据学生id查询实体进行修改
     * @return
     */
    @RequestMapping("/upd")
    public  String upd(Model model, StudentAdd student) {
        int rst = -1;
        try {
            rst =studentService.update(student);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        if(rst <= 0){
            return  "redirect:/stus/toAddOrUpdate?loginId=" + student.getStuLoginId();
        }else{
            return  "redirect:/stus/list";
        }
    }

    /**
     * 添加学生信息
     * @return
     */
    @RequestMapping("/add")
    public  String add(Model model, StudentAdd student){
        int rst = -1;
        try {
            rst =studentService.add(student);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        if(rst <= 0){
            model.addAttribute("oper_rst",rst);
            return  "redirect:/stus/toAddOrUpdate";
        }else{
            return  "redirect:/stus/list";
        }
    }

    /**
     * 根据学员id删除信息
     * @return
     */
    @RequestMapping("/delid")
    @ResponseBody
    public  Integer deleteStudentById(Integer stuLoginId){
        int count = -1;
        try{
            count = studentService.deleteStudentById(stuLoginId);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return count;
    }

    /**
     * 验证身份证是否已经存在
     * @param idCard
     * @param model
     * @return 1表示手机号码已经存在，0表示不存在
     */
    @RequestMapping("/checkidCard")
    @ResponseBody
    public Integer checkidCard(String idCard, Integer loginId, Model model){
        int  rst = -1;
        try{
            rst = studentService.countByCard( idCard,loginId );
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(rst > 0){
            return 1;
        }else{
            return 0;
        }

    }
}