package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.*;
import com.student.student.service.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.crypto.Data;
import java.util.*;

@Controller
@RequestMapping("/credit")
public class CreditController {
    @Autowired
    private CreditService creditService;
    /**
     * 根据条件查询所有
     */
    @RequestMapping("/list")
    public String list(Credit credit , Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        /*Map<String, Object> map = new HashMap<>();
        map.put( "Classs", credit.getClasss() );
        map.put( "Term", credit.getTerm() );
        map.put( "Student", credit.getStudent());
        map.put( "Foul", credit.getFoul() );
        map.put( "Time", credit.getTime() );
        map.put( "Time1", time1 );*/
        PageInfo<Credit> pageInfo = creditService.selectList(credit,  pageNum, pageSize);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute( "Classs", credit.getClasss() );
        model.addAttribute( "Term", credit.getTerm() );
        model.addAttribute( "Student", credit.getStudent());
        model.addAttribute( "Foul", credit.getFoul() );
        model.addAttribute( "Time", credit.getTime() );
        model.addAttribute( "Time1", credit.getTime1() );
        return "Credit/list";
    }
    /**
     * 根据条件查询所有
     */
    @RequestMapping("/credit.json")
    @ResponseBody
    public PageInfo listPageJson(Credit credit,String time1, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo pageInfo = creditService.selectList(credit , pageNum, pageSize);
        return  pageInfo;
    }
    @RequestMapping("/add")
    public String add(Model model){
       try {
           List<Classes> list= creditService.findByClassName();
           List<Regulations> regList = creditService.findRegList();
           model.addAttribute("list",list);
           model.addAttribute("regList",regList);
       }catch (Exception ex){
           model.addAttribute( "rst", "-1" );  //新增失败
       }
        return "Credit/add";
    }
    /**
     * 新增
     * @param model
     * @return
     */
    @RequestMapping("/insert")
    public  String insert(Credit credit, Model model){
        int rst = 0;
        try{
            rst =  creditService.insert(credit);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //新增失败
        }
        return  "redirect:/credit/list";
    }
    /**
     * 根据id删除信息
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public  int delete(Integer id,Model model){
        int rst = 0;
        try{
            rst = creditService.delete(id);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //删除失败
        }
        return rst;
    }
    /**
     * 批量删除
     * @param model
     * @return
     */
    @RequestMapping("/deletes")
    @ResponseBody
    public  Integer delByIds(Model model,String ids){
        int rst = 0;
        try{
            List<String> idsList  = Arrays.asList(ids.split( "," ));
            rst = creditService.deletes(idsList);
        }catch (Exception ex){
            ex.printStackTrace();
            rst = -1;
        }
        if(rst <= 0){
            model.addAttribute( "rst", "-1" );  //删除失败
        }
        return rst;
    }
    /**
     * 根据产品id查询
     */
    @RequestMapping("/listByCtId")
    @ResponseBody
    public  List<String> listByPtId(Integer classesId){
        List<String> list = new ArrayList<>();
        try{
            list = creditService.findStuList(classesId);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return list;
    }
    /**
     * 根据班级的产品id去查询年级
     */
    @RequestMapping("/listByProdid")
    @ResponseBody
    public  List<Classes> listByProdid(Long id, Model model){
        Classes classes = new Classes();
        classes.setId(id);
        List<Classes> classesList = new ArrayList<>();
        try{
            classesList = creditService.findByProdid(classes);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return classesList;
    }

    /**
     * 多条件查询班级扣分情况
     */
    @RequestMapping("/selPoints")
    public String selPoints(String classId, String termId, String startTime, String endTime, Model model){
        System.out.println(classId+"__" + termId +"___" + startTime +"___" + endTime+"________________");
        Map<String, Object> condition = new HashMap<>();
        condition.put("classId",classId);
        condition.put("termId",termId);
        condition.put("startTime",startTime);
        condition.put("endTime",endTime);
        try {
            List<Credit> creditList = creditService.selectPoints(condition);
            model.addAttribute("creditList",creditList);

            Map<String, Object> rstMap = creditService.selects();
            model.addAttribute("cList",rstMap.get("cList"));
            model.addAttribute("tList",rstMap.get("tList"));
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return "credit/integral";
    }
}
