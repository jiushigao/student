package com.student.student.mapper;

import com.student.student.pojo.Producttype;
import com.student.student.pojo.Regulations;
import com.student.student.pojo.Term;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学期管理_张文
 */
@Mapper
public interface TermMapper {
    /**
     * 查询全部学期
     * @return
     */
    List<Term> findTermAll(Term term);

    /**
     * 根据id删除产品
     * @param id
     */
    int deleteTermById(@Param("id") String id);
    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deleteTermByIds(@Param("ids") List<String> ids);

    /**
     * 根据id查询单个产品信息进行修改
     * @param id
     * @return
     */
    Term findTermByIds(@Param("id") String id);

    /**
     * 获取实体类对象进行修改
     * @param term
     */
    int updateTerm(Term term);

    /**
     * 获取实体类对象进行新增
     * @param term
     */
    int insertTerm(Term term);
    /**
     * 根据条件统计
     * @param term
     * @return
     */
    int countByname(Term term);

    /**
     * 根据产品查询学期
     * @param id
     * @return
     */
    List<Term> selectTermid(Integer id);
}
