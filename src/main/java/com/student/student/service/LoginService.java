package com.student.student.service;

import com.student.student.pojo.Login;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 登录
 * 作者：Dora
 * 日期：2019-05-22 13:24
 */
public interface LoginService {
    /**
     * 登录
     * @param login 登录的用户名和密码，类型
     * @param ipAddress ip地址和用户登录地址
     * @return
     * @throws Exception
     */
    Login login(Login login, Map<String, String> ipAddress) throws Exception;

    /**
     * 修改密码
     * @param id
     * @param pwd
     */
    int updatePwd(Integer id, String pwd);

    /**
     * 根据条件统计
     * @param login
     * @return
     */
    int countByPhone(Login login);
    /**
     * 添加登录数据
     * @param login
     * @return
     */
    int insert(Login login);

    /**
     * 登录是首页的信息
     * @return
     */
    public Map<String, Object> indexInfo();

    /**
     * 统计电话号码是否存在,必须要传入角色id和手机号码
     * @param login
     * @return
     */
    int cntPhone(Login login);
}
