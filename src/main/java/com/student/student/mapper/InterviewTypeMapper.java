package com.student.student.mapper;

import com.student.student.pojo.InterviewFun;
import com.student.student.pojo.InterviewType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InterviewTypeMapper {

    /**
     *根据条件查询所有
     */
    List<InterviewType> selectList(InterviewType interviewType);

    /**
     * 查询单个
     */
    InterviewType selectOne(InterviewType interviewType);

    /**
     * 新增
     */
    int insertInterviewType(InterviewType interviewType);

    /**
     * 删除
     */
    int deleteInterviewType(Integer id);

    /**
     * 批量删除
     */
    int delByIds(@Param("ids") List<String> ids);

    /**
     * 修改
     */
    int updateInterviewType(InterviewType interviewType);

    /**
     * 统计类型名是否已经存在
     * @param interviewType
     * @return
     */
    int countByName(InterviewType interviewType);
}
