/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

/**
 * 班委(committe)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Committe implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 123800297338822052L;
    /** 班委编号 */
    private Integer id;
    /** 班委名称 */
    private String name;

    public Committe(Integer id) {
        this.id = id;
    }
    public Committe() {
    }
    public Committe(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * 获取班委编号
     * 
     * @return 班委编号
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置班委编号
     * 
     * @param id
     *          班委编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取班委名称
     * 
     * @return 班委名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置班委名称
     * 
     * @param name
     *          班委名称
     */
    public void setName(String name) {
        this.name = name;
    }
}