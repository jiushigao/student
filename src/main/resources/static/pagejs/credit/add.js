/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;

    //判断操作的结果
    if(null != rst && undefined != rst && rst == -1){
        layer.msg('操作异常，请联系管理员');
    }
    //产品的选择事件
    form.on('select(classs)', function(data){
        var classesId = data.elem.value;
        //给班级赋值
        var options=$("#class option:selected");
        $("#classs").val(options.text());
        $.post("/credit/listByCtId", "classesId="+classesId, function (data1) {
            $("#student").empty();
            var html = "<option value='0'>请选择学员</option>";
            $(data1).each(function () {
                html += "<option value='" + this.name + "'>" + this.name + "</option>";
        });
            //把遍历的数据放到select表里面
            $("#student").append(html);
            //从新刷新了一下下拉框
            form.render('select');      //重新渲染
            //给年级下拉框赋值
        }, "json");
        $.post("/credit/listByProdid", "id="+caId, function (data1) {
            $("#term").empty();
            var html = "<option value='0'>请选择班级</option>";
            $(data1).each(function () {
                html += "<option value='" + this.name + "'>" + this.name + "</option>";
            });
            //把遍历的数据放到select表里面
            $("#term").append(html);
            //从新刷新了一下下拉框
            form.render('select');      //重新渲染
        }, "json");

    });
    //产品的选择事件
    form.on('select(foul)', function(data){
        var integral = data.elem.value;
        $("#integral").val(integral);
        //给扣分项赋值
        var options=$("#foul1 option:selected");
        $("#foul").val(options.text());
    });
    //表单的提交，非空验证
    $("form").submit(function () {
        var time = $("#time").val();
        if(time == null || time == undefined || time == '' || time.trim().length <= 0){
            layer.msg("时间不能为空！");
            return false;
        }
        var clas=$("#class").val();
        if(clas=='-1'){
            layer.msg("请选择班级！");
            return false;
        }
        var term=$("#term").val();
        if(term=='0'){
            layer.msg("请选择年级！");
            return false;
        }
        var student=$("#student").val();
        if(student=='0'){
            layer.msg("请选择学员！");
            return false;
        }
        var foul1=$("#foul1").val();
        if(foul1=='-1'){
            layer.msg("请选择扣分项！");
            return false;
        }
    });
})