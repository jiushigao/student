package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Login;
import com.student.student.pojo.School;
import com.student.student.pojo.Teacher;
import com.student.student.service.LoginService;
import com.student.student.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

/**
 * 老师管理
 * @Author: Joe
 */
@Controller
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private TeacherService teacherService; //基本信息
    @Autowired
    private LoginService loginService; //登录信息

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/list")
    public String list(Teacher teacher, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;

        PageInfo<Teacher> pageInfo = new PageInfo<>(  );
        try{
            pageInfo = teacherService.findListPage(teacher, pageNum, pageSize);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "oper_rst", "-1" );
        }
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("teacher",teacher);
        return "teacher/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/teacher.json")
    @ResponseBody
    public PageInfo listPageJson(String name, String phone, Integer roleId, Integer pageNum, Integer pageSize,
                                 Model model){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<Teacher> pageInfo = new PageInfo<>(  );
        Teacher teacher = new Teacher();
        teacher.setName( name );
        teacher.setPhone( phone );
        teacher.setRoleId( roleId );
        try{
            pageInfo = teacherService.findListPage(teacher, pageNum, pageSize);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "oper_rst", "-1" );
        }
        return  pageInfo;
    }

    /**
     * 编辑教职信息
     * @param teacher
     * @return
     */
    @RequestMapping("/update")
    public String update(Teacher teacher, Model model) {
        int rst = 0;
        try {
            rst = teacherService.update(teacher);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "oper_rst", "-1" );
        }
        if(rst <= 0){
            model.addAttribute( "oper_rst", rst );
            return  "teacher/toAddOrUpdate?loginId="+teacher.getLoginId();
        }else{
            return "redirect:/teacher/list";
        }
    }

    /**
     * 只供跳路径,进入页面
     * @return
     */
    @RequestMapping("/toAddOrUpdate")
    public String toAddOrUpdate(Integer loginId,Model model) {
        if(null != loginId && loginId > 0){
            Teacher teacher = new Teacher();
            Teacher teacherCon = new Teacher(loginId);
            try{
                teacher = teacherService.findOne(teacherCon);
            }catch (Exception ex){
                ex.printStackTrace();
                model.addAttribute( "oper_rst", "-1" );
            }
            model.addAttribute("teacher",teacher);
        }
        return "teacher/edit";
    }

    /**
     * 添加教职信息
     * @param teacher
     * @return
     */
    @RequestMapping("/add")
    public String addTeacher(Teacher teacher, Model model) {
        int rst = 0;
        try {
            rst = teacherService.add(teacher);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "oper_rst", "-1" );
        }
        if(rst <= 0){
            model.addAttribute( "oper_rst", rst );
            return  "redirect:/teacher/toAddOrUpdate";
        }else{
            return "redirect:/teacher/list";
        }
    }

    /**
     * 根据Id删除教职信息
     * @param loginId
     * @return
     */
    @RequestMapping("/delById")
    @ResponseBody
    public Integer delById(Integer loginId) {
        int rst = 0;
        try {
            rst = teacherService.delById(loginId);
        }catch (Exception ex){
           ex.printStackTrace();
        }
        return rst;
    }

    /**
     * 根据Id删除教职信息
     * @param ids
     * @param model
     * @return
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        int rst = 0;
        try{
            List<String> idsList  = Arrays.asList(ids.split( "," ));
            rst = teacherService.delByIds(idsList);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return rst;
    }


}
