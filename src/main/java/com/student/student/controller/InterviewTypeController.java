package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Committe;
import com.student.student.pojo.InterviewType;
import com.student.student.pojo.InterviewType;
import com.student.student.service.InterviewTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

/**
 * 访谈类型
 * @author 资凯
 */

@Controller
@RequestMapping("/interviewType")
public class InterviewTypeController {

    @Autowired
    private InterviewTypeService interviewTypeService;

    /**
     * 根据条件查询所有
     */
    @GetMapping("/list")
    public String list(InterviewType interviewType, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<InterviewType> pageInfo = new PageInfo<>();
        try{
            pageInfo = interviewTypeService.selectList(interviewType, pageNum, pageSize);
        }catch (Exception ex){
            model.addAttribute("rst","-1");
            ex.printStackTrace();
        }
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("interviewType",interviewType);
        return "interviewType/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/interviewType.json")
    @ResponseBody
    public PageInfo listPageJson(InterviewType interviewType, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<InterviewType> pageInfo = new PageInfo<>();
        try{
            pageInfo = interviewTypeService.selectList(interviewType, pageNum, pageSize);
        }catch (Exception ex){
            model.addAttribute("rst","-1");
            ex.printStackTrace();
        }
        return  pageInfo;
    }

    /**
     *根据id查询
     */
    @GetMapping("/toAddOrUpdate")
    public String toAddOrUpdate(Integer id,Model model){
        if (id != null && id != null){
            InterviewType one = new InterviewType(id);
            try{
                InterviewType InterviewType = interviewTypeService.selectOne(one);
                model.addAttribute("interviewType",InterviewType);
            }catch (Exception ex){
                ex.printStackTrace();
                model.addAttribute( "rst", "-1" );
            }
        }
        return "interviewType/edit";
    }

    /**
     * 新增
     */
    @RequestMapping("/add")
    public String add(InterviewType interviewType, Model model){
        if(interviewType == null || interviewType.getTypeName() == null || interviewType.getTypeName().trim().length() <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/interviewType/add";
        }
        int cnt = -1;
        try{
            cnt = interviewTypeService.insertInterviewType(interviewType);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(cnt > 0){
            return "redirect:/interviewType/list";
        }else{
            model.addAttribute( "rst", "-1" );
            return "redirect:/interviewType/toAddOrUpdate";
        }
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delById")
    @ResponseBody
    public Integer delById(Integer id, Model model){
        int rst = -1;
        try{
            rst = interviewTypeService.deleteInterviewType(id);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 批量删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        if(ids == null || ids.trim().length() <= 0){
            return -1;
        }
        int rst = -1;
        try{
            rst = interviewTypeService.delByIds( Arrays.asList(ids.split( "," )));
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public String update(InterviewType interviewType, Model model){
        if(interviewType == null || interviewType.getTypeName() == null || interviewType.getTypeName().trim().length() <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/interviewType/toAddOrUpdate?id="+interviewType.getId();
        }
        int cnt = -1;
        try{
            cnt = interviewTypeService.updateInterviewType(interviewType);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(cnt <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/interviewType/toAddOrUpdate?id="+interviewType.getId();
        }else{
            return "redirect:/interviewType/list";
        }
    }

    /**
     * 验证名字是否已经存在
     * @param name
     * @param model
     * @return 1表示名字已经存在，0表示不存在
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public Integer checkName(String name, Integer id,  Model model){
        int  rst = -1;
        try{
            id = (id == null || id == -1) ? null : id;
            rst = interviewTypeService.countByName( new InterviewType( id, name ) );
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(rst > 0){
            return 1;
        }else{
            return 0;
        }

    }
}
