package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.LoginIpMapper;
import com.student.student.pojo.LoginIp;
import com.student.student.service.LoginIpService;
import com.student.student.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
public class LoginIpServiceImpl implements LoginIpService {
    @Autowired
    private LoginIpMapper loginIpMapper;

    @Override
    public List<LoginIp> findList(LoginIp loginIp) {
        return loginIpMapper.selectList(loginIp);
    }

    @Override
    public PageInfo<LoginIp> findListPage(LoginIp loginIp, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<LoginIp> list = loginIpMapper.selectList(loginIp);
        PageInfo<LoginIp> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

}
