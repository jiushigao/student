package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Role;

import java.util.List;
import java.util.Map;

/**
 * 角色
 * 作者：Dora
 * 日期：2019-06-08 13:02
 */
public interface RoleService {
    List<Role> findList(Role role);


    /**
     * 根据条件查询所有状态信息,分页
     */
    PageInfo<Role> findListPage(Role role, Integer pageNum, Integer pageSize);

    /**
     * 进入修改页面
     */
    Map<String, Object> toUpdate(int roleId);

    /**
     * 进入新增页面
     */
    Map<String, Object> toAdd();

    /**
     * 插入数据
     */
    int insert(String name, String permissions);

    /**
     * 修改数据
     */
    int update(Integer id, String name, String permissions);

    /**
     * 根据主键删除数据
     */
    int delById(Integer id);

    /**
     * 批量删除数据
     */
    int delByIds(List<String> ids);

    /**
     * 判断校区名字是否唯一
     */
    int findByName(String name, Integer id);
}
