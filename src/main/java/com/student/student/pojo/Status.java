/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

/**
 * 学员状态管理(status)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Status implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 5462284442533746458L;

    /** 状态id */
    private Integer id;

    /** 状态名称 */
    private String name;
    public Status(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    public Status(Integer id) {
        this.id = id;
    }
    public Status() {
    }
    /**
     * 获取状态id
     * 
     * @return 状态id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置状态id
     * 
     * @param id
     *          状态id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取状态名称
     * 
     * @return 状态名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置状态名称
     * 
     * @param name
     *          状态名称
     */
    public void setName(String name) {
        this.name = name;
    }
}