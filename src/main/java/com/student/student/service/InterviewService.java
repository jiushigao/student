package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.*;

import java.util.List;
import java.util.Map;

/**
 * 学员访谈
 * @author 资凯
 */
public interface InterviewService {

    /**
     * 根据条件查询List,分页
     * @param interview
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<Interview> findListPage(Interview interview, Integer pageNum, Integer pageSize);

    /**
     * 根据条件查询List,不分页
     * @param interview
     * @return
     */
    List<Interview> findList(Interview interview);

    /**
     * 新增访谈记录
     */
    int insertInterview(Interview interview);

    /**
     * 根据班级id查询对应学生
     */
    List<Student> findStudentByCid(Integer cid);

    /**
     * 进入修改页面所需要查询的数
     * @param id
     * @return
     */
    /*Map<String, Object> toUpdate(Integer id);*/

    /**
     * 进入新增页面所需要查询的数
     * @return
     */
    Map<String, Object> toAdd();
    /**
     * 根据访谈id删除访谈记录
     */
    int delById(Integer id);

    /**
     * 根据访谈id批量删除访谈记录
     */
    int delByIds(List<String> ids);

    /**
     * 根据访谈id修改访谈记录
     */
    int updateInterviewById(Interview interview);

    /**
     * 查询单个
     */
    Interview selectOne(Interview interview);

}
