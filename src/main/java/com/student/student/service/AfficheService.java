package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.Affiche;

import java.util.List;

public interface AfficheService {

    PageInfo<Affiche> findAfficheAll(String content, Integer pageNum, Integer pageSize);
    void deleteAfficheById(Integer noticeid);
    void deleteAfficheByIds(String[] stu);
    Affiche findAfficheByIds(Integer noticeid);
    void updateAffiche(Affiche affiche);
    void insertAffiche(Affiche affiche);
}
