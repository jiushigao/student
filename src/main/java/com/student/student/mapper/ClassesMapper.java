package com.student.student.mapper;

import com.student.student.pojo.Classes;
import com.student.student.pojo.Term;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ClassesMapper {
    /**
     * 根据条件查询多个
     * @param classes
     * @return
     */
    List<Classes> selectByConAll(Classes classes);

    /**
     * 根据条件查询单个
     * @param classes
     * @return
     */
    Classes selectByConOne(Classes classes);

    int  insertClasses(Classes classes);
    int  deleteClassesById(Integer id);
    int  updateClasses(Classes classes);
    /**
     * 查询所有的班级
     * @return
     */
    List<Classes> findByClassName();
    /**
     * 根据班级的产品id去查询年级
     * @return
     */
    List<Classes> findByProdid(Classes classes);
    /**
     * 批量删除
     * @param stu
     */
    void  deleteClassesByIds(String[] stu);

    int countByName(String name);

    /**
     * 根据条件统计
     * @param classes
     * @return
     */
    int count(Classes classes);

    /**
     * 根据年级得到班级的id和名字
     * @param termId
     * @return
     */
    List<Classes> findClassesByTerm(@Param( "termId" ) Integer termId);
}
