package com.student.student.service;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.InterviewFun;

import java.util.List;

public interface InterviewFunService {

    /**
     *根据条件查询所有
     */
    PageInfo<InterviewFun> findListPage(InterviewFun interviewFun, Integer pageNum, Integer pageSize);

    /**
     * 查询单个
     */
    InterviewFun findOne(InterviewFun interviewFun);

    /**
     * 新增
     */
    int add(InterviewFun interviewFun);

    /**
     * 删除
     */
    int delById(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    int delByIds(List<String> ids);

    /**
     * 修改
     */
    int update(InterviewFun interviewFun);

    /**
     * 统计类型名称是否已经存在
     * @param interviewFun
     * @return
     */
    int countByName(InterviewFun interviewFun);
}
