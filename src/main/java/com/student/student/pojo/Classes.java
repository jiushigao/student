
package com.student.student.pojo;

import lombok.Data;

/**
 * 班级信息(classes)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
@Data
public class Classes implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 427778729555718623L;

    /** 班级编号 */
    private Long id;

    /** 班级名称 */
    private String name;

    /** 教员老师id */
    private Integer technologyId;
    /** 班主任老师id */
    private Integer lifeId;
    /**学期*/
    private  Integer termId;
    /**学员状态0、就读1、毕业2、退学3、休学，*/
    private Integer status;

    /** 班主任老师*/
    private Teacher lifeTeacher;
    /** 教员老师 */
    private Teacher technologyTeacher;
    private  Term term;
    private  Producttype producttype;

    public Term getTerm() {
        return term;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public Integer getTermId() {
        return termId;
    }
    public void setTermId(Integer termId) {
        this.termId = termId;
    }
    public Producttype getProducttype() {
        return producttype;
    }
    public void setProducttype(Producttype producttype) {
        this.producttype = producttype;
    }

    public Teacher getLifeTeacher() {
        return lifeTeacher;
    }
    public void setLifeTeacher(Teacher lifeTeacher) {
        this.lifeTeacher = lifeTeacher;
    }
    public Teacher getTechnologyTeacher() {
        return technologyTeacher;
    }
    public void setTechnologyTeacher(Teacher technologyTeacher) {
        this.technologyTeacher = technologyTeacher;
    }

    public Classes() {
    }
    public Classes(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * 获取班级编号
     * 
     * @return 班级编号
     */
    public Long getId() {
        return this.id;
    }

    /**
     * 设置班级编号
     * 
     * @param id
     *          班级编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取班级名称
     * 
     * @return 班级名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置班级名称
     * 
     * @param name
     *          班级名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取教员老师id
     * 
     * @return 教员老师id
     */
    public Integer getTechnologyId() {
        return this.technologyId;
    }

    /**
     * 设置教员老师id
     * 
     * @param technologyId
     *          教员老师id
     */
    public void setTechnologyId(Integer technologyId) {
        this.technologyId = technologyId;
    }

    /**
     * 获取班主任老师id
     * 
     * @return 班主任老师id
     */
    public Integer getLifeId() {
        return this.lifeId;
    }

    /**
     * 设置班主任老师id
     * 
     * @param lifeId
     *          班主任老师id
     */
    public void setLifeId(Integer lifeId) {
        this.lifeId = lifeId;
    }
    /**学员状态0、就读1、毕业2、退学3、休学，*/
    public Integer getStatus() {
        return status;
    }
    /**学员状态0、就读1、毕业2、退学3、休学，*/
    public void setStatus(Integer status) {
        this.status = status;
    }
}