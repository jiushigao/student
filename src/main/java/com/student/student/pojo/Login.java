
package com.student.student.pojo;

import java.sql.Timestamp;
import java.util.List;

/**
 * 登录表(login)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Login implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 6891502496863910009L;
    /** 主键 */
    private Integer id;
    /** 电话 */
    private String phone;
    /** 密码 */
    private String pwd;
    /** 登录时间 */
    private Timestamp loginTime;
    /**角色id*/
    private Integer roleId;
    /**角色名*/
    private String roleName;
    /**昵称*/
    private String name;
    /**权限集合*/
    private List<Permission> permissionList;

    public Login() {
    }
    public Login(Integer id, String pwd) {
        this.id = id;
        this.pwd = pwd;
    }
    public Login(String phone, Integer roleId) {
        this.phone = phone;
        this.roleId = roleId;
    }
    public Login(Integer id, String phone,Integer roleId) {
        this.id = id;
        this.phone = phone;
        this.roleId = roleId;
    }
    public Login(Integer id, Timestamp loginTime) {
        this.id = id;
        this.loginTime = loginTime;
    }
    public Login(Integer id) {
        this.id = id;
    }
    public Login(String phone) {
        this.phone = phone;
    }
    public Login(String phone, String pwd, Integer roleId) {
        this.phone = phone;
        this.pwd = pwd;
        this.roleId = roleId;
    }

    /**
     * 获取主键
     * 
     * @return 主键
     */
    public Integer getId() {
        return this.id;
    }
    /**
     * 设置主键
     * 
     * @param id
     *          主键
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 获取电话
     * 
     * @return 电话
     */
    public String getPhone() {
        return this.phone;
    }
    /**
     * 设置电话
     * 
     * @param phone
     *          电话
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**
     * 获取密码
     * 
     * @return 密码
     */
    public String getPwd() {
        return this.pwd;
    }
    /**
     * 设置密码
     * 
     * @param pwd
     *          密码
     */
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    /**
     * 获取登录时间
     * 
     * @return 登录时间
     */
    public Timestamp getLoginTime() {
        return this.loginTime;
    }
    /**
     * 设置登录时间
     * 
     * @param loginTime
     *          登录时间
     */
    public void setLoginTime(Timestamp loginTime) {
        this.loginTime = loginTime;
    }
    /**角色id*/
    public Integer getRoleId() {
        return roleId;
    }
    /**角色id*/
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
    /**权限集合*/
    public List<Permission> getPermissionList() {
        return permissionList;
    }
    /**权限集合*/
    public void setPermissionList(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }
    /**昵称*/
    public String getName() {
        return name;
    }
    /**昵称*/
    public void setName(String name) {
        this.name = name;
    }
    /**角色名*/
    public String getRoleName() {
        return roleName;
    }
    /**角色名*/
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}