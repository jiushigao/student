package com.student.student.controller;

import com.student.student.pojo.Classes;
import com.student.student.pojo.Regulations;
import com.student.student.pojo.Violate;
import com.student.student.service.ViolateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;

@Controller
@RequestMapping("/vio")
public class ViolateController {
    @Autowired
    private ViolateService violateService;

    @RequestMapping("/toclassName")
    public  String toclassName(Model model,String name){
        List<Classes> classes = violateService.ClassName(name);
        model.addAttribute("classes",classes);
        model.addAttribute("name",name);
        return  "violate/violate_list";
    }
    /**
     * 根据班级查询班级违纪
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/toclassList")
    public  String toclassList(Model model, Integer id, String name){
        List<Violate> finvio = violateService.finvio(id,name);
        model.addAttribute("finvio",finvio);
        return  "violate/violate_list1";
    }

    /**
     * 根据班级查询班级名字和违纪分类
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/tofinvioid")
    public  String tofinvioid(Model model,Integer id){
        List<Violate> finvio = violateService.finvioid(id);
        List<Regulations> regulations = violateService.finvioReg();
        model.addAttribute("finvio",finvio);
        model.addAttribute("regulations",regulations);
        return  "violate/violate_add";
    }

    /**
     * 添加
     * @param model
     * @param violate
     * @return
     */
    @RequestMapping("/toAddvio")
    public  String toAddvio(Model model,Violate violate){
        violateService.addVio(violate);
        return  "redirect:/vio/toclassName";
    }

    /**
     * 删除
     * @param vid
     * @return
     */
    @RequestMapping("/todelvio")
    public  String todelvio(Integer vid){
        violateService.delVio(vid);
        return  "redirect:/vio/toclassName";
    }

}
