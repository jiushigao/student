package com.student.student.service.impl;

import com.student.student.mapper.*;
import com.student.student.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.sql.Timestamp;

import com.student.student.service.LoginService;
import org.springframework.stereotype.Service;

/**
 * 作者：Dora
 * 日期：2019-05-22 13:24
 */
@Service
public class LoginServiceImpl implements  LoginService{
    @Autowired
    private LoginMapper loginMapper ; //登录
    @Autowired
    private LoginIpMapper loginIpMapper; //登录日志
    @Autowired
    private ClassesMapper classesMapper; //班级
    @Autowired
    private StudemtMapper studemtMapper; //学员

    /**
     * 登录是首页的信息
     * @return
     */
    public Map<String, Object> indexInfo(){
        Map<String, Object> cntMap = new HashMap<>();
        //在校班级数量
        Classes classes = new Classes();
        classes.setStatus( 0 );
        int cntClasses = classesMapper.count( classes );
        cntMap.put( "cntClasses", cntClasses );
        //在校学员数量
        int cntStu = studemtMapper.countByStatus( );
        cntMap.put( "cntStu", cntStu );
        //进两个礼拜未联系学员
        //系统公告
        return cntMap;
    }

    /**
     * 统计电话号码是否存在,必须要传入角色id和手机号码
     * @param login
     * @return
     */
    @Override
    public int cntPhone(Login login) {
        return loginMapper.countByPhone( login );
    }

    /**
     * 登录
     * @param login 登录的用户名和密码，类型
     * @param ipAddress ip地址和用户登录地址
     * @return
     * @throws Exception
     */
    @Override
    public Login login(Login login, Map<String, String> ipAddress) throws Exception {
        //判断用户名和密码是否正确
        int rst = loginMapper.count( login );
        if(rst <= 0){
            return null;
        }
        //得到登录信息
        Login loginEntity = loginMapper.selectOne( login );
        //修改登录时间
        Date date = new Date(System.currentTimeMillis());
        Login updateLogin = new Login(loginEntity.getId(), new Timestamp(date.getTime()));
        rst = loginMapper.updateById( updateLogin);
        if(rst <= 0){
            return null;
        }
        //添加登录日志
        LoginIp loginIp = new LoginIp(loginEntity.getId(), ipAddress.get( "ip" ), ipAddress.get( "address" ), new Timestamp(date.getTime()));
        rst = loginIpMapper.add( loginIp );
        if(rst <= 0){
            return null;
        }
        return loginEntity;
    }

    /**
     * 修改密码
     * @param id
     * @param pwd
     */
    @Override
    public int updatePwd(Integer id,String pwd) {
        return loginMapper.updateById(new Login(id, pwd));
    }

    /**
     * 根据条件统计
     * @param login
     * @return
     */
    @Override
    public int countByPhone(Login login) {
        return loginMapper.countByPhone( login );
    }

    /**
     * 插入
     * @param login
     * @return
     */
    @Override
    public int insert(Login login) {
        return loginMapper.insert(login);
    }

}
