package com.student.student.controller;

import com.github.pagehelper.PageInfo;
import com.student.student.pojo.*;
import com.student.student.service.CommitteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * 班委管理
 * @author 资凯
 */
@Controller
@RequestMapping("/committe")
public class CommitteController {

    @Autowired
    private CommitteService committeService;

    /**
     * 根据条件查询所有
     */
    @GetMapping("/list")
    public String list(Committe committe, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<Committe> pageInfo = new PageInfo<>();
        try {
            pageInfo = committeService.findList(committe, pageNum, pageSize);
        }catch (Exception ex){
            model.addAttribute("rst","-1");
            ex.printStackTrace();
        }
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("committe",committe);
        return "committe/list";
    }

    /**
     * 根据条件查询所有
     */
    @RequestMapping("/committe.json")
    @ResponseBody
    public PageInfo listPageJson(Committe committe, Model model, Integer pageNum, Integer pageSize){
        pageNum = pageNum == null  ? 1 : pageNum;
        pageSize = pageSize == null  ? 10 : pageSize;
        PageInfo<Committe> pageInfo = new PageInfo<>();
        try {
            pageInfo = committeService.findList(committe, pageNum, pageSize);
        }catch (Exception ex){
            model.addAttribute("rst","-1");
            ex.printStackTrace();
        }
        return  pageInfo;
    }


    /**
     * 根据id查询
     */
    @GetMapping("/toAddOrUpdate")
    public String toAddOrUpdate(Integer id,Model model){
        if (id != null && id != null){
            Committe committeCon = new Committe(id);
            try {
                Committe committe = committeService.selectOne(committeCon);
                model.addAttribute("committe",committe);
            }catch (Exception ex){
                ex.printStackTrace();
                model.addAttribute( "rst", "-1" );
            }
        }
        return "committe/edit";
    }

    /**
     * 根据Id修改
     */
    @RequestMapping("/update")
    public String update(Committe committe,Model model){
        if(committe == null || committe.getName() == null || committe.getName().trim().length() <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/committe/toAddOrUpdate?id="+committe.getId();
        }
        int cnt = -1;
        try{
            cnt = committeService.updateCommitteById(committe);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(cnt <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/committe/toAddOrUpdate?id="+committe.getId();
        }else{
            return "redirect:/committe/list";
        }
    }

    /**
     * 新增
     */
    @RequestMapping("/add")
    public String add(Committe committe,Model model){
        if(committe == null || committe.getName() == null || committe.getName().trim().length() <= 0){
            model.addAttribute( "rst", "-1" );
            return "redirect:/committe/add";
        }
        int cnt = -1;
        try{
            cnt = committeService.insertCommitte(committe);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(cnt > 0){
            return "redirect:/committe/list";
        }else{
            model.addAttribute( "rst", "-1" );
            return "redirect:/committe/toAddOrUpdate";
        }
    }

    /**
     * 根据Id删除
     */
    @RequestMapping("/delById")
    @ResponseBody
    public Integer delById(Integer id,Model model){
        int rst = -1;
        try{
            rst = committeService.deleteCommitteById(id);
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 批量删除
     */
    @RequestMapping("/delByIds")
    @ResponseBody
    public Integer delByIds(String ids, Model model){
        if(ids == null || ids.trim().length() <= 0){
            return -1;
        }
        int rst = -1;
        try{
            rst = committeService.delByIds( Arrays.asList(ids.split( "," )));
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        return rst;
    }

    /**
     * 验证名字是否已经存在
     * @return 1表示名字已经存在，0表示不存在
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public Integer checkName(String name, Integer id,  Model model){
        int  rst = -1;
        try{
            id = (id == null || id == -1) ? null : id;
            rst = committeService.countByName( new Committe( id, name ) );
        }catch (Exception ex){
            ex.printStackTrace();
            model.addAttribute( "rst", "-1" );
        }
        if(rst > 0){
            return 1;
        }else{
            return 0;
        }

    }


    /**
     * 根据班级id查询对应班委信息
     */
    @GetMapping("/selByClassesId")
    public String selByClassesId(Integer classesId,Model model){
        Map<String, Object> condition = new HashMap<>();
        condition.put( "classesId",  classesId);
        List<Student> studentList = committeService.findAllByClassesId(condition);
        model.addAttribute("sList",studentList);
        return "committe/by_classIdlist";
    }

    /**
     * 修改学生职位
     */
    @RequestMapping("/toUpdateStu")
    public String updateStu(Integer sid,Integer cid,Model model){
        List<Student> studentList = null;//committeService.findAllByClassesId(cid);
        List<Committe> committeList = new ArrayList<>(); // committeService.findCommitteByCondition(null);
        model.addAttribute("sid",sid);
        model.addAttribute("cid",cid);
        model.addAttribute("sList",studentList);
        model.addAttribute("cList",committeList);
        return "committe/update_stu";
    }
}
