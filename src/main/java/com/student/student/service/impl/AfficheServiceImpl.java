package com.student.student.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.student.student.mapper.AfficheMapper;
import com.student.student.pojo.Admin;
import com.student.student.pojo.Affiche;
import com.student.student.service.AfficheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AfficheServiceImpl implements AfficheService {

    @Autowired
    private AfficheMapper afficheMapper;

    @Override
    public PageInfo<Affiche> findAfficheAll(String content, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Affiche> list = afficheMapper.findAfficheAll(content);
        for (Affiche affiche : list) {
            if(affiche.getContent().length() > 15){
                affiche.setContent( affiche.getContent().substring( 0,15 ) + "..." );
            }
            if(affiche.getTitle().length() > 7){
                affiche.setTitle( affiche.getTitle().substring( 0,7 ) + "..." );
            }
        }
        PageInfo<Affiche> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public void deleteAfficheById(Integer noticeid) {
        afficheMapper.deleteAfficheById(noticeid);
    }

    @Override
    public void deleteAfficheByIds(String[] stu) {
        afficheMapper.deleteAfficheByIds(stu);
    }

    @Override
    public Affiche findAfficheByIds(Integer noticeid) {
        return afficheMapper.findAfficheByIds(noticeid);
    }

    @Override
    public void updateAffiche(Affiche affiche) {
        afficheMapper.updateAffiche(affiche);
    }

    @Override
    public void insertAffiche(Affiche affiche) {
        afficheMapper.insertAffiche(affiche);
    }


}
