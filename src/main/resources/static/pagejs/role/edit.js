/*
* @Author: Dora
*/
layui.config({
    base : "js/"
}).use(['jquery','form','layer','jquery','element', 'laypage', 'layer'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        $ = layui.jquery;


    //一级菜单全选和全不选
    form.on('checkbox(p1allChoose)', function(data){
        var child = $(data.elem).parent().next().find('input[type="checkbox"]:not([name="show"])');
        child.each(function(index, item){
            item.checked = data.elem.checked;
        });
        form.render('checkbox');
    });
    //二级菜单
    form.on('checkbox(p2allChoose)', function(data){
        if(data.elem.checked == true){ //选中子级
            var child = $(data.elem).parent().prev().find('input[type="checkbox"]:not([name="show"])');
            child.each(function(index, item){
                item.checked = data.elem.checked;
            });
        }
        form.render('checkbox');
    });

    //验证名字是否已经存在
    $("[name=name]").blur(function(){
        var name = $(this).val();
        if(name == null || name == undefined || name.length <= 0){
            return;
        }
        var id = $("[name=id]").val() ;
        $.post("/role/checkName","name=" + name + "&id="+id,function (data) {
            if (data > 0) {
                $("[name=name]").val($("[name=hiddenName]").val());
                layer.msg('该角色名称已存在！', {time: 700});
            }
        },"text")
    }) ;
    //表单的提交，非空验证
    $(".submit").click(function () {
        var name = $("[name=name]").val();
        if(name == null || name == undefined || name == '' || name.trim().length <= 0){
            layer.msg("角色名不能为空！", {time: 700});
            return false;
        }
        var permissionIds = "";
        $("[name=p1name]:checked").each(function (i){
            permissionIds += $(this).attr("id") + ",";
        });
        $("[name=p2name]:checked").each(function (i){
            permissionIds += $(this).attr("id") + ",";
        });

        var id = $("[name=id]").val();
        if(id == null || id == undefined || id.trim().length <= 0) { //表示新增
            $.post("/role/add","name="+name+"&permissions="+permissionIds,function (data) {
                addCallBack(data);
            },"text");
        }else{
            $.post("/role/update","name="+name+"&permissions="+permissionIds+"&id="+id,function (data) {
                addCallBack(data);
            },"text");
        }
    });
})

//新增或修改的回调函数
function  addCallBack(data) {
    if (data > 0) {
        layer.msg('成功！', {time: 700}, function () {
            location.href = "/role/list";
        });
    }else{
        if(data == -5){
            layer.msg('有数据为空，请检查！', {time: 700});
        }else if(data == -6){
            layer.msg('角色名称已经存在！', {time: 700});
        }else{
            layer.msg('系统异常，请联系管理员！', {time: 700});
        }
    }
}