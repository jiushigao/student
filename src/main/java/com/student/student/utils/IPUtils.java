package com.student.student.utils;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 作者：Dora
 * 日期：2019-05-28 14:32
 */
public class IPUtils {

    public  static Map<String, String> getIpAndAddress(HttpServletRequest request){
        Map<String, String> rstMap = new HashMap<>();

        String ipAddress = request.getHeader("x-forwarded-for");
        if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if(ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")){
                InetAddress inet = null;//根据网卡取本机配置的IP
                try {
                    inet = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                ipAddress= inet.getHostAddress();
            }
        }
        //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if(ipAddress != null && ipAddress.length() > 15){
            if(ipAddress.indexOf(",") > 0){
                ipAddress = ipAddress.substring(0,ipAddress.indexOf(","));
            }
        }
        rstMap.put( "ip", ipAddress );
        URL url = null;
        try {
            url = new URL("http://ip.taobao.com/service/getIpInfo.php?ip=" + ipAddress);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        URLConnection conn = null;
        try {
            conn = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String line = null;
        StringBuffer result = new StringBuffer();
        while (true) {
            try {
                if (!((line = reader.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            result.append(line);
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ipAddress = result.substring(result.indexOf("data"));
        ipAddress = ipAddress.substring(ipAddress.indexOf(":") + 1);
        String region = ipAddress.substring(ipAddress.indexOf("region") + 1, ipAddress.indexOf("city"));
        String city = ipAddress.substring(ipAddress.indexOf("city") , ipAddress.indexOf("county"));

        String address = region.substring(8,10) + city.substring(7,9);//省 + 市区
        rstMap.put( "address", address );

        return rstMap;
    }
}
