package com.student.student.pojo;

import lombok.Data;

@Data
public class Violate {

    private  Integer vid;
    private  Integer sid;
    private  Integer rid;
    private  String  remark;

    private  Classes classes;
    private  Student student;
    private  Regulations regulations;

}
