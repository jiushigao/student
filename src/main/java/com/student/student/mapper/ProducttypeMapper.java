package com.student.student.mapper;

import com.student.student.pojo.Producttype;
import com.student.student.pojo.Regulations;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 产品
 */
@Mapper
public interface ProducttypeMapper {


    /**
     * 查询全部产品
     * @param producttype
     * @return
     */
    List<Producttype> selectList(Producttype producttype);

    /**
     * 返回单个实体
     * @param producttype
     * @return
     */
    Producttype selectOne(Producttype producttype);


    /**
     * 根据id删除产品
     * @param id
     */
    int deleteProducttypeById(@Param("id") String id);
    /**
     * 批量删除
     * @param ids
     * @return
     */
    int deleteProducttypeByIds(@Param("ids") List<String> ids);

    /**
     * 获取实体类对象进行修改
     * @param producttype
     */
    int updateProducttype(Producttype producttype);

    /**
     * 获取实体类对象进行新增
     * @param producttype
     */
    int insertProducttype(Producttype producttype);
    /**
     * 根据条件统计
     * @param producttype
     * @return
     */
    int countByname(Producttype producttype);
}
