package com.student.student.mapper;

import com.student.student.pojo.Workdir;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface workdirLHMapper {

    /**
     * 查询全部学期
     * @return
     */
    List<Workdir> findWorkdirAll(Workdir workdir);

    /**
     * 查询父级id
     * @param parentid
     * @return
     */
    Workdir findWorkdirById(@Param("parentid")Integer parentid);

    /**
     * 添加学员信息
     * @param stuId
     * @param filename
     */
    void insertWork(@Param("stuId") String stuId, @Param("filename") String filename);

    /**
     * 添加作业
     * @param name
     * @param type
     * @param parentid
     */
    void insertWorkdir(@Param("name") String name, @Param("type") String type, @Param("parentid") String parentid);

}
