/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.8
 */

package com.student.student.pojo;

/**
 * 产品管理(producttype)
 * 
 * @author Yang
 * @version 1.0.0 2019-05-21
 */
public class Producttype implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 3826393187755475123L;

    /** 产品id */
    private Integer id;
    /** 产品名称 */
    private String name;

    public Producttype(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    public Producttype(Integer id) {
        this.id = id;
    }
    public Producttype() {
    }

    /**
     * 获取产品名称
     *
     * @return 产品名称
     */
    public String getName() {
        return name;
    }
    /**
     * 设置产品名称
     *
     * @param name
     *          产品名称
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 获取产品id
     * 
     * @return 产品id
     */
    public Integer getId() {
        return this.id;
    }
    /**
     * 设置产品id
     * 
     * @param id
     *          产品id
     */
    public void setId(Integer id) {
        this.id = id;
    }
}