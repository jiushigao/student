package com.student.student.pojo.stu;

/**
 * 学员信息，用于列表页的封装数据
 * @author Dora
 */
public class StudentList implements java.io.Serializable {
    /** 主键 */
    private Integer id;
    /** 学员id */
    private Integer stuLoginId;
    /** 性别 */
    private String sex;
    /** QQ */
    private String qq;
    /** 学员基本情况描述 */
    private String describe;
    /** 家庭经济来源 */
    private String economy;
    /** 工作经历 */
    private String work;
    /** 游戏经历 */
    private String game;
    /** 推荐人 */
    private String referee;
    /** 身份证 */
    private String idCard;
    /** 家庭住址 */
    private String address;
    /** 备注 */
    private String remarks;
    /** 学员姓名 */
    private String stuName;
    /** 学员电话 */
    private String phone;
    /** 校区名字 */
    private String sname;
    /** 教员名字*/
    private String ttname;
    /** 班主任名字*/
    private String ltname;
     /** 班级名字*/
    private String cname;
    /** 年龄阶段*/
    private String ageName;
    /** 产品名称*/
    private String ptname;
    /** 学期名称*/
    private String termName;
    /** 学员类型*/
    private String stname;
   /** 职位名称 */
    private String positionName;
    /** 学员状态 */
    private String statusName;
    /** 主键 */
    public Integer getId() {
        return id;
    }
    /** 主键 */
    public void setId(Integer id) {
        this.id = id;
    }
    /** 学员id */
    public Integer getStuLoginId() {
        return stuLoginId;
    }
    /** 学员id */
    public void setStuLoginId(Integer stuLoginId) {
        this.stuLoginId = stuLoginId;
    }
    /** 性别 */
    public String getSex() {
        return sex;
    }
    /** 性别 */
    public void setSex(String sex) {
        this.sex = sex;
    }
    /** QQ */
    public String getQq() {
        return qq;
    }
    /** QQ */
    public void setQq(String qq) {
        this.qq = qq;
    }
    /** 学员基本情况描述 */
    public String getDescribe() {
        return describe;
    }
    /** 学员基本情况描述 */
    public void setDescribe(String describe) {
        this.describe = describe;
    }
    /** 家庭经济来源 */
    public String getEconomy() {
        return economy;
    }
    /** 家庭经济来源 */
    public void setEconomy(String economy) {
        this.economy = economy;
    }
    /** 工作经历 */
    public String getWork() {
        return work;
    }
    /** 工作经历 */
    public void setWork(String work) {
        this.work = work;
    }
    /** 游戏经历 */
    public String getGame() {
        return game;
    }
    /** 游戏经历 */
    public void setGame(String game) {
        this.game = game;
    }
    /** 推荐人 */
    public String getReferee() {
        return referee;
    }
    /** 推荐人 */
    public void setReferee(String referee) {
        this.referee = referee;
    }
    /** 身份证 */
    public String getIdCard() {
        return idCard;
    }
    /** 身份证 */
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
    /** 家庭住址 */
    public String getAddress() {
        return address;
    }
    /** 家庭住址 */
    public void setAddress(String address) {
        this.address = address;
    }
    /** 备注 */
    public String getRemarks() {
        return remarks;
    }
    /** 备注 */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    /** 学员姓名 */
    public String getStuName() {
        return stuName;
    }
    /** 学员姓名 */
    public void setStuName(String stuName) {
        this.stuName = stuName;
    }
    /** 学员电话 */
    public String getPhone() {
        return phone;
    }
    /** 学员电话 */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /** 校区名字 */
    public String getSname() {
        return sname;
    }
    /** 校区名字 */
    public void setSname(String sname) {
        this.sname = sname;
    }
    /** 教员名字*/
    public String getTtname() {
        return ttname;
    }
    /** 教员名字*/
    public void setTtname(String ttname) {
        this.ttname = ttname;
    }
    /** 班主任名字*/
    public String getLtname() {
        return ltname;
    }
    /** 班主任名字*/
    public void setLtname(String ltname) {
        this.ltname = ltname;
    }
    /** 班级名字*/
    public String getCname() {
        return cname;
    }
    /** 班级名字*/
    public void setCname(String cname) {
        this.cname = cname;
    }
    /** 年龄阶段*/
    public String getAgeName() {
        return ageName;
    }
    /** 年龄阶段*/
    public void setAgeName(String ageName) {
        this.ageName = ageName;
    }
    /** 产品名称*/
    public String getPtname() {
        return ptname;
    }
    /** 产品名称*/
    public void setPtname(String ptname) {
        this.ptname = ptname;
    }
    /** 学期名称*/
    public String getTermName() {
        return termName;
    }
    /** 学期名称*/
    public void setTermName(String termName) {
        this.termName = termName;
    }
    /** 学员类型*/
    public String getStname() {
        return stname;
    }
    /** 学员类型*/
    public void setStname(String stname) {
        this.stname = stname;
    }
    /** 职位名称 */
    public String getPositionName() {
        return positionName;
    }
    /** 职位名称 */
    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }
    /** 学员状态 */
    public String getStatusName() {
        return statusName;
    }
    /** 学员状态 */
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}